<?php
ini_set("display_errors",0);
include("../../includes/defines.php");
include("../../includes/fungsi.php");
session_start();
?>

<script type="text/javascript">
 function hanyaAngka(evt) {
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))

     return false;
   return true;
 }
</script>

<div class="card-header"><strong>TAMBAH DATA</strong></div>
<div class="card-body card-block">
 <form action="proses/crud/proses_tambah_penduduk.php" method="POST" enctype="multipart/form-data" >
   <div class="row">
    <div class="col-sm-12">
      <!-- text input -->
      <div class="form-group">
        <label>NIK</label>
        <input type="text" class="form-control" maxlength="16" minlength="16" name="nik" onkeypress="return hanyaAngka(event)" placeholder="NIK" autocomplete="off">
      </div>
    </div>
    <div class="col-sm-12">
      <!-- text input -->
      <div class="form-group">
        <label>NO KK</label>
        <input type="text" class="form-control" maxlength="16" minlength="16" name="nkk" onkeypress="return hanyaAngka(event)" placeholder="NIK" autocomplete="off">
      </div>
    </div>
    <div class="col-sm-12">
     <div class="form-group">
       <label>Nama</label>
       <input type="text" class="form-control" name="nama" onkeypress="return event.charCode < 48 || event.charCode  >57" autocomplete="off" placeholder="Nama">
     </div>
   </div>
   <div class="col-sm-8">
    <div class="form-group">
      <label>Tempat Lahir</label>
      <input type="text" class="form-control" name="tempat_lahir" onkeypress="return event.charCode < 48 || event.charCode  >57" autocomplete="off" placeholder="Tempat Lahir">
    </div>
  </div>
  <div class="col-sm-4">
    <div class="form-group">
      <label>Tanggal Lahir</label>
      <input type="date" class="form-control" name="tanggal_lahir">
    </div>
  </div>
  <div class="col-sm-12">
    <div class="form-group">
      <label>Jenis Kelamin</label>
      <select class="form-control" name="jenis_kelamin">
        <option value="">--Pilih--</option>
        <option value="LAKI-LAKI">Laki-laki</option>
        <option value="PEREMPUAN">Perempuan</option>
      </select>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="form-group">
      <label>Alamat</label>
      <textarea class="form-control" name="alamat"></textarea>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="form-group">
      <label>Status Perkawinan</label>
      <select class="form-control" name="status_perkawinan">
        <option value="">--Pilih--</option>
        <option value="KAWIN">Kawin</option>
        <option value="BELUM KAWIN">Belum Kawin</option>
      </select>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="form-group">
      <label>Pekerjaan</label>
      <input type="text" class="form-control" name="pekerjaan" autocomplete="off" placeholder="Pekerjaan">
    </div>
  </div>
  <div class="col-sm-12">
    <div class="form-group">
      <label>Agama</label>
      <select class="form-control" name="agama">
        <option value="">--Pilih--</option>
        <option>Islam</option>
        <option>Kristen</option>
        <option>Protestan</option>
        <option>Hindu</option>
        <option>Budha</option>
      </select>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="form-group">
      <label>Kewarganegaraan</label>
      <select class="form-control" name="kewarganegaraan">
        <option value="">--Pilih--</option>
        <option value="WNI">WNI</option>
        <option value="WNA">WNA</option>
      </select>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="form-group">
      <label>No. HP</label>
      <input type="text" class="form-control" name="no_hp" onkeypress="return hanyaAngka(event)" autocomplete="off" placeholder="No. HP">
    </div>
  </div>
  <div class="col-sm-12">
    <div class="form-group">
      <label>Email</label>
      <input type="email" class="form-control" name="email" autocomplete="off" placeholder="Email">
    </div>
  </div>
  <div class="card-footer">

   <button type="submit" value="Simpan" name="update" class="btn btn-primary btn-sm fa fa-dot-circle-o"> Simpan</button>
   <button type="submit"  class="btn btn-danger btn-sm" data-dismiss="modal">Kembali</button>
 </div>

</form>


</div>
