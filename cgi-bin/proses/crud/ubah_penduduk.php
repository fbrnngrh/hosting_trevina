  <?php
  include ("../../includes/defines.php");
  if(isset($_POST['nik']))
  {
    $id=$_POST['nik'];
    $query=mysqli_query($con,"SELECT * FROM tb_penduduk WHERE nik='$id'");
    $d=mysqli_fetch_assoc($query);
  }
  
  ?>

  <script type="text/javascript">
   function hanyaAngka(evt) {
     var charCode = (evt.which) ? evt.which : event.keyCode
     if (charCode > 31 && (charCode < 48 || charCode > 57))

       return false;
     return true;
   }
 </script>

 <div class="card-header"><strong>Ubah DATA</strong></div>
 <div class="card-body card-block">
   <form action="proses/crud/proses_ubah_penduduk.php" method="POST" enctype="multipart/form-data" >
     <div class="row">
      <div class="col-sm-12">
        <!-- text input -->
        <div class="form-group">
          <label>NIK</label>
          <input type="text" class="form-control" maxlength="16" minlength="16" name="nik" onkeypress="return hanyaAngka(event)" placeholder="NIK" value="<?php echo $d['nik'] ?>" readonly autocomplete="off">
        </div>
      </div>
       <div class="col-sm-12">
        <!-- text input -->
        <div class="form-group">
          <label>NO KK</label>
          <input type="text" class="form-control" maxlength="16" minlength="16" name="nkk" onkeypress="return hanyaAngka(event)" placeholder="NIK" value="<?php echo $d['nkk'] ?>" autocomplete="off">
        </div>
      </div>
      <div class="col-sm-12">
       <div class="form-group">
         <label>Nama</label>
         <input type="text" class="form-control" name="nama" onkeypress="return event.charCode < 48 || event.charCode  >57" autocomplete="off" value="<?php echo $d['nama'] ?>" placeholder="Nama">
       </div>
     </div>
     <div class="col-sm-8">
      <div class="form-group">
        <label>Tempat Lahir</label>
        <input type="text" class="form-control" name="tempat_lahir" onkeypress="return event.charCode < 48 || event.charCode  >57" autocomplete="off" value="<?php echo $d['tempat_lahir'] ?>" placeholder="Tempat Lahir">
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label>Tanggal Lahir</label>
        <input type="date" class="form-control" value="<?php echo $d['tgl_lahir'] ?>" name="tanggal_lahir">
      </div>
    </div>
    <div class="col-sm-12">
      <div class="form-group">
        <label>Jenis Kelamin</label>
        <select class="form-control" name="jenis_kelamin">
          <option <?php echo $d['jenis_kelamin']=='LAKI-LAKI' ?'selected':''; ?> value="LAKI-LAKI">Laki-laki</option>
          <option <?php echo $d['jenis_kelamin']=='PEREMPUAN' ?'selected':''; ?> value="PEREMPUAN">Perempuan</option>
        </select>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="form-group">
        <label>Alamat</label>
        <textarea class="form-control" name="alamat"><?php echo $d['alamat'] ?></textarea>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="form-group">
        <label>Status Perkawinan</label>
        <select class="form-control" name="status_perkawinan">
          <option <?php echo $d['status_perkawinan']=='KAWIN' ?'selected':''; ?> value="KAWIN">Kawin</option>
          <option <?php echo $d['status_perkawinan']=='BELUM KAWIN' ?'selected':''; ?> value="BELUM KAWIN">Belum Kawin</option>
        </select>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="form-group">
        <label>Pekerjaan</label>
        <input type="text" class="form-control" value="<?php echo $d['pekerjaan'] ?>" name="pekerjaan" autocomplete="off" placeholder="Pekerjaan">
      </div>
    </div>

  <div class="col-sm-12">
    <div class="form-group">
      <label>Agama</label>
      <select class="form-control" name="agama">
        <option value="">--Pilih--</option>
        <option <?php echo $d['agama']=='Islam' ?'selected':''; ?>>Islam</option>
        <option <?php echo $d['agama']=='Kristen' ?'selected':''; ?>>Kristen</option>
        <option <?php echo $d['agama']=='Protestan' ?'selected':''; ?>>Protestan</option>
        <option <?php echo $d['agama']=='Hindu' ?'selected':''; ?>>Hindu</option>
        <option <?php echo $d['agama']=='Budha' ?'selected':''; ?>>Budha</option>
      </select>
    </div>
  </div>
    <div class="col-sm-12">
      <div class="form-group">
        <label>Kewarganegaraan</label>
        <select class="form-control" name="kewarganegaraan">
          <option <?php echo $d['kewarganegaraan']=='WNI' ?'selected':''; ?> value="WNI">WNI</option>
          <option <?php echo $d['kewarganegaraan']=='WNA' ?'selected':''; ?> value="WNA">WNA</option>
        </select>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="form-group">
        <label>No. HP</label>
        <input type="text" class="form-control" name="no_hp" value="<?php echo $d['no_hp'] ?>" onkeypress="return hanyaAngka(event)" autocomplete="off" placeholder="No. HP">
      </div>
    </div>
    <div class="card-footer">

     <button type="submit" value="Simpan" name="update" class="btn btn-primary btn-sm fa fa-dot-circle-o"> Simpan</button>
     <button type="submit"  class="btn btn-danger btn-sm" data-dismiss="modal">Kembali</button>
   </div>

 </form>


</div>
