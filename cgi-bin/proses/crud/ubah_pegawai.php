﻿  <?php
  include ("../../includes/defines.php");
  if(isset($_POST['idpeg']))
  {
    $idpeg=$_POST['idpeg'];
    $query=mysqli_query($con,"SELECT * FROM pegawai    INNER JOIN bagian on bagian.id_bagian = pegawai.id_bagian
       INNER JOIN jabatan on jabatan.id_jabatan = pegawai.id_jabatan
       WHERE id_pegawai=$idpeg");
    $data=mysqli_fetch_assoc($query);

    $id_pegawai=$data['id_pegawai'];
    $nama_pegawai=$data['nama_pegawai'];
    $alamat=$data['alamat'];
    $id_jabatan=$data['id_jabatan'];
    $jabatan=$data['jabatan'];
    $id_bagian=$data['id_bagian'];
    $bagian=$data['bagian'];
    
}
?>           



<div class="card-header"><strong>UBAH DATA PEGAWAI</strong></div>
<div class="card-body card-block">
   <form action="proses/crud/proses_ubah_pegawai.php" method="POST" enctype="multipart/form-data" >

    
      <div class="form-group"><label for="company" class=" form-control-label">Pegawai</label>
          <input hidden="hidden" type="text" name="id_pegawai" value="<?php echo $data['id_pegawai']; ?>">
          <input type="text" name="nama_pegawai" placeholder="Masukkan data pegawai" value="<?php echo $data['nama_pegawai']; ?>" class="form-control" required>

      </div>
      <div class="form-group"><label for="company" class=" form-control-label">Alamat</label><input type="text" name="alamat"value="<?php echo $data['alamat']; ?>" placeholder="Masukkan data alamat" class="form-control" required></div>
      
      <div class="form-group">
          <label for="company" class=" form-control-label">Bagian</label>  


          <select class="form-control" id="exampleFormControlSelect1"  name="id_bagian" required>
              
            <?php
            
            $query= mysqli_query($con,"SELECT * FROM bagian");
            while($data=mysqli_fetch_assoc($query))
            {
                $selected = ($data['id_bagian']==$id_bagian) ? "selected" : "";
                echo "<option value='$data[id_bagian]' $selected>$data[bagian]</option>";
            }
            ?>
        </select>

        
        
    </div>

    <div class="form-group">
      <label for="company" class="control-label">Jabatan</label>  
      <select class="form-control" id="exampleFormControlSelect1" name="id_jabatan" required>
          
        <?php
        
        $query= mysqli_query($con,"SELECT * FROM jabatan");
        while($data=mysqli_fetch_assoc($query))
        {
            $selected = ($data['id_jabatan']==$id_jabatan) ? "selected" : "";
            echo "<option value='$data[id_jabatan]' $selected>$data[jabatan]</option>";
        }
        ?>
    </select>
    
</div>




<div class="card-footer">
  
   <button type="submit" value="Simpan" name="update" class="btn btn-primary btn-sm fa fa-dot-circle-o"> Simpan</button>
   <button type="submit"  class="btn btn-danger btn-sm" data-dismiss="modal">Kembali</button>
</div>

</form>


</div>
