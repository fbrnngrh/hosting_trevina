﻿<?php
ini_set("display_errors",0);
include("../../includes/defines.php");
include("../../includes/fungsi.php");
session_start();
?>



<div class="card-header"><strong>TAMBAH DATA</strong></div>
<div class="card-body card-block">
   <form action="proses/crud/proses_tambah_pegawai.php" method="POST" enctype="multipart/form-data" >
      <div class="form-group"><label for="company" class=" form-control-label">Nama Pegawai</label>
        <input type="text" name="nama_pegawai" placeholder="Masukkan Data Nama Pegawai" class="form-control" required>
    </div>
    <div class="form-group"><label for="company" class=" form-control-label">Alamat</label>
        <input type="text" name="alamat" placeholder="Masukkan Data Alamat" class="form-control" required>
    </div>

    <div class="form-group">
      <label for="company" class=" form-control-label">Jabatan</label>   
      <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="id_jabatan" required>
       <option>Pilih</option>
       <?php
       $query= mysqli_query($con,"SELECT * FROM jabatan ");
       while($data=mysqli_fetch_assoc($query))
       {
        $selected = ($data['id_jabatan']==$id_jabatan) ? "selected" : "";
        echo "<option value='$data[id_jabatan]' $selected>$data[jabatan]</option>";
    }
    ?>
</select>

</div>

<div class="form-group">
  <label for="company" class=" form-control-label">Bagian</label>   
  <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="id_bagian" required>
   <option>Pilih</option>
   <?php
   $query= mysqli_query($con,"SELECT * FROM bagian order by id_bagian asc");
   while($data=mysqli_fetch_assoc($query))
   {
    $selected = ($data['id_bagian']==$id_bagian) ? "selected" : "";
    echo "<option value='$data[id_bagian]' $selected>$data[bagian]</option>";
}
?>
</select>

</div>
<div class="form-group"><label for="company" class=" form-control-label">Username</label>
    <input type="text" name="username" placeholder="Masukkan Data username" class="form-control" required>
</div>
<div class="form-group"><label for="company" class=" form-control-label">Password</label>
    <input type="password" name="password" placeholder="Masukkan Data Password" class="form-control" required>
</div>



<div class="card-footer">

   <button type="submit" value="Simpan" name="update" class="btn btn-primary btn-sm fa fa-dot-circle-o"> Simpan</button>
   <button type="submit"  class="btn btn-danger btn-sm" data-dismiss="modal">Kembali</button>
</div>

</form>


</div>
