<?php
require('fpdf.php');
// session_start();

$pdf = new FPDF('P','mm',array(297,210)); //L For Landscape / P For Portrait
$pdf->AddPage();
$pdf->SetFillColor(255,255,255);
include '../../koneksi.php';
$no_surat=$_GET['id'];
$c=mysqli_query($connect, "SELECT * FROM tb_penduduk join tb_ghoib join tb_agama on tb_agama.id_agama=tb_penduduk.id_agama and tb_penduduk.nik=tb_ghoib.nik where tb_penduduk.nik!='admin' and no_surat='$no_surat'");
$dt=mysqli_fetch_array($c);
$no_surat = $dt['no_surat'];
$nik = $dt['nik'];
$bersangkutan = $dt['bersangkutan'];
// $tanggal = $dt['tanggal'];
$nama = $dt['nama'];
$jenis_kelamin = $dt['jenis_kelamin'];
$tempat_lahir = $dt['tempat_lahir'];
$tanggal_lahir = $dt['tgl_lahir'];
$agama = $dt['agama'];
$pekerjaan = $dt['pekerjaan'];
$no_ktp = $dt['no_ktp'];
$alamat = $dt['alamat'];
$tgl_dibuat = $dt['tanggal_dibuat'];
$ttd = $dt['ttd'];


function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
// $tgl_kematian=explode('-', $tgl_kematian);
// 	$tgl_df=$tgl_kematian[2];
// 	$bln_df=$tgl_kematian[1];
// 	$thn_df=$tgl_kematian[0];

// 	if ($bln_df=='1') {
// 		$bln_hasil_df='Januari';
// 	}elseif ($bln_df=='2') {
// 		$bln_hasil_df='Februari';
// 	}elseif ($bln_df=='3') {
// 		$bln_hasil_df='Maret';
// 	}elseif ($bln_df=='4') {
// 		$bln_hasil_df='April';
// 	}elseif ($bln_df=='5') {
// 		$bln_hasil_df='Mei';
// 	}elseif ($bln_df=='6') {
// 		$bln_hasil_df='Juni';
// 	}elseif ($bln_df=='7') {
// 		$bln_hasil_df='Juli';
// 	}elseif ($bln_df=='8') {
// 		$bln_hasil_df='Agustus';
// 	}elseif ($bln_df=='9') {
// 		$bln_hasil_df='September';
// 	}elseif ($bln_df=='10') {
// 		$bln_hasil_df='Oktober';
// 	}elseif ($bln_df=='11') {
// 		$bln_hasil_df='November';
// 	}elseif ($bln_df=='12') {
// 		$bln_hasil_df='Desember';
// 	}elseif ($bln_df=='00') {
// 		$bln_hasil_df='00';
// 	}

$pdf->Image('logo.png',20,10,18);
$pdf->SetFont('Arial','B',14);
$pdf->SetY(10);
$pdf->SetX(5);
$pdf->Cell(200,6,'PEMERINTAH KABUATEN TANAH LAUT',0,1,'C');
$pdf->SetX(5);
$pdf->Cell(200,6,'KECAMATAN PANYIPATAN',0,1,'C');
$pdf->SetX(5);
$pdf->Cell(200,6,'DESA BATU MULYA',0,1,'C');
$pdf->SetFont('Arial','I',8);
$pdf->SetX(5);
$pdf->Cell(200,4,'Alamat : Jl. Raya Batu Mulya RT. 002 Telpon: 082250325900 Kode Pos. 70871',0,1,'C');
$pdf->Cell(200,4,'Email : pemdesbatumulya@gmail.com',0,0,'C');
  // $pdf->SetY(10);
$pdf->SetLineWidth(1);
$pdf->Ln(5);
$pdf->SetX(20);
$pdf->Cell(165,0,'',1,1,'C');


$pdf->SetY(40);
$pdf->SetFont('Arial','BU',14);
$pdf->SetX(5);
$pdf->Cell(200,5,'SURAT KETERANGAN GHOIB',0,1,'C');
$pdf->SetFont('Arial','',12);
$pdf->SetX(5);
$pdf->Cell(200,5,'Nomor : '.$no_surat,0,1,'C');

$pdf->SetFont('Arial','',12);
$pdf->SetY(58);
$pdf->SetX(35);
$pdf->Cell(200,6,'Yang bertanda tangan dibawah ini, Lurah Karang Taruna, Kecamatan Pelaihari,  ',0,1,'L');
$pdf->SetX(20);
$pdf->SetY(65);
$pdf->SetX(34,5);
$pdf->Cell(200,6,' Kabupaten Tanah laut, Berdasarkan Surat pengantar dari Ketua RT.07B/002 serta ',0,1,'L');
$pdf->SetX(20);
$pdf->SetY(70);
$pdf->SetX(35);
$pdf->Cell(200,6,'setelah melalui penelitian yang dilakukan dengan seksama dengan ini menerangkan',0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,5,' sebenarnya bahwa   :',0,1,'L');
$pdf->SetY(85);
$pdf->SetX(35);
$pdf->Cell(200,6,'Nama ',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$nama,0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'Tempat Tanggal Lahir',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $tempat_lahir.', '.$tanggal_lahir,0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'Jenis Kelamin',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$jenis_kelamin,0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'Pekerjaan',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $pekerjaan,0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'Alamat',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $alamat,0,1,'L');

$pdf->SetY(125);
$pdf->SetX(35);
$pdf->Cell(200,6,' yang bersangkutan adalah suami dari : ',0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'Nama ',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $nama,0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'jenis Kelamin',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $jenis_kelamin,0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'Tempat tanggal dan lahir',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $tempat_lahir.', '.$tanggal_lahir,0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'Agama',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $agama,0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'pekerjaan',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $pekerjaan,0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'No KTP',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $no_ktp,0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'Alamat',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $alamat,0,1,'L');


// $pdf->SetY(150);
$pdf->SetX(35);
$pdf->Cell(200,6,'Telah mneninggalkan rumah sejak Desember yang lalu dan hingga saat ini  .',0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'tidak diketahui lagi dimana keberadaannya.',0,1,'L');
$pdf->SetX(20);

$pdf->SetX(35);
$pdf->Cell(200,6,'Demikian surat keterangan ini dibuat dan diberikan kepada yang bersangkutan  .',0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'untuk dipergunakan seperlunya.',0,1,'L');
$pdf->SetX(20);

$pdf->Ln(5);
$pdf->SetX(120);
$pdf->Cell(30,6,'Dikeluarkan di',0,0,'L');
$pdf->Cell(20,6,': Kelurahan Karang Taruna',0,1,'L');
$pdf->SetX(120);
$pdf->Cell(30,6,'Pada Tanggal ',0,0,'L');
$pdf->Cell(20,6,': '.$tgl_dibuat,0,1,'L');

// $pdf->SetY(225);
$pdf->Ln(15);
$pdf->SetX(120);
$pdf->Cell(200,6,'DODY SAPUTRA, S.STP',0,1,'L');
$pdf->SetX(120);
$pdf->Cell(200,6,'NIP. 19931228 201507 1 001.',0,1,'L');
	// $pdf->Image('health.png',180,5,15);
	// $pdf->Image('kesehatan.png',100,5,15);
 // 	$pdf->SetFont('Arial','',9);
 // 	$pdf->SetY(15);
	// $pdf->SetX(60);
	// $pdf->Cell(50,10,'Jl. H.M. Sarbini RT. XV RW. VI Desa Batu Ampar Kec. Batu Ampar',0,0,'L');



$pdf->Output();
?>