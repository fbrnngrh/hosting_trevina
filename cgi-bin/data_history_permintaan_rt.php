<?php
ini_set("display_errors",0);
include("includes/defines.php");
include("includes/fungsi.php");
include("includes/tgl_indo.php");
include("includes/fungsi_rupiah.php");
cekSession();
cekSession();

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Data History Permintaan Surat</title>

     <link rel="icon" type="image/png" href="login2/surat.png"/>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">


    <div id="wrapper">

         <?php include ('proses/menu/menu_rt.php')?>


                <!-- End of Topbar -->
                <div class="container-fluid">

        
                    <h1 class="h3 mb-2 text-gray-800">Tabel History Permintaan Surat</h1>
                    <p class="mb-4">Kamu Bisa menggunakan tambah, ubah, hapus</p>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Data History Permintaan Surat</h6>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                     <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Tanggal Permintaan</th>
                                           
                                            <th>Kategori Surat</th>
                                            <th>Keterangan Surat</th>
                                            <th>Status Surat</th>
                                            <th>RT</th>
                                            <th>Staf</th>
                                             <th>Kepala Desa</th>
                                            <th>Aksi</th>
                                           
                                        </tr>
                                    </thead>

                                     <?php
                                       $id_user=$_SESSION['id_user'];
                                                    $query = mysqli_query($con,"SELECT * FROM permintaan INNER JOIN katsur ON katsur.id_katsur = permintaan.id_katsur INNER JOIN user ON user.id_user = permintaan.id_user WHERE permintaan.history='Keluar'
                                                     ORDER BY id_permintaan asc");
                                                $no = 1;

                                                    while ($data = mysqli_fetch_assoc($query)) {?> 
                                    <tbody>
                                        <tr>
                                                <td> <?php echo $no++ ?></td> 
                                                <td> <?php echo $data['nama_peminta']; ?></td>
                                                <td> <?= tanggal_indo($data['tanggal_permintaan']); ?> </td>
                                              
                                                <td> <?php echo $data['katsur']; ?></td>
                                                <td> <?php echo $data['keterangan_surat']; ?></td>
                                               <td>    <?php
                                                        if ($data['status_surat'] == Menunggu){
                                                            echo ' <a href="#" class="btn btn-warning btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Menunggu</span>
                                                                    </a>';
                                                          
                                                        
                                                        } elseif ($data['status_surat'] == Terima_rt) {
                                                            echo ' <a href="#" class="btn btn-success btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Proses RT</span>
                                                                    </a>';

                                                        } elseif ($data['status_surat'] == Terima_petugas) {
                                                            echo ' <a href="#" class="btn btn-success btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Proses Petugas</span>
                                                                    </a>';
                                                             } elseif ($data['status_surat'] == Terima_kades) {
                                                            echo ' <a href="#" class="btn btn-success btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Proses Kepala Desa</span>
                                                                    </a>';
                                                        
                                                        } elseif ($data['status_surat'] == Tolak_rt) {
                                                            echo ' <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Proses Tolak RT</span>
                                                                    </a>';
                                                                    
                                                             } elseif ($data['status_surat'] == Tolak_petugas) {
                                                            echo ' <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Proses Tolak Petugas</span>
                                                                    </a>';
                                                                     } elseif ($data['status_surat'] == Tolak_kades) {
                                                            echo ' <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Proses Tolak Kepala Desa</span>
                                                                    </a>';
                                                        }
                                                       ?> 
                                                   </td>
                                               
                                               
                                                <td>    <?php
                                                        if ($data['approval_rt'] == Terima){
                                                            echo ' <a href="#" class="btn btn-success btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Terima</span>
                                                                    </a>';
                                                            } elseif ($data['approval_rt'] == Menunggu) {
                                                            echo ' <a href="#" class="btn btn-warning btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Menunggu</span>
                                                                    </a>';
                                                        
                                                          
                                                        } elseif ($data['approval_rt'] == Tolak) {
                                                            echo ' <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Tolak</span>
                                                                    </a>';
                                                        }
                                                       ?> 
                                                   </td>

                                                     <td>    <?php
                                                        if ($data['approval_petugas'] == Terima){
                                                            echo ' <a href="#" class="btn btn-success btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Terima</span>
                                                                    </a>';
                                                         } elseif ($data['approval_petugas'] == Menunggu) {
                                                            echo ' <a href="#" class="btn btn-warning btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Menunggu</span>
                                                                    </a>';
                                                          
                                                        } elseif ($data['approval_petugas'] == Tolak) {
                                                            echo ' <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Tolak</span>
                                                                    </a>';
                                                        }
                                                       ?> 
                                                   </td>


                                            
                                          <td>    <?php
                                                        if ($data['approval_kepala_desa'] == Terima){
                                                            echo ' <a href="#" class="btn btn-success btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Terima</span>
                                                                    </a>';

                                                         } elseif ($data['approval_kepala_desa'] == Menunggu) {
                                                            echo ' <a href="#" class="btn btn-warning btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Menunggu</span>
                                                                    </a>';
                                                          
                                                        } elseif ($data['approval_kepala_desa'] == Tolak) {
                                                            echo ' <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                                                                        <span class="icon text-white-50">
                                                                            <i class="fas fa-check"></i>
                                                                        </span>
                                                                        <span class="text">Tolak</span>
                                                                    </a>';
                                                        }
                                                       ?> 
                                                   </td>
                                      
                                      
                                      
                                      
                                      
                                      
                                                  <td class="td-actions text-center">

                                                            
                                                             <a href="download_pengguna.php?filename=<?=$data['surat']?>"><button type="button" rel="tooltip" class="btn btn-primary btn-round">
                                                            <i class="fa fa-download"></i>
                                                        </button></a><br><br>

                                                        </td>
                                        </tr>
                                       
                                    </tbody>
                                        <?php  } ?>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                           <span>Copyright &copy; by Trevina Sabrina Erin </span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

      <?php include ('proses/modal/modal_keluar.php')?>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>

</body>

</html>
<?php 

include("proses/modal/permintaan_pengguna.php");

?>