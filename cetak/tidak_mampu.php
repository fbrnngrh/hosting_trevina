<?php
require('fpdf.php');
// session_start();

$pdf = new FPDF('P','mm',array(297,210)); //L For Landscape / P For Portrait
$pdf->AddPage();
$pdf->SetFillColor(255,255,255);
include("../includes/defines.php");
$id_surat_keluar=$_GET['id'];
$c=mysqli_query($con, "SELECT * FROM surat_keluar sk left join tb_tidak_mampu tk on tk.id_surat_keluar=sk.id_surat_keluar left join tb_penduduk tp on tp.nik=tk.nik where kode_cetak='$id_surat_keluar'");
$dt=mysqli_fetch_array($c);

$cari=mysqli_fetch_array(mysqli_query($con, "SELECT nama_pegawai, jabatan frOM pegawai p left join jabatan j on p.id_jabatan=j.id_jabatan where jabatan='Kepala Desa'"));
$no_surat = $dt['no_surat'];
$nama = $dt['nama'];
$nik = $dt['nik'];
$nik_ayah = $dt['nik_ayah'];
$agama = $dt['agama'];
$status_perkawinan = $dt['status_perkawinan'];
//$keperluan = $dt['keperluan'];
$jk = $dt['jenis_kelamin'];
$tempat_lahir = $dt['tempat_lahir'];
$tanggal_lahir = $dt['tgl_lahir'];
$pekerjaan = $dt['pekerjaan'];
$alamat = $dt['alamat'];
$tgl_dibuat = $dt['tgl_dibuat'];
$ttd = $dt['ttd'];

$cc=mysqli_query($con, "SELECT * FROM tb_penduduk p left join tb_tidak_mampu t on p.nik=t.nik where p.nik='$nik_ayah'");
$dta=mysqli_fetch_array($cc);


function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

$pdf->Image('logo.png',20,10,18);
$pdf->SetFont('Arial','B',14);
$pdf->SetY(10);
$pdf->SetX(5);
$pdf->Cell(200,6,'PEMERINTAH KABUATEN TANAH LAUT',0,1,'C');
$pdf->SetX(5);
$pdf->Cell(200,6,'KECAMATAN PANYIPATAN',0,1,'C');
$pdf->SetX(5);
$pdf->Cell(200,6,'DESA BATU MULYA',0,1,'C');
$pdf->SetFont('Arial','I',8);
$pdf->SetX(5);
$pdf->Cell(200,4,'Alamat : Jl. Raya Batu Mulya RT. 002 Telpon: 082250325900 Kode Pos. 70871',0,1,'C');
$pdf->Cell(200,4,'Email : pemdesbatumulya@gmail.com',0,0,'C');
  // $pdf->SetY(10);
$pdf->SetLineWidth(1);
$pdf->Ln(5);
$pdf->SetX(20);
$pdf->Cell(165,0,'',1,1,'C');

$pdf->Ln(2);
$pdf->SetFont('Arial','BU',14);
$pdf->SetX(5);
$pdf->Cell(200,5,'SURAT KETERANGAN TIDAK MAMPU',0,1,'C');
$pdf->SetFont('Arial','',12);
$pdf->SetX(5);
$pdf->Cell(200,5,'Nomor : '.$no_surat,0,1,'C');

$pdf->Ln(5);
$pdf->SetFont('Arial','',12);
// $pdf->SetY(58);
$pdf->SetX(20);
$pdf->MultiCell(0,6,'Yang bertanda tangan dibawah ini :',0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Nama ',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$cari['nama_pegawai'],0,1,'L');

$pdf->SetX(20);
$pdf->Cell(200,6,'Jabatan ',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$cari['jabatan'],0,1,'L');

$pdf->SetX(20);
$pdf->MultiCell(0,6,'Yang bertanda tangan dibawah ini :',0,1,'L');
$pdf->Image("../ttd/".$dt['validasi_link'],160,10,25);
$pdf->SetX(20);
$pdf->Cell(200,6,'Nama',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$nama,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Tempat Tanggal Lahir',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $tempat_lahir.', '.tgl_indo($tanggal_lahir),0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Jenis Kelamin',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$jk,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'NIK',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(0,6,': '. $nik,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Status Perkawinan',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$status_perkawinan,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Pekerjaan',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$pekerjaan,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Agama',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$agama,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Alamat',0,0,'L');
$pdf->SetX(85);
$pdf->MultiCell(0,6,': '.$alamat,0,1,'L');

$pdf->SetX(20);
$pdf->MultiCell(0,6,'Yang bertanda tangan dibawah ini :',0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Nama Ayah',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$dta['nama'],0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'NIK',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$dta['nik'],0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Tempat Tanggal Lahir',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $dta['tempat_lahir'].', '.tgl_indo($dta['tgl_lahir']),0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Pekerjaan',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$pekerjaan,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Agama',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$agama,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Alamat',0,0,'L');
$pdf->SetX(85);
$pdf->MultiCell(0,6,': '.$alamat,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Penghasilan',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': Rp'.number_format($dt['penghasilan'],0,',','.'),0,1,'L');

// $pdf->SetY(130);
$pdf->SetX(20);
$pdf->MultiCell(0,6,'Yang bersangkutan adalah benar penduduk Desa Batu Mulya, Kecamatan Panyipatan, Kabupaten Tanah Laut yang mana orang tersebut di atas dalam keadaan ekonomi tidak mampu sebab:',0,1,'L');

$pdf->SetX(25);
$pdf->MultiCell(0,6,'1. Orang Tua yang bersangkutan berpenghasilan di bawah rata-rata',0,1,'L');

$pdf->SetX(20);
$pdf->MultiCell(0,6,'Surat Keterangan Tidak Mampu ini diberikan untuk Melengkapi Persyaratan Pengusulan Kartu Indonesia Pintar (KIP).',0,1,'L');


$pdf->SetX(20);

$pdf->MultiCell(0,6,'Demikian surat keterangan ini dibuat untuk dipergunakan sebagaimana mestinya.',0,1,'L');

$pdf->Ln(5);

$pdf->SetX(120);
$pdf->Cell(30,6,'Dikeluarkan di',0,0,'L');
$pdf->Cell(20,6,': Desa Batu Mulya',0,1,'L');
$pdf->SetX(120);
$pdf->Cell(30,6,'Pada Tanggal ',0,0,'L');
$pdf->Cell(20,6,': '.tgl_indo($tgl_dibuat),0,1,'L');

$pdf->Image("../ttd/".$ttd,121,233,15);
// $pdf->SetY(225);
$pdf->Ln(15);
$pdf->SetX(40);
$pdf->Cell(200,6,$nama,0,0,'L');
$pdf->SetX(120);
$pdf->Cell(200,6,$cari['nama_pegawai'],0,1,'L');
	// $pdf->Image('health.png',180,5,15);
	// $pdf->Image('kesehatan.png',100,5,15);
 // 	$pdf->SetFont('Arial','',9);
 // 	$pdf->SetY(15);
	// $pdf->SetX(60);
	// $pdf->Cell(50,10,'Jl. H.M. Sarbini RT. XV RW. VI Desa Batu Ampar Kec. Batu Ampar',0,0,'L');



$pdf->Output();
?>