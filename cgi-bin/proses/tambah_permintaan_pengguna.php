﻿<?php
ini_set("display_errors",0);
include("../includes/defines.php");
include("../includes/fungsi.php");
session_start();
?>
                 

                
                            <div class="card-header"><strong>TAMBAH DATA PERMINTAAN SURAT</strong></div>
                            <div class="card-body card-block">
                                 <form action="proses/proses_tambah_permintaan_pengguna.php" method="POST" enctype="multipart/form-data" >
                                      <div class="form-group"><label for="company" class=" form-control-label">Tanggal Permintaan</label><input type="date" name="tanggal_permintaan" placeholder="Masukkan Data Permintaan" class="form-control" required></div>

                                        <div class="form-group"><label for="company" class=" form-control-label">Nama</label><input type="text" name="nama_peminta" placeholder="Masukkan Data Nama" class="form-control" required></div>

                                        <div class="form-group">
                                          <label for="company" class=" form-control-label">Kategori Surat</label>   <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="id_katsur" required>
                                             <option>Pilih</option>
                                                <?php
                                                    $query= mysqli_query($con,"SELECT * FROM katsur order by id_katsur asc");
                                                    while($data=mysqli_fetch_assoc($query))
                                                      {
                                                        $selected = ($data['id_katsur']==$id_katsur) ? "selected" : "";
                                                         echo "<option value='$data[id_katsur]' $selected>$data[katsur]</option>";
                                                                    }
                                                                ?>
                                          </select>
                                    
                                    </div>


                                        <div class="form-group"><label for="company" class=" form-control-label">Keterangan Surat</label><input type="text" name="keterangan_surat" placeholder="Masukkan Data Keterangan Surat" class="form-control" required></div>

                                
                                
                                   <div class="card-footer">
                                      
                                         <button type="submit" value="Simpan" name="update" class="btn btn-primary btn-sm fa fa-dot-circle-o"> Simpan</button>
                                        <button type="submit"  class="btn btn-danger btn-sm" data-dismiss="modal">Kembali</button>
                                  </div>
                               
                                </form>

                              
                            </div>
                      