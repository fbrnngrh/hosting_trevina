﻿<?php
ini_set("display_errors",0);
include("../../includes/defines.php");
include("../../includes/fungsi.php");
session_start();
?>



<div class="card-header"><strong>TAMBAH DATA PENGGUNA</strong></div>
<div class="card-body card-block">
 <form action="proses/crud/proses_tambah_user.php" method="POST" enctype="multipart/form-data" >
 <!--  <div class="form-group">
    <label for="company" class=" form-control-label">Pengguna</label>
    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="id_pegawai" required>
     <option>Pilih</option>
     <?php
     $query1= mysqli_query($con,"SELECT * FROM pegawai 
         INNER JOIN bagian on bagian.id_bagian = pegawai.id_bagian 
         INNER JOIN jabatan on jabatan.id_jabatan = pegawai.id_jabatan
         ORDER BY id_pegawai");
     while($data1=mysqli_fetch_assoc($query1))
     {
        $selected = ($data1['id_pegawai']==$id_pegawai) ? "selected" : "";
        echo "<option value='$data1[id_pegawai]' $selected>$data1[nama_pegawai] / $data1[jabatan] </option>";
    }
    ?>
</select>
</div> -->
<!-- <div class="form-group">
    <label for="company" class=" form-control-label">Nama</label><input type="text" name="nama" placeholder="Masukkan data nama" class="form-control" required>
</div> -->
<!-- <div class="form-group">
    <label for="company" class=" form-control-label">Email</label><input type="text" name="email" placeholder="Masukkan data email" class="form-control" required>
</div> -->
<div class="form-group">
    <label for="company" class=" form-control-label">Username</label><input type="text" name="username" placeholder="Masukkan data username" class="form-control" required>
</div>
<div class="form-group">
    <label for="company" class=" form-control-label">Password</label><input type="password" name="password" placeholder="Masukkan data password" class="form-control" required>
</div>
<div class="form-group">
    <label for="company" class=" form-control-label">Level</label>
    <fieldset class="controls">
        <label class="custom-control custom-radio">
            <input type="radio" name="level" value="Admin" required class="form-check-input" <?php if($data['level']=="Admin"){echo "checked";}?>> <span class="custom-control-indicator"></span> <span class="custom-control-description"> Admin</span> </label>
        </fieldset>
        <fieldset>
            <label class="custom-control custom-radio">
                <input type="radio" name="level" value="Petugas" class="form-check-input" <?php if($data['level']=="Petugas"){echo "checked";}?>> <span class="custom-control-indicator"></span> <span class="custom-control-description">Petugas</span> </label>
            </fieldset>
            <fieldset>
                <label class="custom-control custom-radio">
                    <input type="radio" name="level" value="RT" class="form-check-input" <?php if($data['level']=="RT"){echo "checked";}?>> <span class="custom-control-indicator"></span> <span class="custom-control-description">RT</span> </label>
                </fieldset>
                <fieldset>
                    <label class="custom-control custom-radio">
                        <input type="radio" name="level" value="KaDes" class="form-check-input" <?php if($data['level']=="Kades"){echo "checked";}?>> <span class="custom-control-indicator"></span> <span class="custom-control-description">Kades</span> </label>
                    </fieldset>
                </div>


                <div class="card-footer">

                 <button type="submit" value="Simpan" name="update" class="btn btn-primary btn-sm fa fa-dot-circle-o"> Simpan</button>
                 <button type="submit"  class="btn btn-danger btn-sm" data-dismiss="modal">Kembali</button>
             </div>

         </form>


     </div>
