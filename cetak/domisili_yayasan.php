<?php
require('fpdf.php');
// session_start();

$pdf = new FPDF('P','mm',array(297,210)); //L For Landscape / P For Portrait
$pdf->AddPage();
$pdf->SetFillColor(255,255,255);
include '../../koneksi.php';
$no_surat=$_GET['id'];
$c=mysqli_query($connect, "SELECT * FROM tb_domisili_yayasan");
$dt=mysqli_fetch_array($c);

$lembaga = $dt['lembaga'];
$penanggung_jawab = $dt['penanggung_jawab'];
$jabatan = $dt['jabatan'];
$alamat = $dt['alamat'];
$tanggal_dibuat = $dt['tanggal_dibuat'];

// $tgl_kematian = $dt['tgl_kematian'];
$tanggal_dibuat = $dt['tanggal_dibuat'];

// function tgl_indo($tanggal){
//   $bulan = array (
//     1 =>   'Januari',
//     'Februari',
//     'Maret',
//     'April',
//     'Mei',
//     'Juni',
//     'Juli',
//     'Agustus',
//     'September',
//     'Oktober',
//     'November',
//     'Desember'
//   );
//   $pecahkan = explode('-', $tanggal);
//   return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
// }
// $tgl_kematian=explode('-', $tgl_kematian);
// 	$tgl_df=$tgl_kematian[2];
// 	$bln_df=$tgl_kematian[1];
// 	$thn_df=$tgl_kematian[0];

// 	if ($bln_df=='1') {
// 		$bln_hasil_df='Januari';
// 	}elseif ($bln_df=='2') {
// 		$bln_hasil_df='Februari';
// 	}elseif ($bln_df=='3') {
// 		$bln_hasil_df='Maret';
// 	}elseif ($bln_df=='4') {
// 		$bln_hasil_df='April';
// 	}elseif ($bln_df=='5') {
// 		$bln_hasil_df='Mei';
// 	}elseif ($bln_df=='6') {
// 		$bln_hasil_df='Juni';
// 	}elseif ($bln_df=='7') {
// 		$bln_hasil_df='Juli';
// 	}elseif ($bln_df=='8') {
// 		$bln_hasil_df='Agustus';
// 	}elseif ($bln_df=='9') {
// 		$bln_hasil_df='September';
// 	}elseif ($bln_df=='10') {
// 		$bln_hasil_df='Oktober';
// 	}elseif ($bln_df=='11') {
// 		$bln_hasil_df='November';
// 	}elseif ($bln_df=='12') {
// 		$bln_hasil_df='Desember';
// 	}elseif ($bln_df=='00') {
// 		$bln_hasil_df='00';
// 	}

$pdf->Image('logo.png',20,10,18);
$pdf->SetFont('Arial','B',14);
$pdf->SetY(10);
$pdf->SetX(5);
$pdf->Cell(200,6,'PEMERINTAH KABUATEN TANAH LAUT',0,1,'C');
$pdf->SetX(5);
$pdf->Cell(200,6,'KECAMATAN PANYIPATAN',0,1,'C');
$pdf->SetX(5);
$pdf->Cell(200,6,'DESA BATU MULYA',0,1,'C');
$pdf->SetFont('Arial','I',8);
$pdf->SetX(5);
$pdf->Cell(200,4,'Alamat : Jl. Raya Batu Mulya RT. 002 Telpon: 082250325900 Kode Pos. 70871',0,1,'C');
$pdf->Cell(200,4,'Email : pemdesbatumulya@gmail.com',0,0,'C');
	// $pdf->SetY(10);
$pdf->SetLineWidth(1);
$pdf->Ln(5);
$pdf->SetX(20);
$pdf->Cell(165,0,'',1,1,'C');


$pdf->SetY(40);
$pdf->SetFont('Arial','BU',14);
$pdf->SetX(5);
$pdf->Cell(200,5,'SURAT KETERANGAN DOMISILI YAYASAN',0,1,'C');
$pdf->SetFont('Arial','',12);
$pdf->SetX(5);
$pdf->Cell(200,5,'Nomor : '.$no_surat,0,1,'C');

$pdf->SetFont('Arial','',12);
// $pdf->SetY(58);
$pdf->SetX(35);
$pdf->Cell(200,6,'Yang bertanda tangan dibawah ini :',0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,5,'Nama    : DODY SAPUTRA, S. STP',0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,5,'NIP        : 19921228 201507 1 001',0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,5,'Jabatan : Lurah',0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,5,'Dengan ini menerangkan dengan sebenar-benarnya  bahwa   :',0,1,'L');
// $pdf->SetY(75);
$pdf->SetX(35);
$pdf->Cell(200,6,'Nama Lembaga ',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$lembaga,0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'Penanggung Jawab',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $penanggung_jawab,0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'Jabatan',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$jabatan,0,1,'L');
$pdf->SetX(35);
$pdf->Cell(200,6,'Alamat',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$alamat,0,1,'L');



// $pdf->SetY(115);
$pdf->SetX(35);
$pdf->Cell(200,6,'Demikian surat keterangan domisili ini dibuat dan diberikan  ',0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,' untuk dapat dipergunakan sebagaimana mestinya :',0,1,'L');


$pdf->SetFont('Arial','',12);

// $pdf->SetY(150);
// $pdf->SetY(150);

// $pdf->SetY(150);
$pdf->Cell(200,6,'Demikian surat keterangan  domisiili ini dibuat dan diberikan untu dapat dipergunakan dengan ',0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'sebagaimana mestinya.',0,1,'L');

// $pdf->SetY(175);
$pdf->SetX(120);
$pdf->Cell(30,6,'Dikeluarkan di',0,0,'L');
$pdf->Cell(20,6,': Kelurahan Karang Taruna',0,1,'L');
$pdf->SetX(120);
$pdf->Cell(30,6,'Pada Tanggal ',0,0,'L');
$pdf->Cell(20,6,': '.$tanggal_dibuat,0,1,'L');

$pdf->SetY(225);
$pdf->SetX(120);
$pdf->Cell(200,6,'DODY SAPUTRA, S.STP',0,1,'L');
$pdf->SetX(120);
$pdf->Cell(200,6,'NIP. 19931228 201507 1 001.',0,1,'L');
	// $pdf->Image('health.png',180,5,15);
	// $pdf->Image('kesehatan.png',100,5,15);
 // 	$pdf->SetFont('Arial','',9);
 // 	$pdf->SetY(15);
	// $pdf->SetX(60);
	// $pdf->Cell(50,10,'Jl. H.M. Sarbini RT. XV RW. VI Desa Batu Ampar Kec. Batu Ampar',0,0,'L');



$pdf->Output();
?>