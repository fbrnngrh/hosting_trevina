﻿<?php
ini_set("display_errors",0);
include("../../includes/defines.php");
include("../../includes/fungsi.php");
session_start();
?>
                 

                
                            <div class="card-header"><strong>TAMBAH DATA KATEGORI SURAT</strong></div>
                            <div class="card-body card-block">
                                 <form action="proses/crud/proses_tambah_katsur.php" method="POST" enctype="multipart/form-data" >
                                      <div class="form-group"><label for="company" class=" form-control-label">Kategori Surat</label><input type="text" name="katsur" placeholder="Masukkan Data Kategori Surat" class="form-control" required></div>

                                        <div class="form-group">
                                          <label for="company" class=" form-control-label">Macam Surat</label>   <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="id_macsur" required>
                                             <option>Pilih</option>
                                                <?php
                                                    $query= mysqli_query($con,"SELECT * FROM macsur order by id_macsur asc");
                                                    while($data=mysqli_fetch_assoc($query))
                                                      {
                                                        $selected = ($data['id_macsur']==$id_macsur) ? "selected" : "";
                                                         echo "<option value='$data[id_macsur]' $selected>$data[macsur]</option>";
                                                                    }
                                                                ?>
                                          </select>
                                    
                                    </div>

                                
                                
                                   <div class="card-footer">
                                      
                                         <button type="submit" value="Simpan" name="update" class="btn btn-primary btn-sm fa fa-dot-circle-o"> Simpan</button>
                                        <button type="submit"  class="btn btn-danger btn-sm" data-dismiss="modal">Kembali</button>
                                  </div>
                               
                                </form>

                              
                            </div>
                      