<?php
require('fpdf.php');
// session_start();

$pdf = new FPDF('P','mm',array(297,210)); //L For Landscape / P For Portrait
$pdf->AddPage();
$pdf->SetFillColor(255,255,255);
include("../includes/defines.php");
$id_surat_keluar=$_GET['id'];
$c=mysqli_query($con, "SELECT * FROM surat_keluar sk left join tb_lahiran tk on tk.id_surat_keluar=sk.id_surat_keluar where kode_cetak='$id_surat_keluar'");
$dt=mysqli_fetch_array($c);

$cari=mysqli_fetch_array(mysqli_query($con, "SELECT nama_pegawai frOM pegawai p left join jabatan j on p.id_jabatan=j.id_jabatan where jabatan='Kepala Desa'"));
$no_surat = $dt['no_surat'];
  $hari = $dt['hari_lahir'];
  $tanggal_lahir = $dt['tgl_lahir'];
  $jam_lahir = $dt['jam'];
  $jenis_kelamin = $dt['jenis_kelamin'];
  $jenis_kelahiran = $dt['jenis_kelahiran'];
  $lahiran_ke = $dt['kelahiran_ke'];
  $berat = $dt['berat'];
  $panjang = $dt['panjang'];
  $lahir_di = $dt['lahir_di'];
  $nama_anak = $dt['nama_anak'];
  $nama_ibu = $dt['nama_ibu'];
  $pekerjaan_ibu = $dt['pekerjaan_ibu'];
  $nik_ibu = $dt['nik_ibu'];
  $nama_ayah = $dt['nama_ayah'];
  $pekerjaan_ayah = $dt['pekerjaan_ayah'];
  $nik_ayah = $dt['nik_ayah'];
  $alamat = $dt['alamat'];
  $tgl_dibuat = $dt['tgl_surat'];
  $ttd = $dt['ttd'];

function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

$pdf->Image('logo.png',20,10,18);
$pdf->SetFont('Arial','B',14);
$pdf->SetY(10);
$pdf->SetX(5);
$pdf->Cell(200,6,'PEMERINTAH KABUATEN TANAH LAUT',0,1,'C');
$pdf->SetX(5);
$pdf->Cell(200,6,'KECAMATAN PANYIPATAN',0,1,'C');
$pdf->SetX(5);
$pdf->Cell(200,6,'DESA BATU MULYA',0,1,'C');
$pdf->SetFont('Arial','I',8);
$pdf->SetX(5);
$pdf->Cell(200,4,'Alamat : Jl. Raya Batu Mulya RT. 002 Telpon: 082250325900 Kode Pos. 70871',0,1,'C');
$pdf->Cell(200,4,'Email : pemdesbatumulya@gmail.com',0,0,'C');
	// $pdf->SetY(10);
$pdf->SetLineWidth(1);
$pdf->Ln(5);
$pdf->SetX(20);
$pdf->Cell(165,0,'',1,1,'C');


$pdf->Ln(2);
$pdf->SetFont('Arial','BU',14);
$pdf->SetX(5);
$pdf->Cell(200,5,'SURAT KETERANGAN LAHIR',0,1,'C');
$pdf->SetFont('Arial','',12);
$pdf->SetX(5);
$pdf->Cell(200,5,'Nomor : '.$no_surat,0,1,'C');
$pdf->Ln(5);
$pdf->SetX(20);
$pdf->MultiCell(0,6,'Yang bertanda tangan di bawah ini Kepala Desa Batu Mulya, Kecamatan Panyipatan, Kabupaten Tanah Laut, menerangkan bahwa telah lahir seorang bayi :',0,1,'L');
$pdf->Image("../ttd/".$dt['validasi_link'],160,10,25);
$pdf->SetX(20);
$pdf->Cell(200,6,'Hari, Tanggal ',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$hari.', '.tgl_indo($tanggal_lahir),0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Pukul ',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$jam_lahir,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Jenis Kelamin',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$jenis_kelamin,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Jenis Kelahiran',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$jenis_kelahiran,0,1,'L');

$pdf->SetX(20);
$pdf->Cell(200,6,'Kelahiran Ke',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$lahiran_ke,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Berat Lahir',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '.$berat,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Panjang Badan',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $panjang,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Panjang',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $panjang,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Lahir Di',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $lahir_di,0,1,'L');
$pdf->SetX(20);
$pdf->Cell(200,6,'Diberi Nama',0,0,'L');
$pdf->SetX(85);
$pdf->SetFont('Arial','BU',12);
$pdf->Cell(200,6,': '. $nama_anak,0,1,'L');

$pdf->SetFont('Arial','',12);
$pdf->SetX(20);
$pdf->Cell(200,6,'Dari Orang Tua',0,1,'L');


$pdf->SetX(20);
$pdf->Cell(200,6,'Nama Ibu',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $nama_ibu,0,1,'L');

$pdf->SetX(20);
$pdf->Cell(200,6,'Pekerjaan Ibu',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $pekerjaan_ibu,0,1,'L');

$pdf->SetX(20);
$pdf->Cell(200,6,'NIK Ibu',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $nik_ibu,0,1,'L');

$pdf->SetX(20);
$pdf->Cell(200,6,'Nama Ayah',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $nama_ayah,0,1,'L');

$pdf->SetX(20);
$pdf->Cell(200,6,'Pekerjaan Ayah',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $pekerjaan_ayah,0,1,'L');

$pdf->SetX(20);
$pdf->Cell(200,6,'NIK Ayah',0,0,'L');
$pdf->SetX(85);
$pdf->Cell(200,6,': '. $nik_ayah,0,1,'L');

$pdf->SetX(20);
$pdf->Cell(200,6,'Alamat',0,0,'L');
$pdf->SetX(85);
$pdf->MultiCell(0,6,': '. $alamat,0,1,'L');

$pdf->SetX(20);
$pdf->MultiCell(0,6,'Demikian Surat Keterangan ini dibuat untuk dipergunakan sebagaimana mestinya.',0,1,'L');

$pdf->Ln(5);
$pdf->SetX(120);
$pdf->Cell(30,6,'Dikeluarkan di',0,0,'L');
$pdf->Cell(20,6,': Desa Batu Mulya',0,1,'L');
$pdf->SetX(40);
$pdf->Cell(200,6,'Penolong Persalinan',0,0,'L');
$pdf->SetX(120);
$pdf->Cell(30,6,'Pada Tanggal ',0,0,'L');
$pdf->Cell(20,6,': '.tgl_indo($tgl_dibuat),0,1,'L');

$pdf->Image("../ttd/".$ttd,121,197,15);
// $pdf->SetY(225);
$pdf->Ln(15);
$pdf->SetX(40);
$pdf->Cell(200,6,'...................................',0,0,'L');
$pdf->SetX(120);
$pdf->Cell(200,6,$cari['nama_pegawai'],0,1,'L');
	// $pdf->Image('health.png',180,5,15);
	// $pdf->Image('kesehatan.png',100,5,15);
 // 	$pdf->SetFont('Arial','',9);
 // 	$pdf->SetY(15);
	// $pdf->SetX(60);
	// $pdf->Cell(50,10,'Jl. H.M. Sarbini RT. XV RW. VI Desa Batu Ampar Kec. Batu Ampar',0,0,'L');



$pdf->Output();
?>