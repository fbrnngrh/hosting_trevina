<?php
ini_set("display_errors",0);
include("includes/defines.php");
include("includes/fungsi.php");
cekSession();

?>


<!DOCTYPE html>
<html lang="en">
<?php include 'header.php' ?>

<body id="page-top">


    <div id="wrapper">

     <?php include ('proses/menu/menu_admin.php')?>


     <!-- End of Topbar -->
     <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Tabel Penduduk</h1>
        <p class="mb-4">Kamu Bisa menggunakan tambah, ubah, hapus<a target="_blank">


            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Data Penduduk</h6>
                    <br> <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Tambah 
                    </button><br>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                         <thead>
                            <tr>
                                <th>No</th>
                                <th>NKK</th>
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>Tempat/ Tanggal Lahir</th>
                                <th>Alamat</th>
                                <th>Aksi</th>

                            </tr>
                        </thead>

                        <tbody>

                            <?php
                            $query = mysqli_query($con,"SELECT * FROM tb_penduduk ORDER BY nkk asc");
                            $no = 1;

                            while ($data = mysqli_fetch_assoc($query)) {?> 
                                <tr>
                                    <td> <?php echo $no++ ?></td> 
                                    <td><?php echo $data['nkk']; ?></td>
                                    <td><?php echo $data['nik']; ?></td>
                                    <td><?php echo $data['nama']; ?></td>
                                    <td><?php echo $data['tempat_lahir'].', '.$dt['tgl_lahir']; ?></td>
                                    <td><?php echo $data['alamat']; ?></td>

                                    <td class="td-actions text-center">
                                     <a  href ="#edit" data-toggle="modal" data-id="<?php echo $data['nik'];?>" data-target="#edit"><button type="button" rel="tooltip" class="btn btn-success btn-round">
                                        <i class="fa fa-edit"></i>
                                    </button></a>

                                    <a  href="#" onclick="confirm_modal('proses/crud/hapus_penduduk.php?id=<?php echo $data['nik'];?>');"><button type="button" rel="tooltip" class="btn btn-danger btn-round remove">
                                        <i class="fas fa-trash"></i>
                                    </button></a>


                                </td>
                            </tr>
                        <?php  } ?>
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
           <span>Copyright &copy; by Trevina Sabrina Erin </span>
       </div>
   </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>

<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<?php include ('proses/modal/modal_keluar.php')?>

<?php include('footer.php')?>

</body>

</html>
<?php 

include("proses/modal/penduduk.php");

?>