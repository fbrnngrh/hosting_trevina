<script>
  $(document).ready(function() {
    $('.select2').select2({theme: 'bootstrap4'});
  });
</script>
<?php
$now=date('Y-m-d');
$tgl=explode('-', $now);
$tgl_df=$tgl[2];
$bln_df=$tgl[1];
$thn_df=$tgl[0];

if ($bln_df=='1') {
 $bln_hasil_df='I';
}elseif ($bln_df=='2') {
 $bln_hasil_df='II';
}elseif ($bln_df=='3') {
 $bln_hasil_df='III';
}elseif ($bln_df=='4') {
 $bln_hasil_df='IV';
}elseif ($bln_df=='5') {
 $bln_hasil_df='V';
}elseif ($bln_df=='6') {
 $bln_hasil_df='VI';
}elseif ($bln_df=='7') {
 $bln_hasil_df='VII';
}elseif ($bln_df=='8') {
 $bln_hasil_df='VIII';
}elseif ($bln_df=='9') {
 $bln_hasil_df='IX';
}elseif ($bln_df=='10') {
 $bln_hasil_df='X';
}elseif ($bln_df=='11') {
 $bln_hasil_df='XI';
}elseif ($bln_df=='12') {
 $bln_hasil_df='XII';
}elseif ($bln_df=='00') {
 $bln_hasil_df='00';
}

include("includes/defines.php");
session_start();
$id_user=$_SESSION['id_user'];
$level=$_SESSION['level'];
$cari=mysqli_fetch_array(mysqli_query($con, "SELECT * from tb_penduduk where id_user='$id_user'"));
$nik=$cari['nik'];
if ($level=='Pengguna') {
  $where=" where nik='$nik'";
}else{
  $where="";
}
if(isset($_POST['inputType'])){
	$inputType = $_POST['inputType'];
	switch($inputType){
		case 'Kehilangan':
    $query1 = "SELECT max(no_surat) as maxKode FROM tb_kehilangan";
    $hasil = mysqli_query ($con,$query1);
    $data  = mysqli_fetch_array($hasil);
    $kegiatan = $data['maxKode'];

    $noUrut = (int) substr($kegiatan, 4, 3);
    $noUrut++;
    // $char = "/470/001/Pem.";
    $satu=sprintf("%03s", $noUrut)."/";
    $dua='337/';
    $tiga='PEM/';
    $empat=$bln_hasil_df."/";
    $lima=$thn_df;
    $newID = $dua.$satu.$tiga.$empat.$lima;
    ?>
    
    <div class="form-group">
      <label>Nomor Surat</label>
      <input type="text" class="form-control" value="<?php echo $newID ?>" readonly name="no_surat">
    </div>
    <div class="form-group">
      <label>NIK</label>
      <select class="form-control select2" name="nik">
        <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>


    </select>
  </div>
  <div class="form-group">
    <label>Tanggal Kehilangan</label>
    <input type="date" class="form-control" name="tanggal_hilang">
  </div>

  <div class="form-group">
    <label>Jam</label>
    <input type="time" class="form-control" name="jam">
  </div>

  <div class="form-group">
    <label>Hari</label>
    <select class="form-control" name="hari">
      <option selected disabled>--Pilih--</option>
      <option>Senin</option>
      <option>Selasa</option>
      <option>Rabu</option>
      <option>Kamis</option>
      <option>Jumat</option>
      <option>Sabtu</option>
      <option>Minggu</option>
    </select>
  </div>
  <div class="form-group">
    <label>Lokasi</label>
    <input type="text" class="form-control" name="lokasi">
  </div>
  <div class="form-group">
    <label>Nama Barang Hilang</label>
    <input type="text" class="form-control" name="nama_barang_hilang">
  </div>
  <div class="form-group">
    <label>Atas Nama</label>
    <input type="text" class="form-control" name="atas_nama">
  </div>
  <div class="form-group">
    <label>Upload Persyaratan</label>
    <input type="file" class="form-control" accept="application/pdf" name="file">
  </div>
  <div class="form-group">
    <label>Tanggal Dibuat</label>
    <input type="date" class="form-control" value="<?php echo date('Y-m-d') ?>" readonly name="tgl_dibuat">
  </div>
  <?php
  break;
  case 'Kematian':
  $query1 = "SELECT max(no_surat) as maxKode FROM tb_kematian";
  $hasil = mysqli_query ($con,$query1);
  $data  = mysqli_fetch_array($hasil);
  $kegiatan = $data['maxKode'];

  $noUrut = (int) substr($kegiatan, 0, 3);
  $noUrut++;
    // $char = "/470/001/Pem.";
  $satu=sprintf("%03s", $noUrut)."/";
  $dua='472.12/';
  $tiga='PEM/';
  $empat=$bln_hasil_df."/";
  $lima=$thn_df;
  $newID = $dua.$satu.$tiga.$empat.$lima;
  ?>
  <div class="form-group">
    <label>Nomor Surat</label>
    <input type="text" class="form-control" value="<?php echo $newID ?>" readonly name="no_surat">
  </div>
  <div class="form-group">
    <label>NIK</label>
    <select class="form-control select2" name="nik">
     <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
  </select>
</div>
<div class="form-group">
  <label>Hari</label>
  <select class="form-control" name="hari">
    <option selected disabled>--Pilih--</option>
    <option>Senin</option>
    <option>Selasa</option>
    <option>Rabu</option>
    <option>Kamis</option>
    <option>Jumat</option>
    <option>Sabtu</option>
    <option>Minggu</option>
  </select>
</div>
<div class="form-group">
  <label> Tanggal Meninggal</label>
  <input type="date" class="form-control" name="tgl_kematian">
</div>
<div class="form-group">
  <label>Jam</label>
  <input type="time" class="form-control" name="jam">
</div>
<div class="form-group">
  <label>Tempat Kematian</label>
  <input type="text" class="form-control" name="tempat_kematian">
</div>
<div class="form-group">
  <label>Sebab Kematian</label>
  <input type="text" class="form-control" name="sebab_kematian">
</div>
<div class="form-group">
  <label>Yang Menyatakan</label>
  <input type="text" class="form-control" name="yang_menyatakan">
</div>
<div class="form-group">
  <label>Keterangan Visum</label>
  <input type="text" class="form-control" name="keterangan_visum">
</div>
<div class="form-group">
  <label>Desa</label>
  <input type="text" class="form-control" name="desa">
</div>
<div class="form-group">
  <label>Kecamatan</label>
  <input type="text" class="form-control" name="kec">
</div>
<div class="form-group">
  <label>Kabupaten</label>
  <input type="text" class="form-control" name="kab">
</div>
<div class="form-group">
  <label>Provinsi</label>
  <input type="text" class="form-control" name="prov">
</div>
<div class="form-group">
    <label>Upload Persyaratan</label>
    <input type="file" class="form-control" accept="application/pdf" name="file">
  </div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="text" class="form-control" name="tgl_dibuat" value="<?php echo $now; ?>" readonly>
</div>
<?php
break;
case 'Penguburan':
$query1 = "SELECT max(no_surat) as maxKode FROM tb_penguburan";
$hasil = mysqli_query ($con,$query1);
$data  = mysqli_fetch_array($hasil);
$kegiatan = $data['maxKode'];

$noUrut = (int) substr($kegiatan, 0, 3);
$noUrut++;
    // $char = "/470/001/Pem.";
$satu=sprintf("%03s", $noUrut)."/";
$dua='Pengu/';
$tiga='BM/';
$empat=$bln_hasil_df."/";
$lima=$thn_df;
$newID = $satu.$dua.$tiga.$empat.$lima;
?>
<div class="form-group">
  <label>No Surat Penguburan</label>
  <input type="text" class="form-control" value="<?php echo $newID ?>" readonly name="no_surat">
</div>
<!-- text input -->
<div class="form-group">
  <label>NIK</label>
  <select class="form-control select2" name="nik">
    <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
  </select>
</div>
<div class="form-group">
  <label>Hari</label>
  <select class="form-control" name="hari">
    <option selected disabled>--Pilih--</option>
    <option>Senin</option>
    <option>Selasa</option>
    <option>Rabu</option>
    <option>Kamis</option>
    <option>Jumat</option>
    <option>Sabtu</option>
    <option>Minggu</option>
  </select>
</div>
<div class="form-group">
  <label>Tanggal Meninggal</label>
  <input type="date" class="form-control" name="tanggal_meninggal">
</div>
<div class="form-group">
    <label>Upload Persyaratan</label>
    <input type="file" class="form-control" accept="application/pdf" name="file">
  </div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="date" class="form-control" readonly value="<?php echo date('Y-m-d') ?>" name="tgl_dibuat">
</div>
<?php
break;
case 'Pindah':
$query1 = "SELECT max(no_surat) as maxKode FROM tb_pindah";
$hasil = mysqli_query ($con,$query1);
$data  = mysqli_fetch_array($hasil);
$kegiatan = $data['maxKode'];

$noUrut = (int) substr($kegiatan, 0, 3);
$noUrut++;
    // $char = "/470/001/Pem.";
$satu=sprintf("%03s", $noUrut)."/";
$dua='Pindah/';
$tiga='BM/';
$empat=$bln_hasil_df."/";
$lima=$thn_df;
$newID = $satu.$dua.$tiga.$empat.$lima;
?>
<div class="form-group">
  <label>No Surat Pindah</label>
  <input type="text" class="form-control" value="<?php echo $newID ?>" readonly name="no_surat">
</div>
<div class="form-group">
  <label>NIK</label>
  <select class="form-control select2" name="nik">
   <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
  </select>
</div>

<div class="form-group">
  <label>Tanggal Pindah</label>
  <input type="date" class="form-control" name="pada_tanggal">
</div>
<div class="form-group">
  <label>Alamat Tujuan Pindah</label>
  <input type="text" class="form-control" name="alamat_tujuan_pindah">
</div>
<div class="form-group">
  <label>Alasan Pindah</label>
  <input type="text" class="form-control" name="alasan_pindah">
</div>
<div class="form-group">
  <label>Jumlah Pengikut</label>
  <input type="text" class="form-control" onkeypress="return hanyaAngka(event)" name="jumlah_pengikut">
</div>
<div class="form-group">
    <label>Upload Persyaratan</label>
    <input type="file" class="form-control" accept="application/pdf" name="file">
  </div>
<div class="form-group">
  <label>Tgl Dibuat</label>
  <input type="date" class="form-control" readonly value="<?php echo date('Y-m-d') ?>" name="tgl_dibuat">
</div>
<?php
break;
case 'Tidak Mampu':
$query1 = "SELECT max(no_surat) as maxKode FROM tb_tidak_mampu";
$hasil = mysqli_query ($con,$query1);
$data  = mysqli_fetch_array($hasil);
$kegiatan = $data['maxKode'];

$noUrut = (int) substr($kegiatan, 4, 3);
$noUrut++;
    // $char = "/470/001/Pem.";
$satu=sprintf("%03s", $noUrut)."/";
$dua='401/';
$tiga='SKTM-BM/';
$empat=$bln_hasil_df."/";
$lima=$thn_df;
$newID = $dua.$satu.$tiga.$empat.$lima;
?>
<div class="form-group">
  <label>Nomor Surat</label>
  <input type="text" class="form-control" name="no_surat" value="<?php echo $newID; ?>" readonly>
</div>
<div class="form-group">
  <label>NIK</label>
  <select class="form-control select2" name="nik">
   <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
  </select>
</div>
<div class="form-group">
  <label>NIK Ayah</label>
  <select class="form-control select2" name="nik_ayah">
    <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
  </select>
</div>
<div class="form-group">
  <label>Penghasilan</label>
  <input type="text" class="form-control" name="penghasilan">
</div>
<div class="form-group">
    <label>Upload Persyaratan</label>
    <input type="file" class="form-control" accept="application/pdf" name="file">
  </div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="text" class="form-control" name="tgl_dibuat" value="<?php echo $now; ?>" readonly>
</div>
<?php
break;
case 'Usaha':
$query1 = "SELECT max(no_surat) as maxKode FROM tb_usaha";
$hasil = mysqli_query ($con,$query1);
$data  = mysqli_fetch_array($hasil);
$kegiatan = $data['maxKode'];

$noUrut = (int) substr($kegiatan, 4, 3);
$noUrut++;
    // $char = "/470/001/Pem.";
$satu=sprintf("%03s", $noUrut)."/";
$dua='517/';
$tiga='KESRA/';
$empat=$bln_hasil_df."/";
$lima=$thn_df;
$newID = $dua.$satu.$tiga.$empat.$lima;
?>
<div class="form-group">
  <label>Nomor Surat Usaha</label>
  <input type="text" class="form-control" name="no_surat" value="<?php echo $newID; ?>" readonly>
</div>
<div class="form-group">
  <label>NIK</label>
  <select class="form-control select2" name="nik">
   <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
  </select>
</div>
<div class="form-group">
  <label>Usaha</label>
  <input type="text" class="form-control" name="usaha">
</div>
<div class="form-group">
  <label>Jenis Usaha</label>
  <input type="text" class="form-control" name="jenis_usaha">
</div>
<div class="form-group">
  <label>Penanggung Jawab</label>
  <input type="text" class="form-control" name="penanggung_jawab">
</div>
<div class="form-group">
  <label>Alamat Usaha</label>
  <input type="text" class="form-control" name="alamat_usaha">
</div>
<div class="form-group">
  <label>Akta Berdiri</label>
  <input type="text" class="form-control" name="akta">
</div>
<div class="form-group">
    <label>Upload Persyaratan</label>
    <input type="file" class="form-control" accept="application/pdf" name="file">
  </div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="date" class="form-control" name="tgl_dibuat" value="<?php echo $now; ?>" readonly>
</div>

<?php
break;
case 'Domisili Organisasi':
$query1 = "SELECT max(no_surat) as maxKode FROM tb_domisili_organisasi";
$hasil = mysqli_query ($con,$query1);
$data  = mysqli_fetch_array($hasil);
$kegiatan = $data['maxKode'];

$noUrut = (int) substr($kegiatan, 4, 3);
$noUrut++;
    // $char = "/470/001/Pem.";
$satu=sprintf("%03s", $noUrut)."/";
$dua='145/';
$tiga='SKDO/';
$empat=$bln_hasil_df."/";
$lima=$thn_df;
$newID = $dua.$satu.$tiga.$empat.$lima;
?>
<div class="form-group">
  <label>Nomor Surat Domisili Organisasi</label>
  <input type="text" class="form-control" name="no_surat" value="<?php echo $newID; ?>" readonly>
</div>
<div class="form-group">
  <label>Nama Organisasi</label>
  <input type="text" class="form-control" name="nama_organisasi">
</div>
<div class="form-group">
  <label>Jenis Kegiatan</label>
  <input type="text" class="form-control" name="jenis_kegiatan">
</div>
<div class="form-group">
  <label>Alamat Organisai</label>
  <input type="text" class="form-control" name="alamat_organisasi">
</div>
<div class="form-group">
  <label>Status Bangunan</label>
  <input type="text" class="form-control" name="status_bangunan">
</div>
<div class="form-group">
  <label>Peruntukan Bangunan</label>
  <input type="text" class="form-control" name="peruntukan_bangunan">
</div>
<div class="form-group">
  <label>Akta Pendirian</label>
  <input type="text" class="form-control" name="akta_pendirian">
</div>
<div class="form-group">
  <label>Peserta</label>
  <input type="text" class="form-control" name="peserta">
</div>
<div class="form-group">
  <label>Penanggung Jawab</label>
  <select class="form-control select2" name="pj">
    <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
  </select>
</div>
<div class="form-group">
  <label>Tanggal Berlaku</label>
  <input type="date" class="form-control" name="tgl_berlaku">
</div>
<div class="form-group">
    <label>Upload Persyaratan</label>
    <input type="file" class="form-control" accept="application/pdf" name="file">
  </div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="text" class="form-control" name="tgl_dibuat" value="<?php echo $now; ?>" readonly>
</div>
<?php
break;
case 'Belum Nikah':
$query1 = "SELECT max(no_surat) as maxKode FROM tb_belum_pernah_menikah";
$hasil = mysqli_query ($con,$query1);
$data  = mysqli_fetch_array($hasil);
$kegiatan = $data['maxKode'];

$noUrut = (int) substr($kegiatan, 7, 3);
$noUrut++;
    // $char = "/470/001/Pem.";
$satu=sprintf("%03s", $noUrut)."/";
$dua='471.11/';
$tiga='SKBM/';
$empat=$bln_hasil_df."/";
$lima=$thn_df;
$newID = $dua.$satu.$tiga.$empat.$lima;
?>
<div class="form-group">
  <label>Nomor Surat</label>
  <input type="text" class="form-control" name="no_surat" value="<?php echo $newID; ?>" readonly>
</div>
<div class="form-group">
  <label>NIK</label>
  <select class="form-control select2" name="nik">
    <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
  </select>
</div>
<div class="form-group">
  <label>Nama Ayah</label>
  <input type="text" class="form-control" name="nama_ayah">
</div>
<div class="form-group">
  <label>Nama Ibu</label>
  <input type="text" class="form-control" name="nama_ibu">
</div>
<div class="form-group">
    <label>Upload Persyaratan</label>
    <input type="file" class="form-control" accept="application/pdf" name="file">
  </div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="text" class="form-control" name="tgl_dibuat" value="<?php echo $now; ?>" readonly>
</div>
<?php
break;
case 'Lahiran':
$query1 = "SELECT max(no_surat) as maxKode FROM tb_lahiran";
$hasil = mysqli_query ($con,$query1);
$data  = mysqli_fetch_array($hasil);
$kegiatan = $data['maxKode'];

$noUrut = (int) substr($kegiatan, 4, 3);
$noUrut++;
    // $char = "/470/001/Pem.";
$satu=sprintf("%03s", $noUrut)."/";
$dua='145/';
$tiga='SKL';
$empat=$bln_hasil_df."/";
$lima=$thn_df;
$newID = $dua.$satu.$tiga;
?>
<div class="form-group">
  <label>No Surat Kelahiran</label>
  <input type="text" class="form-control" value="<?php echo $newID ?>" readonly name="no_surat">
</div>
<!-- text input -->
<!--  -->
<div class="form-group">
  <label>Hari Lahir</label>
  <select class="form-control" name="hari">
    <option selected disabled>--Pilih--</option>
    <option>Senin</option>
    <option>Selasa</option>
    <option>Rabu</option>
    <option>Kamis</option>
    <option>Jumat</option>
    <option>Sabtu</option>
    <option>Minggu</option>
  </select>
</div>
<div class="form-group">
  <label>Tanggal Lahir</label>
  <input type="date" class="form-control" name="tanggal_lahir">
</div>
<div class="form-group">
  <label>Jam Lahir</label>
  <input type="time" class="form-control" name="jam">
</div>
<div class="form-group">
  <label>Jenis Kelamin</label>
  <select class="form-control" name="jenis_kelamin">
    <option selected disabled>--Pilih--</option>
    <option>Laki-laki</option>
    <option>Perempuan</option>
  </select>
</div>
<div class="form-group">
  <label>Jenis Kelahiran</label>
  <input type="text" class="form-control" name="jenis_kelahiran">
</div>
<div class="form-group">
  <label>Lahiran Ke</label>
  <input type="text" class="form-control" name="lahiran_ke">
</div>
<div class="form-group">
  <label>Berat</label>
  <input type="text" class="form-control" name="berat">
</div>
<div class="form-group">
  <label>Panjang</label>
  <input type="text" class="form-control" name="panjang">
</div>
<div class="form-group">
  <label>Lahir Di</label>
  <input type="text" class="form-control" name="lahir_di">
</div>
<div class="form-group">
  <label>Nama Anak</label>
  <input type="text" class="form-control" name="nama_anak">
</div>
<div class="form-group">
  <label>Nama Ibu</label>
  <input type="text" class="form-control" name="nama_ibu">
</div>
<div class="form-group">
  <label>Pekerjaan Ibu</label>
  <input type="text" class="form-control" name="pekerjaan_ibu">
</div>
<div class="form-group">
  <label>NIK Ibu</label>
  <select class="form-control select2" name="nik_ibu">
    <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
  </select>
</div>

<div class="form-group">
  <label>Nama Ayah</label>
  <input type="text" class="form-control" name="nama_ayah">
</div>
<div class="form-group">
  <label>Pekerjaan Ayah</label>
  <input type="text" class="form-control" name="pekerjaan_ayah">
</div>
<div class="form-group">
  <label>NIK Ayah</label>
  <select class="form-control select2" name="nik_ayah">
    <option value="" disabled="" selected="">--Pilih Penduduk--</option>
    <?php 
    include '../koneksi.php';
    $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
    while ($dt=mysqli_fetch_array($cari)) {
      ?>
      <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
      <?php
    }
    ?>
  </select>
</div>
<div class="form-group">
  <label>Alamat</label>
  <input type="text" class="form-control" name="alamat">
</div>
<div class="form-group">
    <label>Upload Persyaratan</label>
    <input type="file" class="form-control" accept="application/pdf" name="file">
  </div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="date" class="form-control" readonly value="<?php echo date('Y-m-d') ?>" name="tgl_dibuat">
</div>
<?php
break;
case 'Bepergian':
$query1 = "SELECT max(no_surat) as maxKode FROM tb_bepergian";
$hasil = mysqli_query ($con,$query1);
$data  = mysqli_fetch_array($hasil);
$kegiatan = $data['maxKode'];

$noUrut = (int) substr($kegiatan, 4, 3);
$noUrut++;
    // $char = "/470/001/Pem.";
$satu=sprintf("%03s", $noUrut)."/";
$dua='470/';
$tiga='SKB/';
$empat=$bln_hasil_df."/";
$lima=$thn_df;
$newID = $dua.$satu.$tiga.$empat.$lima;
?>
<div class="form-group">
  <label>No Surat</label>
  <input type="text" class="form-control" value="<?php echo $newID ?>" readonly name="no_surat">
</div>
<div class="form-group">
  <label>NIK</label>
  <select class="form-control select2" name="nik">
    <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
    </select>
</div>

<div class="form-group">
  <label>Alamat</label>
  <input type="text" class="form-control" name="alamat_tujuan">
</div>
<div class="form-group">
  <label>Kota</label>
  <input type="text" class="form-control" name="kota">
</div>
<div class="form-group">
  <label>Provinsi</label>
  <input type="text" class="form-control" name="provinsi">
</div>
<div class="form-group">
  <label>Tanggal Berangkat</label>
  <input type="date" class="form-control" name="tgl_berangkat">
</div>
<div class="form-group">
  <label>Keperluan</label>
  <input type="text" class="form-control" name="keperluan">
</div>
<div class="form-group">
  <label>Pengikut (orang)</label>
  <input type="number" class="form-control" name="pengikut">
</div>
<div class="form-group">
    <label>Upload Persyaratan</label>
    <input type="file" class="form-control" accept="application/pdf" name="file">
  </div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="date" class="form-control" readonly value="<?php echo date('Y-m-d') ?>" name="tgl_dibuat">
</div>
<?php
break;
case 'Identitas':
$query1 = "SELECT max(no_surat) as maxKode FROM tb_identitas";
$hasil = mysqli_query ($con,$query1);
$data  = mysqli_fetch_array($hasil);
$kegiatan = $data['maxKode'];

$noUrut = (int) substr($kegiatan, 4, 3);
$noUrut++;
    // $char = "/470/001/Pem.";
$satu=sprintf("%03s", $noUrut)."/";
$dua='470/';
$tiga='SKID';
// $empat=$bln_hasil_df."/";
// $lima=$thn_df;
$newID = $dua.$satu.$tiga;
?>
<div class="form-group">
  <label>No Surat</label>
  <input type="text" class="form-control" value="<?php echo $newID ?>" readonly name="no_surat">
</div>
<div class="form-group">
  <label>NIK</label>
  <select class="form-control select2" name="nik">
    <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
  </select>
</div>

<div class="form-group">
  <label>Golongan Darah</label>
  <select name="gol_darah" class="form-control">
    <option selected disabled>--Pilih--</option>
    <option>A</option>
    <option>B</option>
    <option>AB</option>
    <option>O</option>
  </select>
</div>
<div class="form-group">
  <label>Tanggal Berlaku</label>
  <input type="date" class="form-control" name="tgl_berlaku">
</div>
<div class="form-group">
    <label>Upload Persyaratan</label>
    <input type="file" class="form-control" accept="application/pdf" name="file">
  </div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="date" class="form-control" readonly value="<?php echo date('Y-m-d') ?>" name="tgl_dibuat">
</div>
<?php
break;
case 'Tidak Memiliki Rumah':
$query1 = "SELECT max(no_surat) as maxKode FROM tb_tidak_memiliki_rumah";
$hasil = mysqli_query ($con,$query1);
$data  = mysqli_fetch_array($hasil);
$kegiatan = $data['maxKode'];

$noUrut = (int) substr($kegiatan, 4, 3);
$noUrut++;
    // $char = "/470/001/Pem.";
$satu=sprintf("%03s", $noUrut)."/";
$dua='470/';
$tiga='Kesra/';
$empat=$bln_hasil_df."/";
$lima=$thn_df;
$newID = $dua.$satu.$tiga.$empat.$lima;
?>
<div class="form-group">
  <label>No Surat</label>
  <input type="text" class="form-control" value="<?php echo $newID ?>" readonly name="no_surat">
</div>
<div class="form-group">
  <label>NIK</label>
  <select class="form-control select2" name="nik">
   <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
  </select>
</div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="date" class="form-control" readonly value="<?php echo date('Y-m-d') ?>" name="tgl_dibuat">
</div>
<?php
break;
case 'Yatim':
$query1 = "SELECT max(no_surat) as maxKode FROM tb_yatim";
$hasil = mysqli_query ($con,$query1);
$data  = mysqli_fetch_array($hasil);
$kegiatan = $data['maxKode'];

$noUrut = (int) substr($kegiatan, 4, 3);
$noUrut++;
    // $char = "/470/001/Pem.";
$satu=sprintf("%03s", $noUrut)."/";
$dua='145/';
$tiga='SKY/';
$empat=$bln_hasil_df."/";
$lima=$thn_df;
$newID = $dua.$satu.$tiga.$empat.$lima;
?>
<div class="form-group">
  <label>No Surat</label>
  <input type="text" class="form-control" value="<?php echo $newID ?>" readonly name="no_surat">
</div>
<div class="form-group">
  <label>NIK</label>
  <select class="form-control select2" name="nik">
    <?php
        if ($where=='') {
          ?>
          <option value="" selected="">--Pilih Penduduk--</option>
          <?php
        }
        ?>
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk $where");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
  </select>
</div>
<div class="form-group">
    <label>Upload Persyaratan</label>
    <input type="file" class="form-control" accept="application/pdf" name="file">
  </div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="date" class="form-control" readonly value="<?php echo date('Y-m-d') ?>" name="tgl_dibuat">
</div>
<?php
break;
}

}
?>

