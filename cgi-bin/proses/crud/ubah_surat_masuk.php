<?php
ini_set("display_errors",0);
include("../../includes/defines.php");
include("../../includes/fungsi.php");
session_start();
$id_surat=$_POST['id_surat_masuk'];
$c=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM surat_masuk where id_surat='$id_surat'"));
// $id_surat=$c['id_surat'];
?>


<div class="card-header"><strong>UBAH DATA</strong></div>
<div class="card-body card-block">
  <form action="proses/crud/proses_ubah_surat_masuk.php" method="POST" enctype="multipart/form-data" >
    <div class="form-group">
      <label for="company" class=" form-control-label">Nomor Surat</label>

      <input type="hidden" class="form-control" value="<?php echo $c['id_surat'] ?>" name="id_surat">
      <input type="text" class="form-control" value="<?php echo $c['nomor_surat'] ?>" name="nomor_surat">
    </div>
    <div class="form-group">
      <label for="company" class=" form-control-label">Tanggal Surat Masuk</label>
      <input type="text" class="form-control" value="<?php echo $c['tgl_masuk'] ?>" readonly name="tgl_masuk">
    </div>
    <div class="form-group">
      <label for="company" class=" form-control-label">Perihal</label>
      <input type="text" class="form-control" value="<?php echo $c['perihal'] ?>" name="perihal">
    </div>
    <div class="form-group">
      <label for="company" class=" form-control-label">Keterangan</label>
      <textarea class="form-control" rows="4" name="keterangan"><?php echo $c['keterangan'] ?></textarea>
    </div>
    <div class="form-group">
      <label for="company" class=" form-control-label">Lampiran</label>
      <input type="file" accept=".pdf" class="form-control" name="lampiran">
    </div>
    
    <div class="card-footer">

     <button type="submit" value="Simpan" name="update" class="btn btn-primary btn-sm fa fa-dot-circle-o"> Simpan</button>
     <button type="submit"  class="btn btn-danger btn-sm" data-dismiss="modal">Kembali</button>
   </div>

 </form>


</div>
