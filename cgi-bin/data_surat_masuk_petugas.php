<?php
ini_set("display_errors",0);
include("includes/defines.php");
include("includes/fungsi.php");
include("includes/tgl_indo.php");
include("includes/fungsi_rupiah.php");
cekSession();
session_start();
$id_user=$_SESSION['id_user'];
$c=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM pegawai where id_user='$id_user'"));
$id_pegawai=$c['id_pegawai'];

?>


<!DOCTYPE html>
<html lang="en">
<?php include 'header.php' ?>

<body id="page-top">


    <div id="wrapper">

       <?php include ('proses/menu/menu_petugas.php')?>


       <!-- End of Topbar -->
       <div class="container-fluid">


        <h1 class="h3 mb-2 text-gray-800">Tabel Surat Masuk</h1>
        <p class="mb-4">Kamu Bisa menggunakan tambah, ubah, hapus</p>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data Surat Masuk</h6>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                       <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor Surat</th>
                            <th>Tanggal Surat Masuk</th>
                            <th>Perihal</th>
                            <th>Keterangan</th>
                            <th>Lampiran</th>
                            <th>Disposisi</th>



                        </tr>
                    </thead>

                    
                    <tbody>
                       <?php

                       $query = mysqli_query($con,"SELECT * FROM surat_masuk s left join pegawai p on p.id_pegawai=s.disposisi where disposisi='$id_pegawai'");
                       $no = 1;

                       while ($data = mysqli_fetch_assoc($query)) {?> 
                        <tr>
                            <td> <?php echo $no++ ?></td> 
                            <td> <?php echo $data['nomor_surat']; ?></td>
                            <td> <?= tanggal_indo($data['tgl_masuk']); ?> </td>
                            <td> <?php echo $data['perihal']; ?></td>
                            <td> <?php echo $data['keterangan']; ?></td>
                            <td> <a href="surat_masuk/<?php echo $data['lampiran']; ?>" target='_blank'><?php echo $data['lampiran']; ?></a>
                            </td>
                            <td> <?php echo $data['nama_pegawai']; ?></td>
                        </tr>
                    <?php  } ?>
                </tbody>
                
            </table>
        </div>
    </div>
</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
         <span>Copyright &copy; by Trevina Sabrina Erin </span>
     </div>
 </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<?php include ('proses/modal/modal_keluar.php')?>

<?php include('footer.php')?>

</body>

</html>
<?php 

include("proses/modal/permintaan_pengguna.php");

?>