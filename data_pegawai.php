<?php
ini_set("display_errors",0);
include("includes/defines.php");
include("includes/fungsi.php");
cekSession();

?>


<!DOCTYPE html>
<html lang="en">

<?php include 'header.php' ?>

<body id="page-top">


    <div id="wrapper">

     <?php include ('proses/menu/menu_admin.php')?>


     <!-- End of Topbar -->
     <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Tabel Pegawai</h1>
        <p class="mb-4">Kamu Bisa menggunakan tambah, ubah, hapus</p>


        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data Pegawai</h6>
                <br> <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Tambah 
                </button><br>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pegawai</th>
                            <th>Alamat</th>
                            <th>Jabatan</th>
                            <th>Bagian</th>
                            <th>Aksi</th>

                        </tr>
                    </thead>


                    <tbody>
                       <?php
                       $query = mysqli_query($con,"SELECT * FROM pegawai 
                         INNER JOIN bagian on bagian.id_bagian = pegawai.id_bagian
                         INNER JOIN jabatan on jabatan.id_jabatan = pegawai.id_jabatan
                         
                         ORDER BY id_pegawai");
                       $no = 1; 

                       while ($data = mysqli_fetch_assoc($query)) {?> 
                        <tr>
                            <td> <?php echo $no++ ?></td> 
                            <td> <?php echo $data['nama_pegawai']; ?></td>
                            <td> <?php echo $data['alamat']; ?></td>
                            <td> <?php echo $data['jabatan']; ?></td>

                            <td> <?php echo $data['bagian']; ?></td>
                            <td class="td-actions text-center">
                             <a  href ="#edit" data-toggle="modal" data-id="<?php echo $data['id_pegawai'];?>" data-target="#edit"><button type="button" rel="tooltip" class="btn btn-success btn-round">
                                <i class="fa fa-edit"></i>
                            </button></a>

                            <a  href="#" onclick="confirm_modal('proses/crud/hapus_pegawai.php?id=<?php echo $data['id_pegawai'];?>');"><button type="button" rel="tooltip" class="btn btn-danger btn-round remove">
                                <i class="fas fa-trash"></i>
                            </button></a>


                        </td>
                    </tr>
                <?php  } ?>
            </tbody>
            
        </table>
    </div>
</div>
</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
           <span>Copyright &copy; by Trevina Sabrina Erin </span>
       </div>
   </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<?php include ('proses/modal/modal_keluar.php')?>
<?php include('footer.php')?>

</body>

</html>
<?php 

include("proses/modal/pegawai.php");

?>