-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 14, 2023 at 04:11 PM
-- Server version: 5.7.43
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `labid_sisurmen_batumulya`
--

-- --------------------------------------------------------

--
-- Table structure for table `bagian`
--

CREATE TABLE `bagian` (
  `id_bagian` int(11) NOT NULL,
  `bagian` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bagian`
--

INSERT INTO `bagian` (`id_bagian`, `bagian`) VALUES
(1, 'Staf Pelayanan Masyarakat'),
(2, 'Kades'),
(3, 'Bendahara');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `jabatan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `jabatan`) VALUES
(1, 'Kepala Desa'),
(2, 'Sekdess'),
(3, 'KASI Kesejahteraan Pelayanan');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` text NOT NULL,
  `alamat` text NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `id_bagian` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `alamat`, `id_jabatan`, `id_bagian`, `id_user`) VALUES
(1, 'MASDUKI', 'Batu Mulya', 1, 2, 0),
(2, 'Erin', 'Batu Mulya', 3, 1, 13),
(3, 'Sabrina', 'Sabrina', 2, 3, 14);

-- --------------------------------------------------------

--
-- Table structure for table `permintaan`
--

CREATE TABLE `permintaan` (
  `id_permintaan` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tanggal_permintaan` date NOT NULL,
  `nama` text NOT NULL,
  `keterangan_surat` text NOT NULL,
  `status` text NOT NULL,
  `approval_kades` varchar(10) NOT NULL,
  `approval_petugas` varchar(10) NOT NULL,
  `notif_kades` text NOT NULL,
  `notif_petugas` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `surat_keluar`
--

CREATE TABLE `surat_keluar` (
  `id_surat_keluar` int(11) NOT NULL,
  `jenis_surat` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `ttd` varchar(100) NOT NULL,
  `kode_cetak` varchar(100) NOT NULL,
  `validasi_link` text NOT NULL,
  `upload_file` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_keluar`
--

INSERT INTO `surat_keluar` (`id_surat_keluar`, `jenis_surat`, `status`, `ttd`, `kode_cetak`, `validasi_link`, `upload_file`) VALUES
(95, 'Kematian', 'Tunggu', 'e5de7fb55726fa826fb21c71d1772bc4.png', '20230710025404', '99636349f0aaf152fccd924b8b553a60.png', 'Persyaratan dan Ketentuan Pengajuan Permohonan Surat Kantor Desa Batu Mulya.pdf'),
(96, 'Kehilangan', 'Tunggu', '8e901861deda8864a4f37e3a49835b25.png', '20230711090941', 'ea2ab87dbe8b42e076540d56a7811bf0.png', 'IBADAH PEMUDA REMAJA GPDI BUMI ASIH.pdf'),
(97, 'Kehilangan', 'Tunggu', '8e901861deda8864a4f37e3a49835b25.png', '20230711090945', 'da9e7ea0a058b70ae5f9c4a5f9a8a93a.png', 'IBADAH PEMUDA REMAJA GPDI BUMI ASIH.pdf'),
(98, 'Kehilangan', 'Tunggu', '8e901861deda8864a4f37e3a49835b25.png', '20230711090947', '4a23ef7c671e60b5e3e11231d5924170.png', 'IBADAH PEMUDA REMAJA GPDI BUMI ASIH.pdf'),
(99, 'Identitas', 'Tunggu', '54c00516302037a4abadf802bd573786.png', '20230712070643', '77487a7b45c5ea2101d5876f1b07984b.png', 'Draft Dokumen Pendukung TA POLITALA (1).pdf');

-- --------------------------------------------------------

--
-- Table structure for table `surat_masuk`
--

CREATE TABLE `surat_masuk` (
  `id_surat` int(11) NOT NULL,
  `nomor_surat` varchar(30) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `perihal` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `lampiran` varchar(100) NOT NULL,
  `disposisi` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_masuk`
--

INSERT INTO `surat_masuk` (`id_surat`, `nomor_surat`, `tgl_masuk`, `perihal`, `keterangan`, `lampiran`, `disposisi`) VALUES
(2, '1/2', '2023-05-10', 'COBA INPUT LAGI', 'werr', 'Undangan Alumni FSI Al-Ikhwana.pdf', '1'),
(3, '111', '2023-05-10', 'COBA INPUT LAGIIIIIIIIIIII', '1', 'document.pdf', '3');

-- --------------------------------------------------------

--
-- Table structure for table `tb_belum_pernah_menikah`
--

CREATE TABLE `tb_belum_pernah_menikah` (
  `id_surat` int(11) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `nama_ayah` varchar(100) NOT NULL,
  `nama_ibu` varchar(100) NOT NULL,
  `tgl_dibuat` varchar(50) NOT NULL,
  `id_surat_keluar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_belum_pernah_menikah`
--

INSERT INTO `tb_belum_pernah_menikah` (`id_surat`, `no_surat`, `nik`, `nama_ayah`, `nama_ibu`, `tgl_dibuat`, `id_surat_keluar`) VALUES
(1, '001/Blmnkh/BM/IV/2023', '6301030506990001', 'randii', 'rinduu', '2023-04-17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_bepergian`
--

CREATE TABLE `tb_bepergian` (
  `id_surat` int(11) NOT NULL,
  `no_surat` varchar(30) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `alamat_tujuan` text NOT NULL,
  `kota` varchar(100) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `tgl_berangkat` date NOT NULL,
  `keperluan` text NOT NULL,
  `pengikut` varchar(5) NOT NULL,
  `tgl_dibuat` date NOT NULL,
  `id_surat_keluar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_domisili_organisasi`
--

CREATE TABLE `tb_domisili_organisasi` (
  `id_surat` int(11) NOT NULL,
  `no_surat` varchar(30) NOT NULL,
  `nama_organisasi` varchar(150) NOT NULL,
  `jenis_kegiatan` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `status_bangunan` varchar(100) NOT NULL,
  `peruntukan_bangunan` varchar(100) NOT NULL,
  `akta_pendirian` varchar(100) NOT NULL,
  `peserta` varchar(100) NOT NULL,
  `pj` varchar(100) NOT NULL,
  `tgl_berlaku` date NOT NULL,
  `tgl_dibuat` date NOT NULL,
  `id_surat_keluar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_identitas`
--

CREATE TABLE `tb_identitas` (
  `id_surat` int(11) NOT NULL,
  `no_surat` varchar(30) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `gol_darah` varchar(3) NOT NULL,
  `tgl_berlaku` date NOT NULL,
  `tgl_dibuat` date NOT NULL,
  `id_surat_keluar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_identitas`
--

INSERT INTO `tb_identitas` (`id_surat`, `no_surat`, `nik`, `gol_darah`, `tgl_berlaku`, `tgl_dibuat`, `id_surat_keluar`) VALUES
(3, '470/001/SKID', '6376892336491732', 'A', '2027-07-06', '2023-07-12', 99);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kehilangan`
--

CREATE TABLE `tb_kehilangan` (
  `id_surat` int(11) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `tanggal_hilang` varchar(18) NOT NULL,
  `jam` varchar(18) NOT NULL,
  `hari` varchar(30) NOT NULL,
  `lokasi` varchar(50) NOT NULL,
  `nama_barang_hilang` varchar(100) NOT NULL,
  `atas_nama` varchar(100) NOT NULL,
  `tgl_dibuat` date NOT NULL,
  `id_surat_keluar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kehilangan`
--

INSERT INTO `tb_kehilangan` (`id_surat`, `no_surat`, `nik`, `tanggal_hilang`, `jam`, `hari`, `lokasi`, `nama_barang_hilang`, `atas_nama`, `tgl_dibuat`, `id_surat_keluar`) VALUES
(7, '337/001/PEM/V/2023', '6301030506990001', '2023-05-13', '21:14', 'Selasa', 'batakan', 'tas', 'gapuri', '2023-05-13', 46),
(8, '337/002/PEM/VII/2023', '6301060208980006', '2023-07-04', '12:10', 'Selasa', 'Batu Mulya', 'Mesin Air', 'Ardi', '2023-07-11', 96),
(9, '337/002/PEM/VII/2023', '6301060208980006', '2023-07-04', '12:10', 'Selasa', 'Batu Mulya', 'Mesin Air', 'Ardi', '2023-07-11', 97),
(10, '337/002/PEM/VII/2023', '6301060208980006', '2023-07-04', '12:10', 'Selasa', 'Batu Mulya', 'Mesin Air', 'Ardi', '2023-07-11', 98);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kematian`
--

CREATE TABLE `tb_kematian` (
  `id_surat` int(11) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `nik` varchar(18) NOT NULL,
  `hari` varchar(50) NOT NULL,
  `tgl_kematian` varchar(30) NOT NULL,
  `jam` varchar(30) NOT NULL,
  `tempat_kematian` varchar(100) NOT NULL,
  `sebab_kematian` varchar(50) NOT NULL,
  `yang_menyatakan` varchar(50) NOT NULL,
  `keterangan_visum` varchar(50) NOT NULL,
  `tgl_dibuat` varchar(50) NOT NULL,
  `id_surat_keluar` int(11) NOT NULL,
  `desa` varchar(100) NOT NULL,
  `kec` varchar(100) NOT NULL,
  `kab` varchar(100) NOT NULL,
  `prov` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kematian`
--

INSERT INTO `tb_kematian` (`id_surat`, `no_surat`, `nik`, `hari`, `tgl_kematian`, `jam`, `tempat_kematian`, `sebab_kematian`, `yang_menyatakan`, `keterangan_visum`, `tgl_dibuat`, `id_surat_keluar`, `desa`, `kec`, `kab`, `prov`) VALUES
(2, '002/Kemat/BM/IV/2023', '6301030509140005', 'Jumat', '2023-04-27', '11:45', 'rumah', 'beheraan', 'aluh kacipung', 'panas burit', '2023-04-17', 33, '', '', '', ''),
(3, '472.12/003/PEM/V/2023', '6301030209100004', 'Senin', '2023-05-11', '09:22', 'rumah', 'beheraan', 'aluh kacipung', 'panas burit', '2023-05-11', 40, 'Batu Mulya', 'Panyipatan', 'Tanah Laut', 'Kalimantan Selamatan'),
(4, '472.12/473/PEM/VII/2023', '1180000077799999', 'Senin', '2023-07-10', '03:53', '1', '1', '1', '1', '2023-07-10', 95, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_lahiran`
--

CREATE TABLE `tb_lahiran` (
  `id_surat` int(11) NOT NULL,
  `no_surat` varchar(30) NOT NULL,
  `hari_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jam` varchar(10) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `jenis_kelahiran` varchar(30) NOT NULL,
  `kelahiran_ke` varchar(20) NOT NULL,
  `berat` varchar(20) NOT NULL,
  `panjang` varchar(20) NOT NULL,
  `lahir_di` varchar(100) NOT NULL,
  `nama_anak` varchar(100) NOT NULL,
  `nama_ibu` varchar(100) NOT NULL,
  `pekerjaan_ibu` varchar(100) NOT NULL,
  `nik_ibu` varchar(16) NOT NULL,
  `nama_ayah` varchar(100) NOT NULL,
  `pekerjaan_ayah` varchar(100) NOT NULL,
  `nik_ayah` varchar(16) NOT NULL,
  `alamat` text NOT NULL,
  `tgl_surat` date NOT NULL,
  `id_surat_keluar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_lahiran`
--

INSERT INTO `tb_lahiran` (`id_surat`, `no_surat`, `hari_lahir`, `tgl_lahir`, `jam`, `jenis_kelamin`, `jenis_kelahiran`, `kelahiran_ke`, `berat`, `panjang`, `lahir_di`, `nama_anak`, `nama_ibu`, `pekerjaan_ibu`, `nik_ibu`, `nama_ayah`, `pekerjaan_ayah`, `nik_ayah`, `alamat`, `tgl_surat`, `id_surat_keluar`) VALUES
(2, '145/002/SKL', 'Rabu', '2023-05-13', '', 'Laki-laki', 'sesar', '1', '1', '1', 'reta', 'tahu jua', 'rinduu', 'ani', '6301030506990001', 'randi', 'Petani', '6301030506990001', 'pelaihari', '2023-05-13', 43),
(3, '145/002/SKL', 'Rabu', '2023-05-13', '12:25', 'Laki-laki', 'sesar', '1', '1', '1', 'reta', 'tahu jua', 'rinduu', 'ani', '6301030506990001', 'randi', 'Petani', '6301030506990001', 'pelaihari', '2023-05-13', 44),
(4, '145/002/SKL', 'Rabu', '2023-05-13', '12:25', 'Laki-laki', 'sesar', '1', '1', '1', 'reta', 'tahu jua', 'rinduu', 'ani', '6301030506990001', 'randi', 'Petani', '6301030506990001', 'pelaihari', '2023-05-13', 45);

-- --------------------------------------------------------

--
-- Table structure for table `tb_penduduk`
--

CREATE TABLE `tb_penduduk` (
  `nik` varchar(18) NOT NULL,
  `nkk` varchar(16) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tgl_lahir` varchar(20) NOT NULL,
  `jenis_kelamin` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `status_perkawinan` varchar(50) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `agama` varchar(30) NOT NULL,
  `kewarganegaraan` varchar(50) NOT NULL,
  `no_hp` varchar(18) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_penduduk`
--

INSERT INTO `tb_penduduk` (`nik`, `nkk`, `nama`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `status_perkawinan`, `pekerjaan`, `agama`, `kewarganegaraan`, `no_hp`, `id_user`) VALUES
('1180000077799999', '7770000081199999', 'Ucup', 'Tanah Laut', '1998-08-11', 'LAKI-LAKI', 'Batu Mulya', 'BELUM KAWIN', 'Buruh', 'Islam', 'WNI', '0899956778', 17),
('1231231231231312', '6398765435620003', 'selamet', 'Batu Mulya', '1999-02-15', 'LAKI-LAKI', 'Dusun Sundawa', 'BELUM KAWIN', 'Buruh', 'Islam', 'WNI', '089888279843', 18),
('1234567891012345', '6323738986430080', 'Rahman', 'Batu Mulya', '1996-05-06', 'LAKI-LAKI', ' Desa Batu Mulya ', 'BELUM KAWIN', 'Buruh', 'Hindu', 'WNI', '086536274747', 21),
('2345678912345559', '2456789156786789', 'Siti', 'Batu Mulya ', '1973-06-13', 'PEREMPUAN', 'Dusun Sidomulyo', 'BELUM KAWIN', 'Petani', 'Islam', 'WNI', '08999665544', 24),
('6301060208980006', '6301060208980666', 'Ardi prasetio', 'Batu mulya', '1998-08-02', 'LAKI-LAKI', 'Batu Mulya ', 'BELUM KAWIN', 'Wiraswasta ', 'Kristen', 'WNI', '08223475544', 20),
('6376892336491732', '6347890100125678', 'Saipul', 'Batu Mulya', '1990-07-06', 'LAKI-LAKI', 'Dusun Wonosari', 'BELUM KAWIN', 'Petani', 'Islam', 'WNI', '089923768227', 23);

-- --------------------------------------------------------

--
-- Table structure for table `tb_penguburan`
--

CREATE TABLE `tb_penguburan` (
  `id_surat` int(11) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `hari` varchar(20) NOT NULL,
  `tanggal_meninggal` date NOT NULL,
  `tgl_dibuat` date NOT NULL,
  `id_surat_keluar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_penguburan`
--

INSERT INTO `tb_penguburan` (`id_surat`, `no_surat`, `nik`, `hari`, `tanggal_meninggal`, `tgl_dibuat`, `id_surat_keluar`) VALUES
(1, '001/Pengu/BM/IV/2023', '6301030509140005', 'Jumat', '2023-04-27', '2023-04-17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tidak_mampu`
--

CREATE TABLE `tb_tidak_mampu` (
  `id_surat` int(11) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `nik_ayah` varchar(16) NOT NULL,
  `penghasilan` varchar(15) NOT NULL,
  `tgl_dibuat` varchar(30) NOT NULL,
  `id_surat_keluar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_tidak_mampu`
--

INSERT INTO `tb_tidak_mampu` (`id_surat`, `no_surat`, `nik`, `nik_ayah`, `penghasilan`, `tgl_dibuat`, `id_surat_keluar`) VALUES
(1, '001/Tdkmpu/BM/IV/2023', '6301030506990001', 'aluhi', '', '2023-04-17', 0),
(2, '002/Tdkmpu/BM/IV/2023', '6301030509140005', 'aluhi', '', '2023-04-17', 36),
(3, '401/001/SKTM-BM/V/2023', '6301030506990001', '6301030509140005', '10000', '2023-05-15', 54),
(4, '401/002/SKTM-BM/VI/2023', '6301030209100004', '1231231231231231', '100000', '2023-06-15', 68);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tidak_memiliki_rumah`
--

CREATE TABLE `tb_tidak_memiliki_rumah` (
  `id_surat` int(11) NOT NULL,
  `no_surat` varchar(30) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `tgl_dibuat` date NOT NULL,
  `id_surat_keluar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tidak_memiliki_rumah`
--

INSERT INTO `tb_tidak_memiliki_rumah` (`id_surat`, `no_surat`, `nik`, `tgl_dibuat`, `id_surat_keluar`) VALUES
(1, '470/001/Kesra/V/2023', '6301036110000006', '2023-05-15', 58),
(2, '470/002/Kesra/VI/2023', '6301032507950001', '2023-06-20', 82),
(3, '470/003/Kesra/VI/2023', '1231231231231312', '2023-06-22', 84);

-- --------------------------------------------------------

--
-- Table structure for table `tb_usaha`
--

CREATE TABLE `tb_usaha` (
  `id_surat` int(11) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `nik` varchar(30) NOT NULL,
  `usaha` varchar(100) NOT NULL,
  `jenis_usaha` varchar(100) NOT NULL,
  `penanggung_jawab` varchar(100) NOT NULL,
  `alamat_usaha` varchar(100) NOT NULL,
  `akta` varchar(100) NOT NULL,
  `tgl_dibuat` varchar(30) NOT NULL,
  `id_surat_keluar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_usaha`
--

INSERT INTO `tb_usaha` (`id_surat`, `no_surat`, `nik`, `usaha`, `jenis_usaha`, `penanggung_jawab`, `alamat_usaha`, `akta`, `tgl_dibuat`, `id_surat_keluar`) VALUES
(1, '001/Usaha/BM/IV/2023', '6301030506990001', 'warung', '', '', 'pelaihari aku', 'ujian disekolah', '2023-04-17', 0),
(2, '002/Usaha/BM/IV/2023', '6301030509140005', 'warung', 'Penjualan', 'gapuri', 'pelaiharii', '123', '2023-04-17', 37),
(3, '517/003/KESRA/V/2023', '6301030506990001', 'warung', 'Penjualan', 'gapuri', 'pelaihari', '', '2023-05-11', 41),
(4, '517/004/KESRA/V/2023', '6301030506990001', 'warung', 'Penjualan', 'gapuri', 'pelaihari', '123', '2023-05-11', 42);

-- --------------------------------------------------------

--
-- Table structure for table `tb_yatim`
--

CREATE TABLE `tb_yatim` (
  `id_surat` int(11) NOT NULL,
  `no_surat` varchar(30) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `tgl_dibuat` date NOT NULL,
  `id_surat_keluar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `level` enum('Admin','Kades','Pengguna','RT') NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `email`, `level`, `status`) VALUES
(1, 'erin', '5f5be3890fa875bfe8fa797b4ba6a397', 'ahmadgapuri1231@gmail.com', 'Admin', 'Aktif'),
(3, '1231231231231231', 'b0d7c058dfeafe8f33a707d16e56c4de', '', 'Pengguna', 'Aktif'),
(6, '1231231231231312', '852b3c8800a302b6219ebbde56794e87', 'ahmadgapuri1231@gmail.com', 'Pengguna', 'Aktif'),
(15, 'rt', '822050d9ae3c47f54bee71b85fce1487', 'ahmadgapuri1231@gmail.com', 'RT', 'Aktif'),
(16, 'kades', '0cfa66469d25bd0d9e55d7ba583f9f2f', 'ahmadgapuri1231@gmail.com', 'Kades', 'Aktif'),
(17, '1180000077799999', 'b585821bcffdaf176945e238684ee233', 'Ucup@gmail.com', 'Pengguna', 'Aktif'),
(18, '1231231231231312', 'd56b699830e77ba53855679cb1d252da', 'selamet@gmail.com', 'Pengguna', 'Aktif'),
(19, '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', 'Admin', 'Aktif'),
(20, '1', 'c4ca4238a0b923820dcc509a6f75849b', 'ardi@gmail.com', 'Pengguna', 'Aktif'),
(21, '1234567891012345', 'd56b699830e77ba53855679cb1d252da', 'Rahman@gmail.com', 'Pengguna', 'Aktif'),
(22, '1234567891234567', 'd56b699830e77ba53855679cb1d252da', 'purnomo@gmail.com', 'Pengguna', 'Aktif'),
(23, '6376892336491732', '4905e84165b494d1117db3517b1f20a2', 'saipul@gmail.com', 'Pengguna', 'Aktif'),
(24, '2345678912345559', 'd56b699830e77ba53855679cb1d252da', 'siti@gmail.com', 'Pengguna', 'Aktif');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bagian`
--
ALTER TABLE `bagian`
  ADD PRIMARY KEY (`id_bagian`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`),
  ADD KEY `id_jabatan` (`id_jabatan`),
  ADD KEY `id_bagian` (`id_bagian`);

--
-- Indexes for table `permintaan`
--
ALTER TABLE `permintaan`
  ADD PRIMARY KEY (`id_permintaan`);

--
-- Indexes for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
  ADD PRIMARY KEY (`id_surat_keluar`);

--
-- Indexes for table `surat_masuk`
--
ALTER TABLE `surat_masuk`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `tb_belum_pernah_menikah`
--
ALTER TABLE `tb_belum_pernah_menikah`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `tb_bepergian`
--
ALTER TABLE `tb_bepergian`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `tb_domisili_organisasi`
--
ALTER TABLE `tb_domisili_organisasi`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `tb_identitas`
--
ALTER TABLE `tb_identitas`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `tb_kehilangan`
--
ALTER TABLE `tb_kehilangan`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `tb_kematian`
--
ALTER TABLE `tb_kematian`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `tb_lahiran`
--
ALTER TABLE `tb_lahiran`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `tb_penduduk`
--
ALTER TABLE `tb_penduduk`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `tb_penguburan`
--
ALTER TABLE `tb_penguburan`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `tb_tidak_mampu`
--
ALTER TABLE `tb_tidak_mampu`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `tb_tidak_memiliki_rumah`
--
ALTER TABLE `tb_tidak_memiliki_rumah`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `tb_usaha`
--
ALTER TABLE `tb_usaha`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `tb_yatim`
--
ALTER TABLE `tb_yatim`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permintaan`
--
ALTER TABLE `permintaan`
  MODIFY `id_permintaan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
  MODIFY `id_surat_keluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `surat_masuk`
--
ALTER TABLE `surat_masuk`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_belum_pernah_menikah`
--
ALTER TABLE `tb_belum_pernah_menikah`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_bepergian`
--
ALTER TABLE `tb_bepergian`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_domisili_organisasi`
--
ALTER TABLE `tb_domisili_organisasi`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_identitas`
--
ALTER TABLE `tb_identitas`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_kehilangan`
--
ALTER TABLE `tb_kehilangan`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_kematian`
--
ALTER TABLE `tb_kematian`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_lahiran`
--
ALTER TABLE `tb_lahiran`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_penguburan`
--
ALTER TABLE `tb_penguburan`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_tidak_mampu`
--
ALTER TABLE `tb_tidak_mampu`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_tidak_memiliki_rumah`
--
ALTER TABLE `tb_tidak_memiliki_rumah`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_usaha`
--
ALTER TABLE `tb_usaha`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_yatim`
--
ALTER TABLE `tb_yatim`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD CONSTRAINT `pegawai` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
