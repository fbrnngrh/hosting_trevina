<?php
ini_set("display_errors",0);
include("includes/defines.php");
include("includes/fungsi.php");
include("includes/tgl_indo.php");
include("includes/fungsi_rupiah.php");
cekSession();
cekSession();

?>


<!DOCTYPE html>
<html lang="en">
<?php include 'header.php' ?>
<body id="page-top">

    <div id="wrapper">

     <?php include ('proses/menu/menu_admin.php')?>


     <!-- End of Topbar -->
     <div class="container-fluid">


        <h1 class="h3 mb-2 text-gray-800">Tabel Surat Masuk</h1>
        <p class="mb-4">Kamu Bisa menggunakan tambah, ubah, hapus</p>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data Surat Masuk</h6>
                <br> <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Tambah
                </button><br>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor Surat</th>
                            <th>Tanggal Surat Masuk</th>
                            <th>Perihal</th>
                            <th>Keterangan</th>
                            <th>Lampiran</th>
                            <th>Disposisi</th>
                            <th>Aksi</th>



                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $query = mysqli_query($con,"SELECT * FROM surat_masuk");
                        $no = 1;

                        while ($data = mysqli_fetch_assoc($query)) {?> 
                            <tr>
                                <td> <?php echo $no++ ?></td> 
                                <td> <?php echo $data['nomor_surat']; ?></td>
                                <td> <?= tanggal_indo($data['tgl_masuk']); ?> </td>
                                <td> <?php echo $data['perihal']; ?></td>
                                <td> <?php echo $data['keterangan']; ?></td>
                                <td> <a href="surat_masuk/<?php echo $data['lampiran']; ?>" target='_blank'><?php echo $data['lampiran']; ?></a>
                                </td>
                                <td><?php 
                                if ($data['disposisi']=='-') {
                                    ?>
                                    <a  href ="#disposisi" data-toggle="modal" data-id="<?php echo $data['id_surat'];?>" data-target="#disposisi"><button type="button" rel="tooltip" class="btn btn-success btn-sm">
                                        <i class="fa fa-eye"></i>
                                    </button></a>
                                    <?php
                                }else{
                                    $disposisi=$data['disposisi'];
                                    $c=mysqli_fetch_array(mysqli_query($con,"SELECT nama_pegawai FROM pegawai where id_pegawai='$disposisi'"));
                                    echo $c['nama_pegawai'];
                                }
                                ?>
                            </td>
                            <td>
                                <?php 
                                if ($data['disposisi']!='-') {
                                    ?>
                                    <a target="_blank" href="cetak/disposisi.php?id=<?php echo $data['id_surat'] ?>"><button class="btn btn-secondary btn-sm" title="Cetak"><i class="fa fa-print"></i></button></a>
                                    <?php 
                                }
                                ?>
                                <a  href ="#edit" data-toggle="modal" data-id="<?php echo $data['id_surat'];?>" data-target="#edit"><button type="button" rel="tooltip" class="btn btn-primary btn-sm">
                                    <i class="fa fa-edit"></i>
                                </button></a>
                                <a  href="#" onclick="confirm_modal('proses/crud/hapus_surat_masuk.php?id=<?php echo $data['id_surat'];?>');"><button type="button" rel="tooltip" class="btn btn-danger btn-sm">
                                    <i class="fas fa-trash"></i>
                                </button></a>
                            </td>
                        </tr>
                    <?php  } ?>
                </tbody>
                
            </table>
        </div>
    </div>
</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
           <span>Copyright &copy; by Trevina Sabrina Erin </span>
       </div>
   </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<?php include ('proses/modal/modal_keluar.php')?>
<?php include('footer.php')?>

</body>

</html>
<?php 

include("proses/modal/surat_masuk.php");

?>