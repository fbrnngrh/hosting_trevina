<?php
// ini_set("display_errors",0);
include("includes/defines.php");
include("includes/fungsi.php");
include("includes/tgl_indo.php");
include("includes/fungsi_rupiah.php");
cekSession();
$id_user=$_SESSION['id_user'];
$cari=mysqli_fetch_array(mysqli_query($con, "SELECT * from tb_penduduk where id_user='$id_user'"));
$nik=$cari['nik'];

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Data Surat Keluar</title>

    <link rel="icon" type="image/png" href="login2/surat.png"/>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">


    <div id="wrapper">

       <?php include ('proses/menu/menu_pengguna.php')?>


       <!-- End of Topbar -->
       <div class="container-fluid">


        <h1 class="h3 mb-2 text-gray-800">Tabel Surat Keluar</h1>
        <p class="mb-4">Kamu Bisa menggunakan tambah, ubah, hapus</p>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data Surat Keluar</h6>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                       <thead>
                        <tr>
                            <th>No</th>
                            <th>No Surat</th>
                            <th>NIK</th>
                            <th>Jenis Surat</th>
                            <th>Tanggal Dibuat</th>
                            <th>Aksi</th>


                        </tr>
                    </thead>

                    <?php


                    $query = mysqli_query($con,"SELECT s.*, k.no_surat kno,k.tgl_dibuat ktgl,k.nik knik, p.no_surat pno,p.tgl_dibuat ptgl, p.nik pnik, u.no_surat uno,u.tgl_dibuat utgl,u.nik unik, d.no_surat dno,d.tgl_dibuat dtgl,d.nik dnik, b.no_surat bno,b.tgl_dibuat btgl,b.nik bnik,pi.no_surat pino,pi.tgl_dibuat pitgl,pi.nik pinik,tm.no_surat tmno,tm.tgl_dibuat tmtgl,tm.nik tmnik,us.no_surat usno,us.tgl_dibuat ustgl, us.nik usnik
                       FROM surat_keluar s
                       LEFT JOIN tb_kehilangan k ON s.id_surat_keluar = k.id_surat_keluar and k.nik='$nik'
                       LEFT JOIN tb_kematian p ON s.id_surat_keluar = p.id_surat_keluar and p.nik='$nik'
                       LEFT JOIN tb_penguburan u ON s.id_surat_keluar = u.id_surat_keluar and u.nik='$nik'
                       LEFT JOIN tb_domisili d ON s.id_surat_keluar = d.id_surat_keluar and d.nik='$nik'
                       LEFT JOIN tb_belum_pernah_menikah b ON s.id_surat_keluar = b.id_surat_keluar and b.nik='$nik'
                       LEFT JOIN tb_pindah pi ON s.id_surat_keluar = pi.id_surat_keluar and pi.nik='$nik'
                       LEFT JOIN tb_tidak_mampu tm ON s.id_surat_keluar = tm.id_surat_keluar and tm.nik='$nik'
                       LEFT JOIN tb_usaha us ON s.id_surat_keluar = us.id_surat_keluar and us.nik='$nik' where k.nik!='' or p.nik!='' or u.nik!='' or d.nik!='' or b.nik!='' or pi.nik!='' or tm.nik!='' or us.nik!=''");
                    $no = 1;

                    while ($data = mysqli_fetch_array($query)) {
                        // var_dump($data);exit;
                        if ($data['jenis_surat']=='Kehilangan') {
                            $no_surat=$data['kno'];
                            $tgl_dibuat=$data['ktgl'];
                            $nik=$data['knik'];
                            $cetak='kehilangan.php';
                            $edit='ubah_kehilangan.php';
                            $hapus='hapus_kehilangan.php';
                        }elseif ($data['jenis_surat']=='Kematian') {
                            $no_surat=$data['pno'];
                            $tgl_dibuat=$data['ptgl'];
                            $nik=$data['pnik'];
                            $cetak='kematian.php';
                            $edit='ubah_kematian.php';
                            $hapus='hapus_kematian.php';
                        }elseif ($data['jenis_surat']=='Penguburan') {
                            $no_surat=$data['uno'];
                            $tgl_dibuat=$data['utgl'];
                            $nik=$data['unik'];
                            $cetak='penguburan.php';
                            $edit='ubah_penguburan.php';
                            $hapus='hapus_penguburan.php';
                        }elseif ($data['jenis_surat']=='Pindah') {
                            $no_surat=$data['pino'];
                            $tgl_dibuat=$data['pitgl'];
                            $nik=$data['pinik'];
                            $cetak='pindah.php';
                            $edit='ubah_pindah.php';
                            $hapus='hapus_pindah.php';
                        }elseif ($data['jenis_surat']=='Tidak Mampu') {
                            $no_surat=$data['tmno'];
                            $tgl_dibuat=$data['tmtgl'];
                            $nik=$data['tmnik'];
                            $cetak='tidak_mampu.php';
                            $edit='ubah_tidak_mampu.php';
                            $hapus='hapus_tidak_mampu.php';
                        }elseif ($data['jenis_surat']=='Usaha') {
                            $no_surat=$data['usno'];
                            $tgl_dibuat=$data['ustgl'];
                            $nik=$data['usnik'];
                            $cetak='usaha.php';
                            $edit='ubah_usaha.php';
                            $hapus='hapus_usaha.php';
                        }elseif ($data['jenis_surat']=='Domisili') {
                            $no_surat=$data['dno'];
                            $tgl_dibuat=$data['dtgl'];
                            $nik=$data['dnik'];
                            $cetak='domisili.php';
                            $edit='ubah_domisili.php';
                            $hapus='hapus_domisili.php';
                        }elseif ($data['jenis_surat']=='Belum Nikah') {
                            $no_surat=$data['bno'];
                            $tgl_dibuat=$data['btgl'];
                            $nik=$data['bnik'];
                            $cetak='belum_menikah.php';
                            $edit='ubah_belum_menikah.php';
                            $hapus='hapus_belum_menikah.php';
                        }
                        ?> 
                        <tbody>
                            <tr>
                                <td> <?php echo $no++ ?></td> 
                                <td> <?php echo $no_surat; ?></td>
                                <td> <?php echo $nik; ?></td>
                                <td> <?php echo $data['jenis_surat']; ?></td>
                                <td> <?= tanggal_indo($tgl_dibuat); ?> </td>
                                <td>
                                    <a target="_blank" href="cetak/<?php echo $cetak ?>?id=<?php echo $data['id_surat_keluar'] ?>"><button class="btn btn-secondary btn-sm" title="Cetak"><i class="fa fa-print"></i></button></a>
                                </td>

                            </tr>

                        </tbody>
                    <?php  } ?>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
         <span>Copyright &copy; by Trevina Sabrina Erin </span>
     </div>
 </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<?php include ('proses/modal/modal_keluar.php')?>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="vendor/datatables/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="js/demo/datatables-demo.js"></script>

</body>

</html>
<?php 

include("proses/modal/permintaan_pengguna.php");

?>