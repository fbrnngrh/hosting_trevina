<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Contoh Form Input Dinamis dengan Ajax</title>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
  <script>
    $(document).ready(function() {
      $("#inputType").change(function() {
        var inputType = $(this).val();
        $.ajax({
          url: "get_form_input.php",
          type: "POST",
          data: { inputType: inputType },
          success: function(data) {
            $("#form-input").html(data);
          }
        });
      });
    });
  </script>
</head>
<body>
  <label for="inputType">Jenis Input:</label>
  <select name="inputType" class="select2" id="inputType">
    <option value="">- Pilih Jenis Input -</option>
    <option value="text">Teks</option>
    <option value="number">Angka</option>
    <option value="email">Email</option>
  </select>
  <br>
  <div id="form-input"></div>
</body>
<script>
$(document).ready(function() {
    $('.select2').select2();
});
</script>
</html>
