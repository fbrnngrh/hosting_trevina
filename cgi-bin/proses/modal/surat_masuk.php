
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" arial-labelledby="myModalLabel" arial-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <div class="add-surat-masuk"></div>
        </div>
    </div>
</div>
</div>



<div class="modal fade" id="edit" tabindex="-1" role="dialog" arial-labelledby="myModalLabel" arial-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <div class="edit-surat-masuk"></div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="disposisi" tabindex="-1" role="dialog" arial-labelledby="myModalLabel" arial-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <div class="disposisi-surat-masuk"></div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="modal_delete">
  <div class="modal-dialog">
    <div class="modal-content" style="margin-top:100px;">
      <div class="modal-header">
        <h4 class="modal-title" style="text-align:center;">Apakah Kamu Yakin ingin Menghapus Data Ini?</h4>
    </div>
    
    <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
        <a href="#" class="btn btn-danger" id="delete_link">IYA</a>
        <button type="submit" class="btn btn-fill btn-inverse" data-dismiss="modal">TIDAK</button>
    </div>
</div>
</div>
</div>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<!-- jika menggunakan bootstrap4 gunakan css ini  -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript">

    $(document).ready(function()
    {
        $('#tambah').on('show.bs.modal', function(e)
        {
            $.ajax( // fungsi ajak untuk mengambil data
            {
                url : 'proses/crud/tambah_surat_masuk.php',
                success : function(data)
                {
                    $('.add-surat-masuk').html(data); // menampilkan data ke dalam modal
                }
            });
        });
    });

    $(document).ready(function()
    {
        $('#edit').on('show.bs.modal', function(e)
        {
            var id_surat_masuk = $(e.relatedTarget).data('id');

            $.ajax( // fungsi ajak untuk mengambil data
            {
                type : 'post',
                url : 'proses/crud/ubah_surat_masuk.php',
                data : 'id_surat_masuk='+id_surat_masuk,
                success : function(data)
                {
                    $('.edit-surat-masuk').html(data); // menampilkan data ke dalam modal
                }
            });
        });
    });

    $(document).ready(function()
    {
        $('#disposisi').on('show.bs.modal', function(e)
        {
            var id_surat_masuk = $(e.relatedTarget).data('id');

            $.ajax( // fungsi ajak untuk mengambil data
            {
                type : 'post',
                url : 'proses/crud/disposisi_surat_masuk.php',
                data : 'id_surat_masuk='+id_surat_masuk,
                success : function(data)
                {
                    $('.disposisi-surat-masuk').html(data); // menampilkan data ke dalam modal
                }
            });
        });
    });
</script>

<script type="text/javascript">
    function confirm_modal(delete_url)
    {
      $('#modal_delete').modal('show', {backdrop: 'static'});
      document.getElementById('delete_link').setAttribute('href' , delete_url);
  }
</script>