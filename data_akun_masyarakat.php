<?php
ini_set("display_errors",0);
include("includes/defines.php");
include("includes/fungsi.php");
cekSession();

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Data Akun Masyarakat</title>

    <link rel="icon" type="image/png" href="login2/surat.png"/>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">


    <div id="wrapper">

     <?php include ('proses/menu/menu_admin.php')?>


     <!-- End of Topbar -->
     <div class="container-fluid">


        <h1 class="h3 mb-2 text-gray-800">Tabel Akun Masyarakat</h1>
        <p class="mb-4">Kamu Bisa menggunakan tambah, ubah, hapus</p>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data Akun Masyarakat</h6>
               <!--  <br> <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Tambah 
               </button><br> -->
           </div>
           <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                 <thead>
                    <tr>
                        <th>No</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Level</th>
                        <th>Status</th>
                        <th>Aksi</th>

                    </tr>
                </thead>

                <?php
                $query = mysqli_query($con,"SELECT * FROM user
                    where level = 'Pengguna'
                    ORDER BY id_user asc");
                $no = 1;

                while ($data = mysqli_fetch_assoc($query)) {?> 
                    <tbody>
                        <tr>
                            <td> <?php echo $no++ ?></td> 
                            <td> <?php echo $data['username']; ?></td>
                            <td> <?php echo $data['password']; ?></td>
                            <td>    <?php
                            if ($data['level'] == Pengguna){
                                echo ' <a href="#" class="btn btn-info btn-icon-split btn-sm">
                                <span class="icon text-white-50">
                                <i class="fas fa-check"></i>
                                </span>
                                <span class="text">Pengguna</span>
                                </a>';

                            } 
                        ?> </td>
                        <td>    <?php
                        if ($data['status'] == Nonaktif){
                            echo ' <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                            <i class="fas fa-check"></i>
                            </span>
                            <span class="text">Nonaktif</span>
                            </a>';

                        } elseif ($data['status'] == Aktif) {
                            echo ' <a href="#" class="btn btn-primary btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                            <i class="fas fa-check"></i>
                            </span>
                            <span class="text">Aktif</span>
                            </a>';
                        }
                    ?> </td>
                    <td class="td-actions text-center">
                     <a  href ="#edit" data-toggle="modal" data-id="<?php echo $data['id_user'];?>" data-target="#edit"><button type="button" rel="tooltip" class="btn btn-success btn-round">
                        <i class="fa fa-edit"></i>
                    </button></a>

                    <a  href="#" onclick="confirm_modal('proses/crud/hapus_akun_masyarakat.php?id=<?php echo $data['id_user'];?>');"><button type="button" rel="tooltip" class="btn btn-danger btn-round remove">
                        <i class="fas fa-trash"></i>
                    </button></a>


                </td>
            </tr>

        </tbody>
    <?php  } ?>
</table>
</div>
</div>
</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
           <span>Copyright &copy; by Trevina Sabrina Erin </span>
       </div>
   </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<?php include ('proses/modal/modal_keluar.php')?>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="vendor/datatables/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="js/demo/datatables-demo.js"></script>

</body>

</html>
<?php 

include("proses/modal/pengguna.php");

?>