﻿  <?php
  include ("../../includes/defines.php");
  if(isset($_POST['iduser']))
  {
    $iduser=$_POST['iduser'];
    $query=mysqli_query($con,"SELECT * FROM user WHERE id_user=$iduser");
    $data=mysqli_fetch_assoc($query);
}
?>        

<div class="card-header"><strong>UBAH DATA PENGGUNA</strong></div>
<div class="card-body card-block">
 <form action="proses/crud/proses_ubah_user.php" method="POST" enctype="multipart/form-data" >
      <div class="form-group">
        <!-- <label for="company" class=" form-control-label">Pengguna</label> -->
        <input hidden="hidden" type="text" name="id_user" value="<?php echo $data['id_user']; ?>">

            <div class="form-group">
                <label for="company" class=" form-control-label">Username</label><input type="text" name="username" placeholder="Masukkan data username" class="form-control" value="<?php echo $data['username']; ?>" required>
            </div>
            <div class="form-group">
                <label for="company" class=" form-control-label">Password</label><input type="password" name="password" placeholder="Masukkan data password" class="form-control" value="<?php echo $data['password']; ?>" required>
            </div>
            <div class="form-group">
                <label for="company" class=" form-control-label">Level</label>
                <fieldset class="controls">
                    <label class="custom-control custom-radio">
                        <input type="radio" name="level" value="Admin" required class="form-check-input" <?php if($data['level']=="Admin"){echo "checked";}?>> <span class="custom-control-indicator"></span> <span class="custom-control-description"> Admin</span> </label>
                    </fieldset>
                    <fieldset>
                        <label class="custom-control custom-radio">
                            <input type="radio" name="level" value="Petugas" class="form-check-input" <?php if($data['level']=="Petugas"){echo "checked";}?>> <span class="custom-control-indicator"></span> <span class="custom-control-description">Petugas</span> </label>
                        </fieldset>
                        <fieldset>
                            <label class="custom-control custom-radio">
                                <input type="radio" name="level" value="RT" class="form-check-input" <?php if($data['level']=="RT"){echo "checked";}?>> <span class="custom-control-indicator"></span> <span class="custom-control-description">RT</span> </label>
                            </fieldset>
                            <fieldset>
                                <label class="custom-control custom-radio">
                                    <input type="radio" name="level" value="Kades" class="form-check-input" <?php if($data['level']=="Kades"){echo "checked";}?>> <span class="custom-control-indicator"></span> <span class="custom-control-description">Kades</span> </label>
                                </fieldset>
                            </div>


                            <div class="card-footer">

                             <button type="submit" value="Simpan" name="update" class="btn btn-primary btn-sm fa fa-dot-circle-o"> Simpan</button>
                             <button type="submit"  class="btn btn-danger btn-sm" data-dismiss="modal">Kembali</button>
                         </div>

                     </form>


                 </div>
