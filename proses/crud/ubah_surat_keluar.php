<?php
ini_set("display_errors",0);
include("../../includes/defines.php");
include("../../includes/fungsi.php");
session_start();
$id_surat_keluar=$_POST['id_surat_keluar'];
$c=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM surat_keluar where id_surat_keluar='$id_surat_keluar'"));
// $id_surat=$c['id_surat'];
?>


<script>
  $(document).ready(function() {
    $('.select2').select2({theme: 'bootstrap4'});
  });
</script>
<div class="card-header"><strong>EDIT DATA</strong></div>
<div class="card-body card-block">
  <form action="proses/crud/proses_ubah_surat_keluar.php" method="POST" enctype="multipart/form-data" >

    <input type="hidden" class="form-control" value="<?php echo $id_surat_keluar ?>" name="id_surat_keluar">
    <input type="hidden" class="form-control" value="<?php echo $c['jenis_surat'] ?>" name="jenis_surat">
    <?php
    if ($c['jenis_surat']=='Kehilangan') {
      $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_kehilangan a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))
      ?>

      <div class="form-group">
        <label>Nomor Surat</label>
        <input type="text" class="form-control" value="<?php echo $d['no_surat'] ?>" readonly name="no_surat">
      </div>
      <div class="form-group">
        <label>NIK</label>
        <select class="form-control select2" name="nik">
         <?php 
         $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
         while ($dt=mysqli_fetch_array($cari)) {
          ?>
          <option <?php echo $d['nik']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
          <?php
        }
        ?>


      </select>
    </div>
    <div class="form-group">
      <label>Tanggal Kehilangan</label>
      <input type="date" class="form-control" value="<?php echo $d['tanggal_hilang'] ?>" name="tanggal_hilang">
    </div>

    <div class="form-group">
      <label>Jam</label>
      <input type="time" class="form-control" value="<?php echo $d['jam'] ?>" name="jam">
    </div>

    <div class="form-group">
      <label>Hari</label>
      <select class="form-control" name="hari">
        <option <?php echo $d['hari']=='Senin'?'selected':''; ?>>Senin</option>
        <option <?php echo $d['hari']=='Selasa'?'selected':''; ?>>Selasa</option>
        <option <?php echo $d['hari']=='Rabu'?'selected':''; ?>>Rabu</option>
        <option <?php echo $d['hari']=='Kamis'?'selected':''; ?>>Kamis</option>
        <option <?php echo $d['hari']=='Jumat'?'selected':''; ?>>Jumat</option>
        <option <?php echo $d['hari']=='Sabtu'?'selected':''; ?>>Sabtu</option>
        <option <?php echo $d['hari']=='Minggu'?'selected':''; ?>>Minggu</option>
      </select>
    </div>
    <div class="form-group">
      <label>Lokasi</label>
      <input type="text" class="form-control" value="<?php echo $d['lokasi'] ?>" name="lokasi">
    </div>
    <div class="form-group">
      <label>Nama Barang Hilang</label>
      <input type="text" class="form-control" value="<?php echo $d['nama_barang_hilang'] ?>" name="nama_barang_hilang">
    </div>
    <div class="form-group">
      <label>Atas Nama</label>
      <input type="text" class="form-control" value="<?php echo $d['atas_nama'] ?>" name="atas_nama">
    </div>
    <div class="form-group">
      <label>Tanggal Dibuat</label>
      <input type="text" class="form-control" value="<?php echo $d['tgl_dibuat'] ?>" readonly name="tgl_dibuat">
    </div>
    <?php
  }elseif($c['jenis_surat']=='Kematian'){
    $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_kematian a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))
    ?>
    
    <div class="form-group">
      <label>Nomor Surat</label>
      <input type="text" class="form-control" value="<?php echo $d['no_surat'] ?>" readonly name="no_surat">
    </div>
    <div class="form-group">
      <label>NIK</label>
      <select class="form-control select2" name="nik">
       <?php 
       $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
       while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option <?php echo $d['nik']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>


    </select>
  </div>
  <div class="form-group">
    <label>Hari</label>
    <select class="form-control" name="hari">
      <option <?php echo $d['hari']=='Senin'?'selected':''; ?>>Senin</option>
      <option <?php echo $d['hari']=='Selasa'?'selected':''; ?>>Selasa</option>
      <option <?php echo $d['hari']=='Rabu'?'selected':''; ?>>Rabu</option>
      <option <?php echo $d['hari']=='Kamis'?'selected':''; ?>>Kamis</option>
      <option <?php echo $d['hari']=='Jumat'?'selected':''; ?>>Jumat</option>
      <option <?php echo $d['hari']=='Sabtu'?'selected':''; ?>>Sabtu</option>
      <option <?php echo $d['hari']=='Minggu'?'selected':''; ?>>Minggu</option>
    </select>
  </div>
  <div class="form-group">
    <label> Tanggal Meninggal</label>
    <input type="date" class="form-control" value="<?php echo $d['tgl_kematian'] ?>" name="tgl_kematian">
  </div>
  <div class="form-group">
    <label>Jam</label>
    <input type="time" class="form-control" value="<?php echo $d['jam'] ?>" name="jam">
  </div>
  <div class="form-group">
    <label>Tempat Kematian</label>
    <input type="text" class="form-control" value="<?php echo $d['tempat_kematian'] ?>" name="tempat_kematian">
  </div>
  <div class="form-group">
    <label>Sebab Kematian</label>
    <input type="text" class="form-control" value="<?php echo $d['sebab_kematian'] ?>" name="sebab_kematian">
  </div>
  <div class="form-group">
    <label>Yang Menyatakan</label>
    <input type="text" class="form-control" value="<?php echo $d['yang_menyatakan'] ?>" name="yang_menyatakan">
  </div>
  <div class="form-group">
    <label>Keterangan Visum</label>
    <input type="text" class="form-control" value="<?php echo $d['keterangan_visum'] ?>" name="keterangan_visum">
  </div>
  <div class="form-group">
    <label>Desa</label>
    <input type="text" class="form-control" value="<?php echo $d['desa'] ?>" name="desa">
  </div>
  <div class="form-group">
    <label>Kecamatan</label>
    <input type="text" class="form-control" value="<?php echo $d['kec'] ?>" name="kec">
  </div>
  <div class="form-group">
    <label>Kabupaten</label>
    <input type="text" class="form-control" value="<?php echo $d['kab'] ?>" name="kab">
  </div>
  <div class="form-group">
    <label>Provinsi</label>
    <input type="text" class="form-control" value="<?php echo $d['prov'] ?>" name="prov">
  </div>
  <div class="form-group">
    <label>Tanggal Dibuat</label>
    <input type="text" class="form-control" name="tgl_dibuat" value="<?php echo $d['tgl_dibuat'] ?>" readonly>
  </div>
  <?php
}elseif($c['jenis_surat']=='Penguburan'){
  $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_penguburan a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))
  ?>
  <div class="form-group">
    <label>No Surat</label>
    <input type="text" class="form-control" value="<?php echo $d['no_surat'] ?>" readonly name="no_surat">
  </div>
  <!-- text input -->
  <div class="form-group">
    <label>NIK</label>
    <select class="form-control select2" name="nik">
     <?php 
     $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
     while ($dt=mysqli_fetch_array($cari)) {
      ?>
      <option <?php echo $d['nik']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
      <?php
    }
    ?>


  </select>
</div>
<div class="form-group">
  <label>Hari</label>
  <select class="form-control" name="hari">
    <option <?php echo $d['hari']=='Senin'?'selected':''; ?>>Senin</option>
    <option <?php echo $d['hari']=='Selasa'?'selected':''; ?>>Selasa</option>
    <option <?php echo $d['hari']=='Rabu'?'selected':''; ?>>Rabu</option>
    <option <?php echo $d['hari']=='Kamis'?'selected':''; ?>>Kamis</option>
    <option <?php echo $d['hari']=='Jumat'?'selected':''; ?>>Jumat</option>
    <option <?php echo $d['hari']=='Sabtu'?'selected':''; ?>>Sabtu</option>
    <option <?php echo $d['hari']=='Minggu'?'selected':''; ?>>Minggu</option>
  </select>
</div>
<div class="form-group">
  <label>Tanggal Meninggal</label>
  <input type="date" class="form-control" value="<?php echo $d['tanggal_meninggal'] ?>" name="tanggal_meninggal">
</div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="date" class="form-control" readonly value="<?php echo $d['tgl_dibuat'] ?>" name="tgl_dibuat">
</div>
<?php
}elseif($c['jenis_surat']=='Pindah'){
  $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_pindah a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))
  ?>
  <div class="form-group">
    <label>No Surat Pindah</label>
    <input type="text" class="form-control" value="<?php echo $d['no_surat'] ?>" readonly name="no_surat">
  </div>
  <div class="form-group">
    <label>NIK</label>
    <select class="form-control select2" name="nik">
     <?php 
     $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
     while ($dt=mysqli_fetch_array($cari)) {
      ?>
      <option <?php echo $d['nik']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
      <?php
    }
    ?>


  </select>
</div>

<div class="form-group">
  <label>Tanggal Pindah</label>
  <input type="date" class="form-control" value="<?php echo $d['pada_tanggal'] ?>" name="pada_tanggal">
</div>
<div class="form-group">
  <label>Alamat Tujuan Pindah</label>
  <input type="text" class="form-control" value="<?php echo $d['alamat_tujuan_pindah'] ?>" name="alamat_tujuan_pindah">
</div>
<div class="form-group">
  <label>Alasan Pindah</label>
  <input type="text" class="form-control" value="<?php echo $d['alasan_pindah'] ?>" name="alasan_pindah">
</div>
<div class="form-group">
  <label>Jumlah Pengikut</label>
  <input type="text" class="form-control" value="<?php echo $d['jumlah_pengikut'] ?>" onkeypress="return hanyaAngka(event)" name="jumlah_pengikut">
</div>
<div class="form-group">
  <label>Tgl Dibuat</label>
  <input type="date" class="form-control" readonly value="<?php echo $d['tgl_dibuat'] ?>" name="tgl_dibuat">
</div>
<?php
}elseif($c['jenis_surat']=='Tidak Mampu'){
  $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_tidak_mampu a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))
  ?>
  <div class="form-group">
    <label>Nomor Surat</label>
    <input type="text" class="form-control" name="no_surat" value="<?php echo $d['no_surat']; ?>" readonly>
  </div>
  <div class="form-group">
    <label>NIK</label>
    <select class="form-control select2" name="nik">
     <?php 
     $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
     while ($dt=mysqli_fetch_array($cari)) {
      ?>
      <option <?php echo $d['nik']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
      <?php
    }
    ?>
  </select>
</div>
<div class="form-group">
  <label>NIK Ayah</label>
  <select class="form-control select2" name="nik_ayah">
    <option value="" disabled="" selected="">--Pilih Penduduk--</option>
    <?php 
    $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
    while ($dt=mysqli_fetch_array($cari)) {
      ?>
      <option <?php echo $d['nik_ayah']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
      <?php
    }
    ?>
  </select>
</div>
<div class="form-group">
  <label>Penghasilan</label>
  <input type="text" class="form-control" value="<?php echo $d['penghasilan']; ?>" name="penghasilan">
</div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="text" class="form-control" name="tgl_dibuat" value="<?php echo $d['tgl_dibuat']; ?>" readonly>
</div>
<?php
}elseif($c['jenis_surat']=='Usaha'){
  $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_usaha a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))
  ?>
  <div class="form-group">
    <label>Nomor Surat</label>
    <input type="text" class="form-control" name="no_surat" value="<?php echo $d['no_surat']; ?>" readonly>
  </div>
  <div class="form-group">
    <label>NIK</label>
    <select class="form-control select2" name="nik">
     <?php 
     $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
     while ($dt=mysqli_fetch_array($cari)) {
      ?>
      <option <?php echo $d['nik']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
      <?php
    }
    ?>
  </select>
</div>
<div class="form-group">
  <label>Usaha</label>
  <input type="text" class="form-control" value="<?php echo $d['usaha']; ?>" name="usaha">
</div>
<div class="form-group">
  <label>Jenis Usaha</label>
  <input type="text" class="form-control" value="<?php echo $d['jenis_usaha']; ?>" name="jenis_usaha">
</div>
<div class="form-group">
  <label>Penanggung Jawab</label>
  <input type="text" class="form-control" value="<?php echo $d['penanggung_jawab']; ?>" name="penanggung_jawab">
</div>
<div class="form-group">
  <label>Alamat Usaha</label>
  <input type="text" class="form-control" value="<?php echo $d['alamat_usaha']; ?>" name="alamat_usaha">
</div>
<div class="form-group">
  <label>Akta Berdiri</label>
  <input type="text" class="form-control" value="<?php echo $d['akta']; ?>" name="akta">
</div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="date" class="form-control" name="tgl_dibuat" value="<?php echo $d['tgl_dibuat']; ?>" readonly>
</div>

<?php
}elseif($c['jenis_surat']=='Domisili Organisasi'){
  $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_domisili_organisasi a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))
  ?>
  <div class="form-group">
    <label>Nomor Surat</label>
    <input type="text" class="form-control" name="no_surat" value="<?php echo $d['no_surat']; ?>" readonly>
  </div>
  <div class="form-group">
    <label>Nama Organisasi</label>
    <input type="text" class="form-control" value="<?php echo $d['nama_organisasi']; ?>" name="nama_organisasi">
  </div>
  <div class="form-group">
    <label>Jenis Kegiatan</label>
    <input type="text" class="form-control" value="<?php echo $d['jenis_kegiatan']; ?>" name="jenis_kegiatan">
  </div>
  <div class="form-group">
    <label>Alamat Organisai</label>
    <input type="text" class="form-control" value="<?php echo $d['alamat']; ?>" name="alamat_organisasi">
  </div>
  <div class="form-group">
    <label>Status Bangunan</label>
    <input type="text" class="form-control" value="<?php echo $d['status_bangunan']; ?>" name="status_bangunan">
  </div>
  <div class="form-group">
    <label>Peruntukan Bangunan</label>
    <input type="text" class="form-control" value="<?php echo $d['peruntukan_bangunan']; ?>" name="peruntukan_bangunan">
  </div>
  <div class="form-group">
    <label>Akta Pendirian</label>
    <input type="text" class="form-control" value="<?php echo $d['akta_pendirian']; ?>" name="akta_pendirian">
  </div>
  <div class="form-group">
    <label>Peserta</label>
    <input type="text" class="form-control" value="<?php echo $d['peserta']; ?>" name="peserta">
  </div>
  <div class="form-group">
    <label>Penanggung Jawab</label>
    <select class="form-control select2" name="pj">
      <option value="" disabled="" selected="">--Pilih Penanggung Jawab--</option>
      <?php 
      $cari=mysqli_query($con, "SELECT * FROM tb_penduduk where nik!='admin'");
      while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option <?php echo $d['pj']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <label>Tanggal Berlaku</label>
    <input type="date" class="form-control" value="<?php echo $d['tgl_berlaku']; ?>" name="tgl_berlaku">
  </div>
  <div class="form-group">
    <label>Tanggal Dibuat</label>
    <input type="text" class="form-control" name="tgl_dibuat" value="<?php echo $d['tgl_dibuat']; ?>" readonly>
  </div>
  <?php
}elseif($c['jenis_surat']=='Belum Nikah'){
  $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_belum_pernah_menikah a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))
  ?>
  <div class="form-group">
    <label>Nomor Surat</label>
    <input type="text" class="form-control" name="no_surat" value="<?php echo $d['no_surat']; ?>" readonly>
  </div>
  <div class="form-group">
    <label>NIK</label>
    <select class="form-control select2" name="nik">
     <?php 
     $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
     while ($dt=mysqli_fetch_array($cari)) {
      ?>
      <option <?php echo $d['nik']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
      <?php
    }
    ?>
  </select>
</div>
<div class="form-group">
  <label>Nama Ayah</label>
  <input type="text" class="form-control" value="<?php echo $d['nama_ayah']; ?>" name="nama_ayah">
</div>
<div class="form-group">
  <label>Nama Ibu</label>
  <input type="text" class="form-control" value="<?php echo $d['nama_ibu']; ?>" name="nama_ibu">
</div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="text" class="form-control" name="tgl_dibuat" value="<?php echo $d['tgl_dibuat']; ?>" readonly>
</div>
<?php
}elseif($c['jenis_surat']=='Yatim'){
  $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_yatim a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))
  ?>
  <div class="form-group">
    <label>Nomor Surat</label>
    <input type="text" class="form-control" name="no_surat" value="<?php echo $d['no_surat']; ?>" readonly>
  </div>
  <div class="form-group">
    <label>NIK</label>
    <select class="form-control select2" name="nik">
     <?php 
     $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
     while ($dt=mysqli_fetch_array($cari)) {
      ?>
      <option <?php echo $d['nik']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
      <?php
    }
    ?>
  </select>
</div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="text" class="form-control" name="tgl_dibuat" value="<?php echo $d['tgl_dibuat']; ?>" readonly>
</div>
<?php
}elseif ($c['jenis_surat']=='Lahiran') {
  $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_lahiran a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))
  ?>
  <div class="form-group">
    <label>No Surat Kelahiran</label>
    <input type="text" class="form-control" value="<?php echo $d['no_surat']; ?>" readonly name="no_surat">
  </div>
  <!-- text input -->
  <!--  -->
  <div class="form-group">
    <label>Hari Lahir</label>
    <select class="form-control" name="hari">
      <option <?php echo $d['hari_lahir']=='Senin'?'selected':''; ?>>Senin</option>
      <option <?php echo $d['hari_lahir']=='Selasa'?'selected':''; ?>>Selasa</option>
      <option <?php echo $d['hari_lahir']=='Rabu'?'selected':''; ?>>Rabu</option>
      <option <?php echo $d['hari_lahir']=='Kamis'?'selected':''; ?>>Kamis</option>
      <option <?php echo $d['hari_lahir']=='Jumat'?'selected':''; ?>>Jumat</option>
      <option <?php echo $d['hari_lahir']=='Sabtu'?'selected':''; ?>>Sabtu</option>
      <option <?php echo $d['hari_lahir']=='Minggu'?'selected':''; ?>>Minggu</option>
    </select>
  </div>
  <div class="form-group">
    <label>Tanggal Lahir</label>
    <input type="date" class="form-control" value="<?php echo $d['tgl_lahir']; ?>" name="tgl_lahir">
  </div>
  <div class="form-group">
    <label>Jam Lahir</label>
    <input type="time" class="form-control" value="<?php echo $d['jam']; ?>" name="jam">
  </div>
  <div class="form-group">
    <label>Jenis Kelamin</label>
    <select class="form-control" name="jenis_kelamin">
      <option <?php echo $d['jenis_kelamin']=='Laki-laki'?'selected':''; ?>>Laki-laki</option>
      <option <?php echo $d['jenis_kelamin']=='Perempuan'?'selected':''; ?>>Perempuan</option>
    </select>
  </div>
  <div class="form-group">
    <label>Jenis Kelahiran</label>
    <input type="text" class="form-control" value="<?php echo $d['jenis_kelahiran']; ?>" name="jenis_kelahiran">
  </div>
  <div class="form-group">
    <label>Lahiran Ke</label>
    <input type="text" class="form-control" value="<?php echo $d['kelahiran_ke']; ?>" name="lahiran_ke">
  </div>
  <div class="form-group">
    <label>Berat</label>
    <input type="text" class="form-control" value="<?php echo $d['berat']; ?>" name="berat">
  </div>
  <div class="form-group">
    <label>Panjang</label>
    <input type="text" class="form-control" value="<?php echo $d['panjang']; ?>" name="panjang">
  </div>
  <div class="form-group">
    <label>Lahir Di</label>
    <input type="text" class="form-control" value="<?php echo $d['lahir_di']; ?>" name="lahir_di">
  </div>
  <div class="form-group">
    <label>Nama Anak</label>
    <input type="text" class="form-control" value="<?php echo $d['nama_anak']; ?>" name="nama_anak">
  </div>
  <div class="form-group">
    <label>Nama Ibu</label>
    <input type="text" class="form-control" value="<?php echo $d['nama_ibu']; ?>" name="nama_ibu">
  </div>
  <div class="form-group">
    <label>Pekerjaan Ibu</label>
    <input type="text" class="form-control" value="<?php echo $d['pekerjaan_ibu']; ?>" name="pekerjaan_ibu">
  </div>
  <div class="form-group">
    <label>NIK Ibu</label>
    <select class="form-control select2" name="nik_ibu">
      <option value="" disabled="" selected="">--Pilih Penduduk--</option>
      <?php 
      include '../koneksi.php';
      $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
      while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option <?php echo $d['nik_ibu']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
    </select>
  </div>

  <div class="form-group">
    <label>Nama Ayah</label>
    <input type="text" class="form-control" value="<?php echo $d['nama_ayah']; ?>" name="nama_ayah">
  </div>
  <div class="form-group">
    <label>Pekerjaan Ayah</label>
    <input type="text" class="form-control" value="<?php echo $d['pekerjaan_ayah']; ?>" name="pekerjaan_ayah">
  </div>
  <div class="form-group">
    <label>NIK Ayah</label>
    <select class="form-control select2" name="nik_ayah">
      <option value="" disabled="" selected="">--Pilih Penduduk--</option>
      <?php 
      include '../koneksi.php';
      $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
      while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option <?php echo $d['nik_ayah']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <label>Alamat</label>
    <input type="text" class="form-control" value="<?php echo $d['alamat']; ?>" name="alamat">
  </div>
  <div class="form-group">
    <label>Tanggal Dibuat</label>
    <input type="date" class="form-control" readonly value="<?php echo date('Y-m-d') ?>" name="tgl_dibuat">
  </div>
  <?php
}
elseif($c['jenis_surat']=='Bepergian'){
  $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_bepergian a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))
  ?>
  <div class="form-group">
    <label>No Surat</label>
    <input type="text" class="form-control" value="<?php echo $d['no_surat']; ?>" readonly name="no_surat">
  </div>
  <div class="form-group">
    <label>NIK</label>
    <select class="form-control select2" name="nik">
      <?php 
      include '../koneksi.php';
      $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
      while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option <?php echo $d['nik']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
    </select>
  </div>

  <div class="form-group">
    <label>Alamat</label>
    <input type="text" class="form-control" value="<?php echo $d['alamat_tujuan']; ?>" name="alamat_tujuan">
  </div>
  <div class="form-group">
    <label>Kota</label>
    <input type="text" class="form-control" value="<?php echo $d['kota']; ?>" name="kota">
  </div>
  <div class="form-group">
    <label>Provinsi</label>
    <input type="text" class="form-control" value="<?php echo $d['provinsi']; ?>" name="provinsi">
  </div>
  <div class="form-group">
    <label>Tanggal Berangkat</label>
    <input type="date" class="form-control" value="<?php echo $d['tgl_berangkat']; ?>" name="tgl_berangkat">
  </div>
  <div class="form-group">
    <label>Keperluan</label>
    <input type="text" class="form-control" value="<?php echo $d['keperluan']; ?>" name="keperluan">
  </div>
  <div class="form-group">
    <label>Pengikut (orang)</label>
    <input type="number" class="form-control" value="<?php echo $d['pengikut']; ?>" name="pengikut">
  </div>
  <div class="form-group">
    <label>Tanggal Dibuat</label>
    <input type="date" class="form-control" readonly value="<?php echo $d['tgl_dibuat']; ?>" name="tgl_dibuat">
  </div>
  <?php

}elseif ($c['jenis_surat']=='Identitas') {
  $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_identitas a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))

  ?>
  <div class="form-group">
    <label>No Surat</label>
    <input type="text" class="form-control" value="<?php echo $d['no_surat']; ?>" readonly name="no_surat">
  </div>
  <div class="form-group">
    <label>NIK</label>
    <select class="form-control select2" name="nik">
      <?php 
      include '../koneksi.php';
      $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
      while ($dt=mysqli_fetch_array($cari)) {
        ?>
        <option <?php echo $d['nik']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
        <?php
      }
      ?>
    </select>
  </div>

  <div class="form-group">
    <label>Golongan Darah</label>
    <select name="gol_darah" class="form-control">
      <option <?php echo $d['gol_darah']=='A'?'selected':'' ?>>A</option>
      <option <?php echo $d['gol_darah']=='B'?'selected':'' ?>>B</option>
      <option <?php echo $d['gol_darah']=='AB'?'selected':'' ?>>AB</option>
      <option <?php echo $d['gol_darah']=='O'?'selected':'' ?>>O</option>
    </select>
  </div>
  <div class="form-group">
    <label>Tanggal Berlaku</label>
    <input type="date" class="form-control" value="<?php echo $d['tgl_berlaku']; ?>" name="tgl_berlaku">
  </div>
  <div class="form-group">
    <label>Tanggal Dibuat</label>
    <input type="date" class="form-control" readonly value="<?php echo $d['tgl_dibuat']; ?>" name="tgl_dibuat">
  </div>
  <?php
}elseif ($c['jenis_surat']=='Tidak Memiliki Rumah') {
 $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_tidak_memiliki_rumah a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))
 ?>
 <div class="form-group">
  <label>No Surat</label>
  <input type="text" class="form-control" value="<?php echo $d['no_surat']; ?>" readonly name="no_surat">
</div>
<div class="form-group">
  <label>NIK</label>
  <select class="form-control select2" name="nik">
    <?php 
    include '../koneksi.php';
    $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
    while ($dt=mysqli_fetch_array($cari)) {
      ?>
      <option <?php echo $d['nik']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
      <?php
    }
    ?>
  </select>
</div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="date" class="form-control" readonly value="<?php echo $d['tgl_dibuat']; ?>" name="tgl_dibuat">
</div>
<?php
}elseif ($c['jenis_surat']=='Yatim') {
 $d=mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tb_yatim a left join surat_keluar b on a.id_surat_keluar=b.id_surat_keluar where b.id_surat_keluar='$id_surat_keluar'"))
 ?>
 <div class="form-group">
  <label>No Surat</label>
  <input type="text" class="form-control" value="<?php echo $d['no_surat']; ?>" readonly name="no_surat">
</div>
<div class="form-group">
  <label>NIK</label>
  <select class="form-control select2" name="nik">
    <?php 
    include '../koneksi.php';
    $cari=mysqli_query($con, "SELECT * FROM tb_penduduk");
    while ($dt=mysqli_fetch_array($cari)) {
      ?>
      <option <?php echo $d['nik']==$dt['nik'] ?'selected':''; ?> value="<?php echo $dt['nik'] ?>"><?php echo $dt['nik'].' / '.$dt['nama'] ?></option>
      <?php
    }
    ?>
  </select>
</div>
<div class="form-group">
  <label>Tanggal Dibuat</label>
  <input type="date" class="form-control" readonly value="<?php echo $d['tgl_dibuat']; ?>" name="tgl_dibuat">
</div>
<?php
}
?>

<div class="card-footer">

 <button type="submit" value="Simpan" name="update" class="btn btn-primary btn-sm fa fa-dot-circle-o"> Simpan</button>
 <button type="submit"  class="btn btn-danger btn-sm" data-dismiss="modal">Kembali</button>
</div>

</form>


</div>
