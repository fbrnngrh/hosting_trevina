<?php
ini_set("display_errors",0);
include("includes/defines.php");
include("includes/fungsi.php");
include("includes/tgl_indo.php");
include("includes/fungsi_rupiah.php");
cekSession();
cekSession();

?>


<!DOCTYPE html>
<html lang="en">

<?php include 'header.php' ?>

<body id="page-top">


    <div id="wrapper">

     <?php include ('proses/menu/menu_petugas.php')?>


     <!-- End of Topbar -->
     <div class="container-fluid">


        <h1 class="h3 mb-2 text-gray-800">Tabel Surat Keluar</h1>
        <p class="mb-4">Kamu Bisa menggunakan tambah, ubah, hapus</p>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data Surat Keluar</h6>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                            <th>No</th>
                            <th>No Surat</th>
                            <th>NIK</th>
                            <th>Jenis Surat</th>
                            <th>Tanggal Dibuat</th>
                            <th>Status</th>
                            <th>Persyaratan</th>


                        </tr>
                    </thead>
  <tbody>

                        <?php

                        $query = mysqli_query($con,"SELECT s.*,r.no_surat rno,r.tgl_dibuat rtgl,r.nik rnik,y.no_surat yno,y.tgl_dibuat ytgl,y.nik ynik,be.no_surat beno,be.tgl_dibuat betgl,be.nik benik,i.no_surat ino,i.tgl_dibuat itgl,i.nik inik,k.no_surat kno,k.tgl_dibuat ktgl,k.nik knik, p.no_surat pno,p.tgl_dibuat ptgl, p.nik pnik, u.no_surat uno,u.tgl_dibuat utgl,u.nik unik, d.no_surat dno,d.tgl_dibuat dtgl,d.pj dnik, b.no_surat bno,b.tgl_dibuat btgl,b.nik bnik,tm.no_surat tmno,tm.tgl_dibuat tmtgl,tm.nik tmnik,us.no_surat usno,us.tgl_dibuat ustgl, us.nik usnik,tl.no_surat tlno,tl.tgl_surat tltgl,tl.nik_ibu tlnik
                           FROM surat_keluar s
                           LEFT JOIN tb_kehilangan k ON s.id_surat_keluar = k.id_surat_keluar
                           LEFT JOIN tb_kematian p ON s.id_surat_keluar = p.id_surat_keluar
                           LEFT JOIN tb_penguburan u ON s.id_surat_keluar = u.id_surat_keluar
                           LEFT JOIN tb_domisili_organisasi d ON s.id_surat_keluar = d.id_surat_keluar
                           LEFT JOIN tb_belum_pernah_menikah b ON s.id_surat_keluar = b.id_surat_keluar
                           LEFT JOIN tb_tidak_mampu tm ON s.id_surat_keluar = tm.id_surat_keluar
                           LEFT JOIN tb_usaha us ON s.id_surat_keluar = us.id_surat_keluar
                           LEFT JOIN tb_lahiran tl ON s.id_surat_keluar = tl.id_surat_keluar
                           LEFT JOIN tb_bepergian be ON s.id_surat_keluar = be.id_surat_keluar
                           LEFT JOIN tb_identitas i ON s.id_surat_keluar = i.id_surat_keluar
                           LEFT JOIN tb_tidak_memiliki_rumah r ON s.id_surat_keluar = r.id_surat_keluar
                           LEFT JOIN tb_yatim y ON s.id_surat_keluar = y.id_surat_keluar");
                        $no = 1;

                        while ($data = mysqli_fetch_assoc($query)) {

                            if ($data['jenis_surat']=='Kehilangan') {
                                $no_surat=$data['kno'];
                                $tgl_dibuat=$data['ktgl'];
                                $nik=$data['knik'];
                                $cetak='kehilangan.php';
                                $edit='ubah_kehilangan.php';
                                $hapus='hapus_kehilangan.php';
                            }elseif ($data['jenis_surat']=='Kematian') {
                                $no_surat=$data['pno'];
                                $tgl_dibuat=$data['ptgl'];
                                $nik=$data['pnik'];
                                $cetak='kematian.php';
                                $edit='ubah_kematian.php';
                                $hapus='hapus_kematian.php';
                            }elseif ($data['jenis_surat']=='Penguburan') {
                                $no_surat=$data['uno'];
                                $tgl_dibuat=$data['utgl'];
                                $nik=$data['unik'];
                                $cetak='penguburan.php';
                                $edit='ubah_penguburan.php';
                                $hapus='hapus_penguburan.php';
                            }elseif ($data['jenis_surat']=='Tidak Mampu') {
                                $no_surat=$data['tmno'];
                                $tgl_dibuat=$data['tmtgl'];
                                $nik=$data['tmnik'];
                                $cetak='tidak_mampu.php';
                                $edit='ubah_tidak_mampu.php';
                                $hapus='hapus_tidak_mampu.php';
                            }elseif ($data['jenis_surat']=='Usaha') {
                                $no_surat=$data['usno'];
                                $tgl_dibuat=$data['ustgl'];
                                $nik=$data['usnik'];
                                $cetak='usaha.php';
                                $edit='ubah_usaha.php';
                                $hapus='hapus_usaha.php';
                            }elseif ($data['jenis_surat']=='Domisili Organisasi') {
                                $no_surat=$data['dno'];
                                $tgl_dibuat=$data['dtgl'];
                                $nik=$data['dnik'];
                                $cetak='domisili_organisasi.php';
                                $edit='ubah_domisili.php';
                                $hapus='hapus_domisili.php';
                            }elseif ($data['jenis_surat']=='Belum Nikah') {
                                $no_surat=$data['bno'];
                                $tgl_dibuat=$data['btgl'];
                                $nik=$data['bnik'];
                                $cetak='belum_menikah.php';
                                $edit='ubah_belum_menikah.php';
                                $hapus='hapus_belum_menikah.php';
                            }elseif ($data['jenis_surat']=='Lahiran') {
                                $no_surat=$data['tlno'];
                                $tgl_dibuat=$data['tltgl'];
                                $nik=$data['tlnik'];
                                $cetak='lahiran.php';
                                $edit='ubah_belum_lahiran.php';
                                $hapus='hapus_lahiran.php';
                            }elseif ($data['jenis_surat']=='Bepergian') {
                                $no_surat=$data['beno'];
                                $tgl_dibuat=$data['betgl'];
                                $nik=$data['benik'];
                                $cetak='bepergian.php';
                                $edit='ubah_belum_bepergian.php';
                                $hapus='hapus_bepergian.php';
                            }elseif ($data['jenis_surat']=='Identitas') {
                                $no_surat=$data['ino'];
                                $tgl_dibuat=$data['itgl'];
                                $nik=$data['inik'];
                                $cetak='identitas.php';
                                $edit='ubah_belum_identitas.php';
                                $hapus='hapus_identitas.php';
                            }elseif ($data['jenis_surat']=='Tidak Memiliki Rumah') {
                                $no_surat=$data['rno'];
                                $tgl_dibuat=$data['rtgl'];
                                $nik=$data['rnik'];
                                $cetak='tidak_memiliki_rumah.php';
                                $edit='ubah_tidak_memiliki_rumah.php';
                                $hapus='hapus_tidak_memiliki_rumah.php';
                            }elseif ($data['jenis_surat']=='Yatim') {
                                $no_surat=$data['yno'];
                                $tgl_dibuat=$data['ytgl'];
                                $nik=$data['ynik'];
                                $cetak='yatim.php';
                                $edit='ubah_yatim.php';
                                $hapus='hapus_yatim.php';
                            }
                            ?> 
                            <tr>
                                <td> <?php echo $no++ ?></td> 
                                <td> <?php echo $no_surat; ?></td>
                                <td> <?php echo $nik; ?></td>
                                <td> <?php echo $data['jenis_surat']; ?></td>
                                <td> <?= tanggal_indo($tgl_dibuat); ?> </td>
                                <td>
                                  <?php 
                                  if ($data['status']=='Tunggu') {
                                    ?>
                                    <span class="badge badge-secondary">Tunggu</span>
                                    <?php
                                  }elseif ($data['status']=='Terima'){
                                    ?>
                                    <span class="badge badge-success">Terima</span>
                                    <?php
                                  }else{
                                     ?>
                                    <span class="badge badge-danger">Tolak</span>
                                    <?php
                                  }

                                   ?>
                                </td>
                                <td> <a href="img/<?php echo $data['upload_file']; ?>" target="_blank"><?php echo $data['upload_file']; ?></a></td>

                            </tr>
                        <?php  } ?>
                    </tbody>
               
            </table>
        </div>
    </div>
</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
           <span>Copyright &copy; by Trevina Sabrina Erin </span>
       </div>
   </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<?php include ('proses/modal/modal_keluar.php')?>

<!-- Bootstrap core JavaScript-->
<?php include('footer.php')?>

</body>

</html>
<?php 

include("proses/modal/permintaan_pengguna.php");

?>