<?php
ini_set("display_errors",0);
include("../../includes/defines.php");
include("../../includes/fungsi.php");
session_start();
?>


<script>
  $(document).ready(function() {
    $("#inputType").change(function() {
      var inputType = $(this).val();
      $.ajax({
        url: "get_form_input.php",
        type: "POST",
        data: { inputType: inputType },
        success: function(data) {
          $("#form-input").html(data);
        }
      });
    });
  });
</script>

<script>
  $(document).ready(function() {
    $('.select2').select2({theme: 'bootstrap4'});
  });
</script>
<div class="card-header"><strong>TAMBAH DATA</strong></div>
<div class="card-body card-block">
  <form action="proses/crud/proses_tambah_surat_keluar.php" method="POST" enctype="multipart/form-data" >
    <div class="form-group">
      <label for="company" class=" form-control-label">Jenis Surat</label>
      <select name="inputType" id="inputType" class="form-control select2">
        <option disabled selected>Pilih Jenis Surat</option>
        <option value="Lahiran">Lahiran</option>
        <option value="Kehilangan">Kehilangan</option>
        <option value="Kematian">Kematian</option>
        <option value="Penguburan">Penguburan</option>
        <!-- <option value="Pindah">Pindah</option> -->
        <option value="Bepergian">Bepergian</option>
        <option value="Tidak Mampu">Tidak Mampu</option>
        <option value="Usaha">Usaha</option>
        <option value="Domisili Organisasi">Domisili Organisasi</option>
        <option value="Belum Nikah">Belum Nikah</option>
        <option value="Identitas">Identitas Diri</option>
        <option value="Tidak Memiliki Rumah">Tidak Memiliki Rumah</option>
        <option value="Yatim">Yatim</option>
      </select>
    </div>
    
    <div id="form-input"></div>

    <div class="card-footer">

     <button type="submit" value="Simpan" name="update" class="btn btn-primary btn-sm fa fa-dot-circle-o"> Simpan</button>
     <button type="submit"  class="btn btn-danger btn-sm" data-dismiss="modal">Kembali</button>
   </div>

 </form>


</div>
