﻿  <?php
  include ("../../includes/defines.php");
  if(isset($_POST['idbag']))
  {
    $idbag=$_POST['idbag'];
    $query=mysqli_query($con,"SELECT * FROM bagian WHERE id_bagian=$idbag");
    $data=mysqli_fetch_assoc($query);
}
?>           



<div class="card-header"><strong>UBAH DATA BAGIAN</strong></div>
<div class="card-body card-block">
   <form action="proses/crud/proses_ubah_bagian.php" method="POST" enctype="multipart/form-data" >

    
      <div class="form-group"><label for="company" class=" form-control-label">Bagian</label>
          <input hidden="hidden" type="text" name="id_bagian" value="<?php echo $data['id_bagian']; ?>">
          <input type="text" name="bagian" placeholder="Masukkan data bagian" value="<?php echo $data['bagian']; ?>" class="form-control" required>

      </div>

      
      
      <div class="card-footer">
          
       <button type="submit" value="Simpan" name="update" class="btn btn-primary btn-sm fa fa-dot-circle-o"> Simpan</button>
       <button type="submit"  class="btn btn-danger btn-sm" data-dismiss="modal">Kembali</button>
   </div>
   
</form>


</div>
