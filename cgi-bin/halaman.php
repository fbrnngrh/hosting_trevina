<?php
// ini_set("display_errors",0);
include("includes/defines.php");
include("includes/fungsi.php");
cekSession();

if ($_SESSION['level']=='Admin') {
    $sql_surat_keluar=mysqli_query($con,"SELECT * FROM surat_keluar");
    $jml_surat_keluar=mysqli_num_rows($sql_surat_keluar);
}elseif ($_SESSION['level']=='Kades') {
    $sql_surat_keluar=mysqli_query($con,"SELECT * FROM surat_keluar");
    $jml_surat_keluar=mysqli_num_rows($sql_surat_keluar);
}elseif ($_SESSION['level']=='RT') {
    $sql_surat_keluar=mysqli_query($con,"SELECT * FROM surat_keluar");
    $jml_surat_keluar=mysqli_num_rows($sql_surat_keluar);
}elseif ($_SESSION['level']=='Pengguna') {
    $id_user=$_SESSION['id_user'];
    $cari=mysqli_fetch_array(mysqli_query($con, "SELECT * from tb_penduduk where id_user='$id_user'"));
    $nik=$cari['nik'];

    // $dt=mysqli_fetch_array(mysqli_query($con,"SELECT count(id_surat) as tot FROM tb_kehilangan where nik='$nik'"));
    // $dt1=mysqli_fetch_array(mysqli_query($con,"SELECT count(id_surat) as tot FROM tb_kematian where nik='$nik'"));
    // $dt2=mysqli_fetch_array(mysqli_query($con,"SELECT count(id_surat) as tot FROM tb_penguburan where nik='$nik'"));
    
    // $dt4=mysqli_fetch_array(mysqli_query($con,"SELECT count(id_surat) as tot FROM tb_tidak_mampu where nik='$nik'"));
    // $dt5=mysqli_fetch_array(mysqli_query($con,"SELECT count(id_surat) as tot FROM tb_usaha where nik='$nik'"));
    // $dt6=mysqli_fetch_array(mysqli_query($con,"SELECT count(id_surat) as tot FROM tb_domisili_ where nik='$nik'"));
    // $dt7=mysqli_fetch_array(mysqli_query($con,"SELECT count(id_surat) as tot FROM tb_belum_pernah_menikah where nik='$nik'"));
    // $jml_surat_keluar=$dt['tot']+$dt1['tot']+$dt2['tot']+$dt3['tot']+$dt4['tot']+$dt5['tot']+$dt6['tot']+$dt7['tot'];
    $sql_surat_keluar=mysqli_query($con,"SELECT * FROM surat_keluar");
    $jml_surat_keluar=mysqli_num_rows($sql_surat_keluar);
}



$sql_permintaan=mysqli_query($con,"SELECT *FROM permintaan");
$jml_permintaan=mysqli_num_rows($sql_permintaan);

$sql_masuk=mysqli_query($con,"SELECT *FROM surat_masuk");
$jml_masuk=mysqli_num_rows($sql_masuk);


$sql_masyarakat=mysqli_query($con,"SELECT *FROM user where user.level = 'Pengguna'");
$jml_masyarakat=mysqli_num_rows($sql_masyarakat);

$sql_pegawai=mysqli_query($con,"SELECT *FROM pegawai");
$jml_pegawai=mysqli_num_rows($sql_pegawai);


?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Halaman</title>

    <link rel="icon" type="image/png" href="login2/surat.png"/>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    

</head>

<body id="page-top">

   <?php if($_SESSION['level']=='Admin'){ ?>
    <div id="wrapper">

       <?php include ('proses/menu/menu_admin.php')?>


       <!-- End of Topbar -->

       <!-- Begin Page Content -->
       <div class="container-fluid">
          <?php 
          $id_user=$_SESSION['id_user'];
          $query_mysqli = mysqli_query($con,"SELECT * FROM user WHERE id_user='$id_user'")or die(mysqli_error());
          while($data = mysqli_fetch_array($query_mysqli)){ 
            ?> 
            <div id="clock"></div>
            <script type="text/javascript">
                <!--
                function showTime() {
                    var a_p = "";
                    var today = new Date();
                    var curr_hour = today.getHours();
                    var curr_minute = today.getMinutes();
                    var curr_second = today.getSeconds();
                    if (curr_hour < 12) {
                        a_p = "AM";
                    } else {
                        a_p = "PM";
                    }
                    if (curr_hour == 0) {
                        curr_hour = 12;
                    }
                    if (curr_hour > 12) {
                        curr_hour = curr_hour - 12;
                    }
                    curr_hour = checkTime(curr_hour);
                    curr_minute = checkTime(curr_minute);
                    curr_second = checkTime(curr_second);
                    document.getElementById('clock').innerHTML=curr_hour + ":" + curr_minute + ":" + curr_second + " " + a_p;
                }

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }
                setInterval(showTime, 500);
        //-->
            </script>

            <!-- Menampilkan Hari, Bulan dan Tahun -->
            <br>
            <script type='text/javascript'>
                <!--
                var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
                var date = new Date();
                var day = date.getDate();
                var month = date.getMonth();
                var thisDay = date.getDay(),
                thisDay = myDays[thisDay];
                var yy = date.getYear();
                var year = (yy < 1000) ? yy + 1900 : yy;
                document.write(thisDay + ', ' + day + ' ' + months[month] + ' ' + year);
            //-->
            </script>
            <marquee width="" height="">
                <br>Hai, <?php echo $data['username'] ?> - Selamat Datang di Sistem Surat Menyurat Kantor Desa Batu Mulya Berbasis Web</marquee>

            <?php } ?>

            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Data & Dokumen</h1>

            </div>

            <!-- Content Row -->
            <div class="row">

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Surat Keluar</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $jml_surat_keluar; ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa fa-envelope fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-success shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    Surat Masuk</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $jml_masuk; ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa fa-envelope fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-info shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Pengguna                                            </div>
                                    <div class="row no-gutters align-items-center">
                                        <div class="col-auto">
                                            <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo $jml_masyarakat; ?></div>
                                        </div>
                                        <div class="col">
                                            <div class="progress progress-sm mr-2">
                                                <div class="progress-bar bg-info" role="progressbar"
                                                style="width: 50%" aria-valuenow="50" aria-valuemin="0"
                                                aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-users fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Pending Requests Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-warning shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                    Pegawai</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $jml_pegawai; ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa fa-users fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Content Row -->



        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; by Trevina </span>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

</div>
<?php } ?>

</div>
<!-- End of Page Wrapper -->

<?php if($_SESSION['level']=='Admin'){ ?>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <?php include ('proses/modal/modal_keluar.php')?>


<?php } ?>



<?php if($_SESSION['level']=='Pengguna'){ ?>
    <div id="wrapper">

       <?php include ('proses/menu/menu_pengguna.php')?>


       <!-- End of Topbar -->

       <!-- Begin Page Content -->
       <div class="container-fluid">

         <?php 
         $id_user=$_SESSION['id_user'];
         $query_mysqli = mysqli_query($con,"SELECT * FROM user WHERE id_user='$id_user'")or die(mysqli_error());
         while($data = mysqli_fetch_array($query_mysqli)){ 
            ?> 
            <div id="clock"></div>
            <script type="text/javascript">
                <!--
                function showTime() {
                    var a_p = "";
                    var today = new Date();
                    var curr_hour = today.getHours();
                    var curr_minute = today.getMinutes();
                    var curr_second = today.getSeconds();
                    if (curr_hour < 12) {
                        a_p = "AM";
                    } else {
                        a_p = "PM";
                    }
                    if (curr_hour == 0) {
                        curr_hour = 12;
                    }
                    if (curr_hour > 12) {
                        curr_hour = curr_hour - 12;
                    }
                    curr_hour = checkTime(curr_hour);
                    curr_minute = checkTime(curr_minute);
                    curr_second = checkTime(curr_second);
                    document.getElementById('clock').innerHTML=curr_hour + ":" + curr_minute + ":" + curr_second + " " + a_p;
                }

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }
                setInterval(showTime, 500);
        //-->
            </script>

            <!-- Menampilkan Hari, Bulan dan Tahun -->
            <br>
            <script type='text/javascript'>
                <!--
                var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
                var date = new Date();
                var day = date.getDate();
                var month = date.getMonth();
                var thisDay = date.getDay(),
                thisDay = myDays[thisDay];
                var yy = date.getYear();
                var year = (yy < 1000) ? yy + 1900 : yy;
                document.write(thisDay + ', ' + day + ' ' + months[month] + ' ' + year);
            //-->
            </script>
            <marquee width="" height="">
                <br>Hai, <?php echo $data['username'] ?> - Selamat Datang di Sistem Surat Menyurat Kantor Desa Batu Mulya Berbasis Web</marquee>

            <?php } ?>

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Permintaan  </h1>

            </div>

            <!-- Content Row -->
            <div class="row">

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Surat</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $jml_surat_keluar; ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa fa-envelope fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <a href="Persyaratan dan Ketentuan Pengajuan Permohonan Surat Kantor Desa Batu Mulya.pdf" target="_blank">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Persyaratan Surat</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa fa-download fa-2x text-gray-300"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <!-- Content Row -->



        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; by Trevina </span>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

</div>
<?php } ?>

</div>
<!-- End of Page Wrapper -->

<?php if($_SESSION['level']=='Pengguna'){ ?>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <?php include ('proses/modal/modal_keluar.php')?>



<?php } ?>





<?php if($_SESSION['level']=='RT'){ ?>
    <div id="wrapper">

       <?php include ('proses/menu/menu_rt.php')?>


       <!-- End of Topbar -->

       <!-- Begin Page Content -->
       <div class="container-fluid">

         <?php 
         $id_user=$_SESSION['id_user'];
         $query_mysqli = mysqli_query($con,"SELECT * FROM user WHERE id_user='$id_user'")or die(mysqli_error());
         while($data = mysqli_fetch_array($query_mysqli)){ 
            ?> 
            <div id="clock"></div>
            <script type="text/javascript">
                <!--
                function showTime() {
                    var a_p = "";
                    var today = new Date();
                    var curr_hour = today.getHours();
                    var curr_minute = today.getMinutes();
                    var curr_second = today.getSeconds();
                    if (curr_hour < 12) {
                        a_p = "AM";
                    } else {
                        a_p = "PM";
                    }
                    if (curr_hour == 0) {
                        curr_hour = 12;
                    }
                    if (curr_hour > 12) {
                        curr_hour = curr_hour - 12;
                    }
                    curr_hour = checkTime(curr_hour);
                    curr_minute = checkTime(curr_minute);
                    curr_second = checkTime(curr_second);
                    document.getElementById('clock').innerHTML=curr_hour + ":" + curr_minute + ":" + curr_second + " " + a_p;
                }

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }
                setInterval(showTime, 500);
        //-->
            </script>

            <!-- Menampilkan Hari, Bulan dan Tahun -->
            <br>
            <script type='text/javascript'>
                <!--
                var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
                var date = new Date();
                var day = date.getDate();
                var month = date.getMonth();
                var thisDay = date.getDay(),
                thisDay = myDays[thisDay];
                var yy = date.getYear();
                var year = (yy < 1000) ? yy + 1900 : yy;
                document.write(thisDay + ', ' + day + ' ' + months[month] + ' ' + year);
            //-->
            </script>
            <marquee width="" height="">
                <br>Hai, <?php echo $data['username'] ?> - Selamat Datang di Sistem Surat Menyurat Kantor Desa Batu Mulya Berbasis Web</marquee>

            <?php } ?>

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Data Permintaan Surat</h1>

            </div>

            <!-- Content Row -->
            <div class="row">

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Surat</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $jml_surat_keluar; ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa fa-envelope fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <!-- Content Row -->



        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; by Trevina </span>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

</div>
<?php } ?>

</div>
<!-- End of Page Wrapper -->

<?php if($_SESSION['level']=='Pengguna'){ ?>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <?php include ('proses/modal/modal_keluar.php')?>



<?php } ?>


<?php if($_SESSION['level']=='Petugas'){ ?>
    <div id="wrapper">

       <?php include ('proses/menu/menu_petugas.php')?>


       <!-- End of Topbar -->

       <!-- Begin Page Content -->
       <div class="container-fluid">

         <?php 
         $id_user=$_SESSION['id_user'];
         $query_mysqli = mysqli_query($con,"SELECT * FROM user WHERE id_user='$id_user'")or die(mysqli_error());
         while($data = mysqli_fetch_array($query_mysqli)){ 
            ?> 
            <div id="clock"></div>
            <script type="text/javascript">
                <!--
                function showTime() {
                    var a_p = "";
                    var today = new Date();
                    var curr_hour = today.getHours();
                    var curr_minute = today.getMinutes();
                    var curr_second = today.getSeconds();
                    if (curr_hour < 12) {
                        a_p = "AM";
                    } else {
                        a_p = "PM";
                    }
                    if (curr_hour == 0) {
                        curr_hour = 12;
                    }
                    if (curr_hour > 12) {
                        curr_hour = curr_hour - 12;
                    }
                    curr_hour = checkTime(curr_hour);
                    curr_minute = checkTime(curr_minute);
                    curr_second = checkTime(curr_second);
                    document.getElementById('clock').innerHTML=curr_hour + ":" + curr_minute + ":" + curr_second + " " + a_p;
                }

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }
                setInterval(showTime, 500);
        //-->
            </script>

            <!-- Menampilkan Hari, Bulan dan Tahun -->
            <br>
            <script type='text/javascript'>
                <!--
                var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
                var date = new Date();
                var day = date.getDate();
                var month = date.getMonth();
                var thisDay = date.getDay(),
                thisDay = myDays[thisDay];
                var yy = date.getYear();
                var year = (yy < 1000) ? yy + 1900 : yy;
                document.write(thisDay + ', ' + day + ' ' + months[month] + ' ' + year);
            //-->
            </script>
            <marquee width="" height="">
                <br>Hai, <?php echo $data['username'] ?> - Selamat Datang di Sistem Surat Menyurat Kantor Desa Batu Mulya Berbasis Web</marquee>

            <?php } ?>

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Data & Dokumen</h1>

            </div>

            <!-- Content Row -->
            <div class="row">

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Surat Keluar</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $jml_surat_keluar; ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa fa-envelope fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-success shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    Surat Masuk</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $jml_masuk; ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa fa-envelope fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Permintaan Surat</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $jml_permintaan; ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa fa-envelope fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <!-- Content Row -->



        </div>
        <!-- /.container-fluid -->

    </div>

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; by Trevina </span>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

</div>
<?php } ?>

</div>
<!-- End of Page Wrapper -->

<?php if($_SESSION['level']=='Petugas'){ ?>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <?php include ('proses/modal/modal_keluar.php')?>



<?php } ?>



<?php if($_SESSION['level']=='Kades'){ ?>
    <div id="wrapper">

       <?php include ('proses/menu/menu_kades.php')?>


       <!-- End of Topbar -->

       <!-- Begin Page Content -->
       <div class="container-fluid">

         <?php 
         $id_user=$_SESSION['id_user'];
         $query_mysqli = mysqli_query($con,"SELECT * FROM user WHERE id_user='$id_user'")or die(mysqli_error());
         while($data = mysqli_fetch_array($query_mysqli)){ 
            ?> 
            <div id="clock"></div>
            <script type="text/javascript">
                <!--
                function showTime() {
                    var a_p = "";
                    var today = new Date();
                    var curr_hour = today.getHours();
                    var curr_minute = today.getMinutes();
                    var curr_second = today.getSeconds();
                    if (curr_hour < 12) {
                        a_p = "AM";
                    } else {
                        a_p = "PM";
                    }
                    if (curr_hour == 0) {
                        curr_hour = 12;
                    }
                    if (curr_hour > 12) {
                        curr_hour = curr_hour - 12;
                    }
                    curr_hour = checkTime(curr_hour);
                    curr_minute = checkTime(curr_minute);
                    curr_second = checkTime(curr_second);
                    document.getElementById('clock').innerHTML=curr_hour + ":" + curr_minute + ":" + curr_second + " " + a_p;
                }

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }
                setInterval(showTime, 500);
        //-->
            </script>

            <!-- Menampilkan Hari, Bulan dan Tahun -->
            <br>
            <script type='text/javascript'>
                <!--
                var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
                var date = new Date();
                var day = date.getDate();
                var month = date.getMonth();
                var thisDay = date.getDay(),
                thisDay = myDays[thisDay];
                var yy = date.getYear();
                var year = (yy < 1000) ? yy + 1900 : yy;
                document.write(thisDay + ', ' + day + ' ' + months[month] + ' ' + year);
            //-->
            </script>
            <marquee width="" height="">
                <br>Hai, <?php echo $data['username'] ?> - Selamat Datang di Sistem Surat Menyurat Kantor Desa Batu Mulya Berbasis Web</marquee>

            <?php } ?>

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Data & Dokumen</h1>

            </div>

            <!-- Content Row -->
            <div class="row">


                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Surat</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $jml_surat_keluar; ?></div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa fa-envelope fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <!-- Content Row -->



        </div>
        <!-- /.container-fluid -->

    </div>

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; by Trevina </span>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

</div>
<?php } ?>

</div>
<!-- End of Page Wrapper -->

<?php if($_SESSION['level']=='KaDes'){ ?>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <?php include ('proses/modal/modal_keluar.php')?>



<?php } ?>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="js/demo/chart-area-demo.js"></script>
<script src="js/demo/chart-pie-demo.js"></script>

</body>

</html>