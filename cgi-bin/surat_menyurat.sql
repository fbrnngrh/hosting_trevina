-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2022 at 04:20 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surat_menyurat`
--

-- --------------------------------------------------------

--
-- Table structure for table `bagian`
--

CREATE TABLE `bagian` (
  `id_bagian` int(11) NOT NULL,
  `bagian` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bagian`
--

INSERT INTO `bagian` (`id_bagian`, `bagian`) VALUES
(1, 'KaDes'),
(2, 'Sekdes'),
(3, 'Staf'),
(4, 'RT');

-- --------------------------------------------------------

--
-- Table structure for table `detail_permintaan_surat`
--

CREATE TABLE `detail_permintaan_surat` (
  `id_detail_permintaan_surat` int(11) NOT NULL,
  `id_permintaan` int(11) NOT NULL,
  `tanggal_detail` date NOT NULL,
  `status` text NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `jabatan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `jabatan`) VALUES
(1, 'Kepala Desa'),
(2, 'Ketua RT'),
(3, 'Sekertaris Desa');

-- --------------------------------------------------------

--
-- Table structure for table `katsur`
--

CREATE TABLE `katsur` (
  `id_katsur` int(11) NOT NULL,
  `katsur` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `katsur`
--

INSERT INTO `katsur` (`id_katsur`, `katsur`) VALUES
(1, 'Surat Keterangan Usaha'),
(2, 'Surat Keterangan Tidak Mampu'),
(3, 'Surat Keterangan Hilang'),
(4, 'Surat Keterangan Rekomendasi');

-- --------------------------------------------------------

--
-- Table structure for table `macsur`
--

CREATE TABLE `macsur` (
  `id_macsur` int(11) NOT NULL,
  `macsur` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `macsur`
--

INSERT INTO `macsur` (`id_macsur`, `macsur`) VALUES
(1, 'Surat Masuk'),
(2, 'Surat Keluar');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` text NOT NULL,
  `alamat` text NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `id_bagian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `alamat`, `id_jabatan`, `id_bagian`) VALUES
(1, 'Kris', 'Batu Tungku', 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `permintaan`
--

CREATE TABLE `permintaan` (
  `id_permintaan` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tanggal_permintaan` date NOT NULL,
  `nama` text NOT NULL,
  `keterangan_surat` text NOT NULL,
  `status` text NOT NULL,
  `approval_rt` text NOT NULL,
  `approval_petugas` text NOT NULL,
  `notif_rt` text NOT NULL,
  `notif_kepala_desa` text NOT NULL,
  `approval_kepala_desa` text NOT NULL,
  `notif_petugas` text NOT NULL,
  `ttd_kepala_desa` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `surat`
--

CREATE TABLE `surat` (
  `id_surat` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_macsur` int(11) NOT NULL,
  `id_katsur` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` text NOT NULL,
  `approval_petugas` text NOT NULL,
  `status_user` text NOT NULL,
  `status_rt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `nama` text NOT NULL,
  `email` text NOT NULL,
  `username` text NOT NULL,
  `password` varchar(30) NOT NULL,
  `level` enum('Admin','Petugas','Pengguna','RT','KaDes') NOT NULL,
  `status` varchar(50) NOT NULL,
  `approval` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `id_pegawai`, `nama`, `email`, `username`, `password`, `level`, `status`, `approval`) VALUES
(1, 3, 'Erin', 'erin@gmail.com', 'admin', 'admin', 'Admin', 'Aktif', 'Diterima');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bagian`
--
ALTER TABLE `bagian`
  ADD PRIMARY KEY (`id_bagian`);

--
-- Indexes for table `detail_permintaan_surat`
--
ALTER TABLE `detail_permintaan_surat`
  ADD PRIMARY KEY (`id_detail_permintaan_surat`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `katsur`
--
ALTER TABLE `katsur`
  ADD PRIMARY KEY (`id_katsur`);

--
-- Indexes for table `macsur`
--
ALTER TABLE `macsur`
  ADD PRIMARY KEY (`id_macsur`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`),
  ADD UNIQUE KEY `id_jabatan` (`id_jabatan`),
  ADD KEY `id_bagian` (`id_bagian`);

--
-- Indexes for table `permintaan`
--
ALTER TABLE `permintaan`
  ADD PRIMARY KEY (`id_permintaan`);

--
-- Indexes for table `surat`
--
ALTER TABLE `surat`
  ADD PRIMARY KEY (`id_surat`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_macsur` (`id_macsur`),
  ADD KEY `id_katsur` (`id_katsur`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bagian`
--
ALTER TABLE `bagian`
  MODIFY `id_bagian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `detail_permintaan_surat`
--
ALTER TABLE `detail_permintaan_surat`
  MODIFY `id_detail_permintaan_surat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `katsur`
--
ALTER TABLE `katsur`
  MODIFY `id_katsur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `macsur`
--
ALTER TABLE `macsur`
  MODIFY `id_macsur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permintaan`
--
ALTER TABLE `permintaan`
  MODIFY `id_permintaan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `surat`
--
ALTER TABLE `surat`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
