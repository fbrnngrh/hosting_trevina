<?php
require('fpdf.php');

// Membuat objek PDF

$pdf = new FPDF('P','mm',array(297,210)); //L For Landscape / P For Portrait
$pdf->AddPage();
$pdf->SetFillColor(255,255,255);

include("../includes/defines.php");
$id=$_GET['id'];
$dt=mysqli_fetch_array(mysqli_query($con,"SELECT * FROM surat_masuk sm left join pegawai p on sm.disposisi=p.id_pegawai left join jabatan j on p.id_jabatan=j.id_jabatan where id_surat='$id'"));

function tgl_indo($tanggal){
  $bulan = array (
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);
  return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
// Header
$pdf->Image('logo.png',20,10,18);
$pdf->SetFont('Arial','B',14);
$pdf->SetY(10);
$pdf->SetX(5);
$pdf->Cell(200,6,'PEMERINTAH KABUATEN TANAH LAUT',0,1,'C');
$pdf->SetX(5);
$pdf->Cell(200,6,'KECAMATAN PANYIPATAN',0,1,'C');
$pdf->SetX(5);
$pdf->Cell(200,6,'DESA BATU MULYA',0,1,'C');
$pdf->SetFont('Arial','I',8);
$pdf->SetX(5);
$pdf->Cell(200,4,'Alamat : Jl. Raya Batu Mulya RT. 002 Telpon: 082250325900 Kode Pos. 70871',0,1,'C');
$pdf->Cell(200,4,'Email : pemdesbatumulya@gmail.com',0,0,'C');
  // $pdf->SetY(10);
$pdf->SetLineWidth(1);
$pdf->Ln(5);
$pdf->SetX(20);
$pdf->Cell(165,0,'',1,1,'C');


$pdf->Ln(2);

$pdf->SetFont('Arial', 'B', 12);
$pdf->SetX(5);
$pdf->Cell(200,6,'LEMBAR DISPOSISI',0,1,'C');
$pdf->Ln(5);

// Konten surat
// $data_disposisi = [
//     'nomor_surat' => '001/DP/2023',
//     'tanggal' => '15 Juni 2023',
//     'perihal' => 'Undangan Rapat',
//     'isi_disposisi' => 'Diteruskan kepada Departemen Keuangan untuk peninjauan lebih lanjut.'
// ];

$pdf->SetFont('Arial', '', 12);
$pdf->SetX(20);
$pdf->Cell(50,6,'Nomor Surat',0,0,'L');
$pdf->Cell(100,6,' : '.$dt['nomor_surat'],0,1,'L');

$pdf->SetX(20);
$pdf->Cell(50,6,'Tanggal Surat',0,0,'L');
$pdf->Cell(100,6,' : '.tgl_indo($dt['tgl_masuk']),0,1,'L');

$pdf->SetX(20);
$pdf->Cell(50,6,'Perihal',0,0,'L');
$pdf->Cell(100,6,' : '.$dt['perihal'],0,1,'L');
$pdf->Ln(7);
$pdf->SetX(5);
$pdf->SetLineWidth(0.5);

$pdf->Ln(5);
$pdf->SetX(20);
$pdf->Cell(165,0,'',1,1,'C');
$pdf->SetFont('Arial', 'B', 12);
$pdf->Ln(5);
$pdf->Cell(185,6,'Disposisi Kepada',0,1,'C');
$pdf->Ln(5);
$pdf->SetFont('Arial', '', 12);
$pdf->SetX(20);
$pdf->Cell(50,6,$dt['nama_pegawai'].' ('.$dt['jabatan'].')',0,1,'L');

$pdf->Ln(5);
$pdf->SetX(20);
$pdf->Cell(165,0,'',1,1,'C');
$pdf->Ln(5);
$pdf->SetFont('Arial', 'B', 12);
$pdf->SetX(20);
$pdf->Cell(50,6,'Isi Disposisi :',0,1,'L');

$pdf->SetFont('Arial', '', 12);
$pdf->SetX(20);
$pdf->MultiCell(200,6,$dt['keterangan'],0,1,'L');
// Simpan file PDF
// $pdf->Output('disposisi_surat.pdf', 'F');


$pdf->Output();

?>