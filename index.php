﻿
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="login2/surat.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Login</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="login2/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="login2/assets/css/material-dashboard.css" rel="stylesheet" />
    <link href="login2/assets/css/demo.css" rel="stylesheet" />
    <link href="login2/assets/css/font-awesome.css" rel="stylesheet" />
    <link href="login2/assets/css/google-roboto-300-700.css" rel="stylesheet" />

</head>


<body>
    <nav class="navbar navbar-primary navbar-transparent navbar-absolute">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand"></a>
            </div>
            <div class="collapse navbar-collapse">
               <ul class="nav navbar-nav navbar-right"> 
                 
                </ul>
            </div>
        </div>
    </nav>
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" filter-color="black"  data-image="img/background.jpg">
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                             <form method="post" action="ceklogin.php">
                               <div class="card card-profile card-hidden">
                                    <div class="card-avatar">
                                        <a href="#pablo">
                                            <img class="avatar" src="login2/surat.png" alt="...">
                                        </a>
                                    </div>
                                   
                                    <div class="card-content">
                                        <h4 class="card-title">LOGIN AKUN</h4>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">face</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                 <label class="control-label">Username</label>
                                                <input type="text" id="username" name="username"  class="form-control"  required />
                                               
                                            </div>
                                        </div>
                                        
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Password</label>
                                                <input type="password" id="password" name="password"  class="form-control"  required/>
                                            </div>
                                        </div>
                                    </div>

                                    <a class="txt12" href="pendaftaran.php">
                                            Pendaftaran
                                        </a>
                                        <br>

                                        <a class="txt14"  href="index.php"> 
                                            Kembali ke Halaman
                                            <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                                        </a> 

                                    <div class="footer text-center">
                                        <button name="submit" type="submit" class="btn btn-info btn-round">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                  
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                  
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                  
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                 
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="#">Sistem Surat Menyurat Kantor Desa Batu Mulya Berbasis Web</a>
                    </p>
                </div>
            </footer>
        </div>
    </div>
</body>
<div class="fixed-plugin">
    <div class="dropdown show-dropdown">
        <a href="#" data-toggle="dropdown"></a>
        
    </div>
</div>
</body>
<script src="login2/assets/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="login2/assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="login2/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="login2/assets/js/material.min.js" type="text/javascript"></script>
<script src="login2/assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="login2/assets/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="login2/assets/js/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="login2/assets/js/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="login2/assets/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="login2/assets/js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<script src="login2/assets/js/jquery.sharrre.js"></script>
<!-- DateTimePicker Plugin -->
<script src="login2/assets/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="login2/assets/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="login2/assets/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
<!-- Select Plugin -->
<script src="login2/assets/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="login2/assets/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="login2/assets/js/sweetalert2.js"></script>
<!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="login2/assets/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="login2/assets/js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="login2/assets/js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="login2/assets/js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="login2/assets/js/demo.js"></script>
<script type="text/javascript">
    $().ready(function() {
        demo.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>

</html>