-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 20, 2022 at 12:55 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `surat_menyurat`
--

-- --------------------------------------------------------

--
-- Table structure for table `bagian`
--

CREATE TABLE IF NOT EXISTS `bagian` (
  `id_bagian` int(11) NOT NULL,
  `bagian` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bagian`
--

INSERT INTO `bagian` (`id_bagian`, `bagian`) VALUES
(1, 'Staf'),
(2, 'Kades');

-- --------------------------------------------------------

--
-- Table structure for table `detal_permintaan_surat`
--

CREATE TABLE IF NOT EXISTS `detal_permintaan_surat` (
`id_detail_permintaan_surat` int(11) NOT NULL,
  `id_permintaan` int(11) NOT NULL,
  `tanggal_detail` date NOT NULL,
  `status` text NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE IF NOT EXISTS `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `jabatan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `jabatan`) VALUES
(1, 'Kepala Desa'),
(2, 'Sekdes'),
(3, 'KASI Kesejahteraan Pelayanan'),
(4, 'Kaur Umum Perencanaan');

-- --------------------------------------------------------

--
-- Table structure for table `katsur`
--

CREATE TABLE IF NOT EXISTS `katsur` (
`id_katsur` int(11) NOT NULL,
  `katsur` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `macsur`
--

CREATE TABLE IF NOT EXISTS `macsur` (
`id_macsur` int(11) NOT NULL,
  `macsur` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` text NOT NULL,
  `alamat` text NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `id_bagian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `alamat`, `id_jabatan`, `id_bagian`) VALUES
(1, 'Erin', 'Batu Mulya', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permintaan`
--

CREATE TABLE IF NOT EXISTS `permintaan` (
`id_permintaan` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tanggal_permintaan` date NOT NULL,
  `nama` text NOT NULL,
  `keterangan_surat` text NOT NULL,
  `status` text NOT NULL,
  `approval_rt` text NOT NULL,
  `approval_petugas` text NOT NULL,
  `notif_rt` text NOT NULL,
  `notif_petugas` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `surat`
--

CREATE TABLE IF NOT EXISTS `surat` (
`id_surat` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_macsur` int(11) NOT NULL,
  `id_katsur` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` text NOT NULL,
  `approval_petugas` text NOT NULL,
  `status_user` text NOT NULL,
  `status_rt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id_user` int(50) NOT NULL,
  `id_pegawai` int(50) NOT NULL,
  `nama` text NOT NULL,
  `email` text NOT NULL,
  `username` text NOT NULL,
  `password` varchar(30) NOT NULL,
  `level` enum('Admin','Petugas','Pengguna','RT') NOT NULL,
  `status` varchar(50) NOT NULL,
  `approval` varchar(30) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `id_pegawai`, `nama`, `email`, `username`, `password`, `level`, `status`, `approval`) VALUES
(1, 1, 'erin', 'erin07@gmail.com', 'erin', 'erin', 'Admin', 'Aktif', 'Diterima');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bagian`
--
ALTER TABLE `bagian`
 ADD PRIMARY KEY (`id_bagian`);

--
-- Indexes for table `detal_permintaan_surat`
--
ALTER TABLE `detal_permintaan_surat`
 ADD PRIMARY KEY (`id_detail_permintaan_surat`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
 ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `katsur`
--
ALTER TABLE `katsur`
 ADD PRIMARY KEY (`id_katsur`);

--
-- Indexes for table `macsur`
--
ALTER TABLE `macsur`
 ADD PRIMARY KEY (`id_macsur`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
 ADD PRIMARY KEY (`id_pegawai`), ADD KEY `id_jabatan` (`id_jabatan`), ADD KEY `id_bagian` (`id_bagian`);

--
-- Indexes for table `permintaan`
--
ALTER TABLE `permintaan`
 ADD PRIMARY KEY (`id_permintaan`);

--
-- Indexes for table `surat`
--
ALTER TABLE `surat`
 ADD PRIMARY KEY (`id_surat`), ADD KEY `id_user` (`id_user`), ADD KEY `id_macsur` (`id_macsur`), ADD KEY `id_katsur` (`id_katsur`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id_user`), ADD KEY `id_pegawai` (`id_pegawai`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detal_permintaan_surat`
--
ALTER TABLE `detal_permintaan_surat`
MODIFY `id_detail_permintaan_surat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `katsur`
--
ALTER TABLE `katsur`
MODIFY `id_katsur` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `macsur`
--
ALTER TABLE `macsur`
MODIFY `id_macsur` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permintaan`
--
ALTER TABLE `permintaan`
MODIFY `id_permintaan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `surat`
--
ALTER TABLE `surat`
MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id_user` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `pegawai`
--
ALTER TABLE `pegawai`
ADD CONSTRAINT `pegawai` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
ADD CONSTRAINT `user` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
